@Scenario81to100
Feature: Scenario81to100

Background: ZPC login page
Given user is on login page
When user enters username
And user enters password
And user clicks on Login button


	@Scenario81
  Scenario: 81
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "EightyOne fullaunch" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MONDEO LX" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2007" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17535206" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1999" in "Vehicle Details" page
    And Enter "Value" in the text area as "190000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "2001-3000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "EightyOne fullaunch" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "EightyOne fullaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/05/1982" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "12/08/2004" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    #And Enter "Occupation" value as "Teacher" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Music Teacher" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario82 @regression1
  Scenario: 82
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "eightytwo fulllaunch" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MONDEO GHIA X TDDI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2002" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17537101" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1998" in "Vehicle Details" page
    And Enter "Value" in the text area as "194999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "5001-6000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "eightytwo fulllaunch" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "eightytwo fulllaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/04/1979" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/06/2005" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Firefighter" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario83
  Scenario: 83
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "eightythreedata fullaunch" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "FIESTA ZETEC" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2008" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17541102" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1388" in "Vehicle Details" page
    And Enter "Value" in the text area as "195000" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "11001-12000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "eightythreedata fullaunch" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "eightythreedata fullaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "25/12/1964" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/06/2005" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    #And Enter "Occupation" value as "Librarian" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Librarian" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario84 
  Scenario: 84
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "eightyfourdata fulllaunch" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "FIESTA LX TDCI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2005" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17541702" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1398" in "Vehicle Details" page
    And Enter "Value" in the text area as "199999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "15001-16000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "eightyfourdata fulllaunch" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "eightyfourdata fulllaunch" in "New Contact: Contact" Page
    #And Click on the "Primary" check button in "New Contact: Contact" page
    And Select date for "Date of Birth" field as "12/07/1986" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "16/03/2007" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Lawyer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario85
  Scenario: 85
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "eightyfivedata fulllaunch" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "FOCUS SPORT TDCI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2003" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17547402" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1753" in "Vehicle Details" page
    And Enter "Value" in the text area as "200000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "19001-20000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "eightyfivedata fulllaunch" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "eightyfivedata fulllaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "21/03/1969" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "15/02/2008" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mathematician" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario86
  Scenario: 86
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "eightysixdata fulllaunch" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "ESCORT LASER 2 AUTO" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1985" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17554114" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1597" in "Vehicle Details" page
    And Enter "Value" in the text area as "204999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "25001-30000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "eightysixdata fulllaunch" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "eightysixdata fulllaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "16/03/1960" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/03/2002" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mortgage Consultant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario87
  Scenario: 87
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "eightseven testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "FOCUS TITANIUM TDCI (136)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17555903" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1997" in "Vehicle Details" page
    And Enter "Value" in the text area as "206999" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "35001-40000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "eightseven testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "eightseven testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/06/1971" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "12/08/2004" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Lawyer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario88 @regression1
  Scenario: 88
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "eightyeightdata fulllaunch" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "ESCORT L DIESEL" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1988" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17560501" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1608" in "Vehicle Details" page
    And Enter "Value" in the text area as "209999" in "Vehicle Details" page
    And Select "Use" value as "Class 3" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "THIRD PARTY ONLY" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "11001-12000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "eightyeightdata fulllaunch" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "eightyeightdata fulllaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/04/1979" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/03/2002" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter "Occupation" value as "Accountant" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario89 @regression1
  Scenario: 89
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "eightyninedata fulllaunch" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "FIESTA ZETEC CLIMATE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2008" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17563603" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1388" in "Vehicle Details" page
    And Enter "Value" in the text area as "210000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "0-1000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "eightyninedata fulllaunch" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "eightyninedata fulllaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/03/1989" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/09/2007" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Police Community Support Officer" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Enter "Occupation" value as "Police Community Support Officer" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario90 @regression1
  Scenario: 90
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "ninetydata fulllaunch" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "HONDA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "CR-V S I-DTEC" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "21057717" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1597" in "Vehicle Details" page
    And Enter "Value" in the text area as "214999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "3001-4000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "ninetydata fulllaunch" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "ninetydata fulllaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "16/03/1960" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "08/10/2001" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter "Occupation" value as "Retired" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Retired" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario91
  Scenario: 91
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "ninetyone testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "HUMBER" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "SCEPTRE AUTO" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1967" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "22003202" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1725" in "Vehicle Details" page
    And Enter "Value" in the text area as "215000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "7001-8000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "ninetyone testcase" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "ninetyone testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/09/1990" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/10/2009" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Firefighter" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "2" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario92
  Scenario: 92
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "ninetytwo testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "JEEP" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "GRAND CHEROKEE PLATINUM" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2004" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "24503902" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "4701" in "Vehicle Details" page
    And Enter "Value" in the text area as "219999" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "THIRD PARTY FIRE & THEFT" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "9001-10000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "ninetytwo testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "ninetytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/05/1980" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/07/2006" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Firefighter" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario93
  Scenario: 93
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "ninetythree testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VOLVO" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "440 SE AUTO" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1995" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "54552302" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1783" in "Vehicle Details" page
    And Enter "Value" in the text area as "220000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "12001-13000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "ninetythree testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "ninetythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/04/1979" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/02/2001" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    #And Enter "Occupation" value as "Librarian" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Librarian" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario94
  Scenario: 94
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "ninetyfour testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VOLVO" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "XC90 OCEAN RACE T6" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2005" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "54553002" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2922" in "Vehicle Details" page
    And Enter "Value" in the text area as "224999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "13001-14000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "ninetyfour testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "ninetyfour testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "26/08/1974" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "16/03/2007" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Lawyer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario95 @regression1
  Scenario: 95
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "ninetyfive testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VOLVO" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "S60 R-DESIGN D2" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2013" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "54664302" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1560" in "Vehicle Details" page
    And Enter "Value" in the text area as "225000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "16001-17000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "ninetyfive testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "ninetyfive testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/06/1969" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/05/2006" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mathematician" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario96
  Scenario: 96
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "ninetysix testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VAUXHALL" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MOVANO MWB" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2001" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "90301736" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2499" in "Vehicle Details" page
    And Enter "Value" in the text area as "229999 " in "Vehicle Details" page
    And Select "Use" value as "Laid Up" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "FIRE & THEFT ONLY" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "15001-16000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "ninetysix testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "ninetysix testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/04/1936" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "25/12/1998" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Retired" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario97
  Scenario: 97
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "ninetyseven tests" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FIAT" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "DUCATO 33 SH1 MULTIJET II 130" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "90313705" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2287" in "Vehicle Details" page
    And Enter "Value" in the text area as "230000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "19001-20000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "ninetyseven tests" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "ninetyseven tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/08/1966" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/04/2003" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Police Community Support Officer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario98
  Scenario: 98
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "ninetyeight testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "FIESTA FREESTYLE 16V" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17537601" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1242" in "Vehicle Details" page
    And Enter "Value" in the text area as "234999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "20001-25000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "ninetyeight testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "ninetyeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "30/12/1948" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "12/08/2004" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Music Teacher" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario99
  Scenario: 99
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "ninetynine test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MOTORCYCLE OTHER" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "1000cc and over" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "92004000" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "0" in "Vehicle Details" page
    And Enter "Value" in the text area as "235000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "25001-30000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Motorcycle" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "ninetynine test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "ninetynine test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "21/03/1969" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/03/2002" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser

  @Scenario100 @regression1
  Scenario: 100
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Hundred testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "SMART" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "FORTWO NIGHTORANGE MHD (71)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2012" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "711902" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "999" in "Vehicle Details" page
    And Enter "Value" in the text area as "239999" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "30001-35000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "Hundred testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "Hundred testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/05/1991" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/10/2008" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Lawyer" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium 
    And Close the browser
