@scenario241to260
Feature: Scenario241to260

Background: ZPC login page
Given user is on login page
When user enters username
And user enters password
And user clicks on Login button


 @Scenario241
  Scenario: 241
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfortyone testfull" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "HOLDEN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "VXR8 GTS" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "19700301" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "6162" in "Vehicle Details" page
    And Enter "Value" in the text area as "765000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "20001-25000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfortyone testfull" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyone testfull" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/04/1965" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "16/04/1983" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Road Sweeper" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyone testfull" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "12/12/1954" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "19/08/1972" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Postman or Woman" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyone testfull" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "28/02/1926" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "25/08/1943" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Bus Driver" in "New Contact: Contact" Page 
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "5" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver5" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyone testfull" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/06/1967" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "13/11/1985" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Interpreter" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PB" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "5" value as "Driver5" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario242
  Scenario: 242
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfortytwo testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "GINETTA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "G 21" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1972" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "20002301" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1600" in "Vehicle Details" page
    And Enter "Value" in the text area as "774999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "5001-6000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfortytwo testcase" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "21/03/1969" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "17/03/1987" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Restaurant Manager" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "2" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/01/1958" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "18/09/1975" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Shoe Repairer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "25/03/1988" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "19/09/2005" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Vicar" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "5" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver5" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/10/1987" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "20/12/2006" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Interpreter" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PB" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "5" value as "Driver5" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario243
  Scenario: 243
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfortythree testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "GOGGOMOBIL" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "400 COUPE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1959" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "20200801" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "392" in "Vehicle Details" page
    And Enter "Value" in the text area as "775000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "6001-7000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfortythree testcase" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "25/12/1964" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "21/12/1982" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Scaffolder" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "23/08/1956" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/05/1974" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Police Community Support Officer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "3" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "11/09/1962" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "07/03/1980" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Pest Control" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "5" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver5" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "03/05/1929" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "04/05/1947" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Calibration Manager" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PB" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "5" value as "Driver5" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario244 
  Scenario: 244
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfortyfour tests" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "HINDUSTAN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "AMBASSADOR CLASSIC DSZ" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "20300401" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1995" in "Vehicle Details" page
    And Enter "Value" in the text area as "780000" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "THIRD PARTY FIRE & THEFT" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "40001 & higher" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfortyfour tests" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyfour tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "22/06/1954" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "17/06/1972" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Hostess" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyfour tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "09/09/1945" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "18/05/1963" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Lift Engineer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyfour tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "28/01/1921" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "25/08/1938" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Janitor" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "4" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "5" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver5" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyfour tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/04/1994" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "13/08/2013" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Literary Agent" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PB" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "5" value as "Driver5" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario245
  Scenario: 245
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfortyfive testfull" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "HILLMAN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "AVENGER SUPER" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1976" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "20501302" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1295" in "Vehicle Details" page
    And Enter "Value" in the text area as "785000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "4001-5000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfortyfive testfull" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyfive testfull" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/07/1961" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "06/07/1979" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Music Teacher" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyfive testfull" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "07/08/1997" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "15/04/2015" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Milkman or Woman" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyfive testfull" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/09/1944" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "07/03/1962" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Blind Fitter" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "5" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver5" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyfive testfull" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "31/10/1985" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "19/10/2006" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Nuclear Scientist" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PB" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "5" value as "Driver5" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "5" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "5" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    #And Close the tab "Quotes"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

	
  @Scenario246 
  Scenario: 246
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfortysix test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "HONDA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "CIVIC ESI CVT (AUTO) - 3DR" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1999" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "21002301" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1590" in "Vehicle Details" page
    And Enter "Value" in the text area as "790000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any Driver" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "16001-17000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfortysix test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortysix test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "30/12/1948" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "26/12/1966" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Housewife or Househusband" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortysix test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "23/11/1969" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/08/1987" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Counsellor" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario247  
  Scenario: 247
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfortyseven testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "HONDA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "ACCORD I-VTEC EX ADAS" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "21043402" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2354" in "Vehicle Details" page
    And Enter "Value" in the text area as "799999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any Driver" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "2001-3000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfortyseven testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyseven testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/09/1944" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "06/09/1962" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Music Teacher" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    #And Close the tab "Quotes"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario248
  Scenario: 248
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfortyeight testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "HYUNDAI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "I10 CLASSIC" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2009" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "22519001" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1086" in "Vehicle Details" page
    And Enter "Value" in the text area as "800000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any Driver" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "9001-10000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfortyeight testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortyeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "12/04/1979" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "07/04/2014" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Unemployed" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    #And Close the tab "Quotes"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario249
  Scenario: 249
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfortynine testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "ISO RIVOLTA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "ISO RIVOLTA" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1969" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "23002101" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "5359" in "Vehicle Details" page
    And Enter "Value" in the text area as "805000" in "Vehicle Details" page
    And Select "Use" value as "Class 2" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any Driver" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "15001-16000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfortynine testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfortynine testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "05/11/1954" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "31/10/1972" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Police Community Support Officer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario250
  Scenario: 250
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfifty testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "ISUZU" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "TROOPER DUTY" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2002" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "23500301" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "3494" in "Vehicle Details" page
    And Enter "Value" in the text area as "810000" in "Vehicle Details" page
    And Select "Use" value as "Class 2" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any Driver" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "9001-10000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfifty testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfifty testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "28/01/1942" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "24/01/1960" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Soldier" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario251
  Scenario: 251
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfiftyone testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "LEXUS" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "GS 300H F SPORT DUAL VVT-I" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "28811417" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2494" in "Vehicle Details" page
    And Enter "Value" in the text area as "815000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any Driver" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "10001-11000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfiftyone testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    #Driver2
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfiftyone testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "02/02/1958" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "29/01/1976" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mortgage Consultant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario252
  Scenario: 252
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfiftytwo testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "LEXUS" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "CT 200H ADVANCE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "28811419" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1798" in "Vehicle Details" page
    And Enter "Value" in the text area as "820000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any driver ex under 25s" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "40001 & higher" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfiftytwo testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    #Driver2
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfiftytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/04/1956" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "06/04/1974" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Police Community Support Officer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    #Driver3
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfiftytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "04/05/1960" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "10/01/1978" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "IT Manager" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario253  
  Scenario: 253
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfiftythree testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "LINCOLN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "CAPRI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1959" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "29001701" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "5194" in "Vehicle Details" page
    And Enter "Value" in the text area as "825000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any driver ex under 25s" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "30001-35000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfiftythree testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario254  
  Scenario: 254
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfiftyfour testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "LONSDALE" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "YD45 2.0" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1984" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "29500701" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1995" in "Vehicle Details" page
    And Enter "Value" in the text area as "834999" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any driver ex under 25s" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "40001 & higher" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfiftyfour testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    #Driver2
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfiftyfour testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/06/1971" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/05/1989" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Librarian" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario255  
  Scenario: 255
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfiftyfive testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "LOTUS" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "ELITE S2 SUPER 95" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1964" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "30010701" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1216" in "Vehicle Details" page
    And Enter "Value" in the text area as "835000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any driver ex under 25s" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "40001 & higher" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfiftyfive testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    #Driver2
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfiftyfive testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "12/07/1986" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "07/07/2012" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Lawyer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario256
  Scenario: 256
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfiftysix testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MICROCAR" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MC2 DYNAMIC LX" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2009" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "30202101" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "505" in "Vehicle Details" page
    And Enter "Value" in the text area as "840000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any driver ex under 30s" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "8001-9000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfiftysix testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfiftysix testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/08/1966" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/07/2014" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mathematician" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfiftysix testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "14/07/1975" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "21/03/1993" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Quality Manager" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario257
  Scenario: 257
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfiftyseven testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MIA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "C" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "30300301" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "0" in "Vehicle Details" page
    And Enter "Value" in the text area as "845000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "THIRD PARTY FIRE & THEFT" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any driver ex under 30s" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "12001-13000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfiftyseven testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfiftyseven testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/09/1980" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "05/09/2008" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario258
  Scenario: 258
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfiftyeight test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MAHINDRA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "INDIAN CHIEF" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1992" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "30500103" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2112" in "Vehicle Details" page
    And Enter "Value" in the text area as "854999" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any driver ex under 30s" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "14001-15000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfiftyeight test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario259
  Scenario: 259
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredfiftynine testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MARCOS" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "1600" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "30704001" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1599" in "Vehicle Details" page
    And Enter "Value" in the text area as "855000" in "Vehicle Details" page
    And Select "Use" value as "Class 3" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any driver ex under 30s" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "0-1000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredfiftynine testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredfiftynine testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "04/11/1951" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "30/10/1969" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Soldier" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario260  
  Scenario: 260
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredsixty test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MARLIN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "HUNTER V8" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "30800501" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "4554" in "Vehicle Details" page
    And Enter "Value" in the text area as "860000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Any driver ex under 30s" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "19001-20000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredsixty test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredsixty test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/04/1943" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/03/1961" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Firefighter" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

