@HomeScenario1to20
Feature: ZPC Home rating Scenarios

Background: ZPC login page
Given user is on login page
When user enters username "sriram.c49@wipro.com.muat2"
And user enters password "London@07"
And user clicks on Login button
Then user gets the title of the page
And page title should be "Lightning Experience"

@HRScenario1
  Scenario: 1
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Enter login Credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1865" in "Risk Location" page
    And Select "Property Usage" value as "Unoccupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber Frame/Brick Infill" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "Dualcom - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "5000000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "500000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "0" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "25000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "10000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "75000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "20000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "50000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "250000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "200000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "100000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "75000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "125000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "25000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "30000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "30000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "60000" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "250" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase one" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
   @HRScenario2
   Scenario: 2
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase two" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Bungalow" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1648" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "1" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation Area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "50% Flat Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "None" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "49999" in "Risk Location" page
    And Enter "Building Excess" in the text area as "0" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "12499" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "0" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1285" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1285" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1300" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1500" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "500" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase two" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    @HRScenario3
   Scenario: 3
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase three" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1655" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home Fully Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "2" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 1" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cedar/Wood Shingle Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "50000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "100" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "12500" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "100" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1285" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2273" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1290" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1650" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3080" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "1000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase three" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
   @HRScenario4
   Scenario: 4
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase four" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Flat" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1665" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cobb" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "80000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "250" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "14200" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "250" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3750" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2800" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1650" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collectios" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7400" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "2500" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "10000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase four" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
     @HRScenario5
    Scenario: 5
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase five" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Other" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1678" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Let property" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2*" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Lath and Plaster" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "99999" in "Risk Location" page
    And Enter "Building Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "15000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as " 2143" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2727" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collectios" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2100" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "5000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "250" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase five" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario6
    Scenario: 6
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase fix" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Semi Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1688" in "Risk Location" page
    And Select "Property Usage" value as "Weekend / Weekday / 2nd Home, Fully Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "6" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade A" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Other" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "100000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "17500" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6250" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3182" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1842" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "18000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5754" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1286" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1900" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "10000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase fix" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario7
    Scenario: 7
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase seven" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1699" in "Risk Location" page
    And Select "Property Usage" value as "Weekend / Weekday / 2nd Home, Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "7" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade B" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Dualcom - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "100001" in "Risk Location" page
    And Enter "Building Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "22500" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3214" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "28000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "14000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7200" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3960" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3491" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2117" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "2500" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase seven" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
     
    @HRScenario8
    Scenario: 8
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase eight" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1715" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Family" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade C" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Thatched" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Dualcom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "249999" in "Risk Location" page
    And Enter "Building Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "40000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "8750" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3956" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "13800" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "4200" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3490" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "5000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase eight" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario9
    Scenario: 9
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase nine" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Unknown" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1725" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Non Family" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "250000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "10000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "50000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "10000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "10000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7143" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "9091" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6521" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "10000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase nine" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario10
    Scenario: 10
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase ten" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1735" in "Risk Location" page
    And Select "Property Usage" value as "Other / Business" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "1" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Preservation Order" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber Frame/Brick Infill" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "250001" in "Risk Location" page
    And Enter "Building Excess" in the text area as "0" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "60000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "250" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "11250" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "10909" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2400" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "250" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "10000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase ten" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario11_34
    Scenario: 11_34
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase eleven" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1688" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Family, Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "2" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "68000001" in "Risk Location" page
    And Enter "Building Excess" in the text area as "0" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "32500000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "302200" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2708333" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1015625" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "812500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5909091" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "4333333" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2031250" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "433268" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "10000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase eleven" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    @HRScenario12_35
    Scenario: 12_35
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase twelve" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1645" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Let property" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2*" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "50% Flat Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "78000001" in "Risk Location" page
    And Enter "Building Excess" in the text area as "100" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "85000000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "325000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7083333" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2656250" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2125000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "12142857" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "15454545" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "11333333" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5312500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "164000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3250000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1650000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1900000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7150946" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "0" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase tewlve" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
     @HRScenario13_36
    Scenario: 13_36
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase thirteen" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Unknown" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1655" in "Risk Location" page
    And Select "Property Usage" value as "Weekend / Weekday / 2nd Home, Fully Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade A" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cedar/Wood Shingle Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Dualcom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "88000001" in "Risk Location" page
    And Enter "Building Excess" in the text area as "250" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "100000000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "365000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "8333333" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3125000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "10526315" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "12580000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "280000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5800000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "4895000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5700000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "8956000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3980000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "9181997" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "250" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase thirteen" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario14_37
    Scenario: 14_37
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase fourteen" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1665" in "Risk Location" page
    And Select "Property Usage" value as "Weekend / Weekday / 2nd Home, Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "7" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade B" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cobb" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "250000001" in "Risk Location" page
    And Enter "Building Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "115000000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "405000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "9583333" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3593750" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2875000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2300000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "16428571" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "20909091" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "15333333" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7187500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "10500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2320000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3580000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "120000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "315000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "8060000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6590000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "19000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "19000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "10404565" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "500" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase fourteen" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario15_38
    Scenario: 15_38
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase fifteen" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Bungalow" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1675" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Family" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade C" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Lath and Plaster" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "499999999" in "Risk Location" page
    And Enter "Building Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "180000000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "10000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "445000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "15000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5625000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "4500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3600000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "32727272" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "24000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "11250000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "560000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3400000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1750000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "29225" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "1000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase fifteen" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario16_39
    Scenario: 16_39
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase sixteen" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Other" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1710" in "Risk Location" page
    And Select "Property Usage" value as "Unoccupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "1" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Thatched" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "176999999" in "Risk Location" page
    And Enter "Building Excess" in the text area as "10000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "375000000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "600000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "31250000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "11718750" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "9375000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "53571428" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "68181818" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "50000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "23437500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "39473684" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "52650000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5000000" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "10000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase sixteen" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario17_40
    Scenario: 17_40
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase seventeen" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Semi Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1725" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "2" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation Area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "200000000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "0" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "400000000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "52821633" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "33333333" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "12500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "10000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "8000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "72727272" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "53333333" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "25000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "78000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2256000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "4500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "666000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6310700" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "26005000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "18000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "36315502" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "2500" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase seventeen" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
     @HRScenario18_41
    Scenario: 18_41
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase eighteen" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1738" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Family, Fully Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 1" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber Frame/Brick Infill" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "210000001" in "Risk Location" page
    And Enter "Building Excess" in the text area as "100" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "1100000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "38825000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "35416666" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "13281250" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "10625000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "8500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "60714286" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "77272727" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "56666667" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "26562500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2256000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "875000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6580000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "111859623" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "5000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase eighteen" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
     @HRScenario19_42
    Scenario: 19_42
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase nineteen" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1748" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Family, Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Wattle and Daub" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "None" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "144000000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "250" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "1345000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "10000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "940000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "39583333" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "14843750" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "11875000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "9500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1409200" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "12250000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "20000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6250000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2580000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "38000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "184425340" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "10000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase nineteen" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
     @HRScenario20_43
    Scenario: 20_43
    
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Unknown" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1758" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Let property" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "8" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2*" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "9999999999" in "Risk Location" page
    And Enter "Building Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "1500000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "100" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "41666667" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "15625000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "12500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "10000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "71428571" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "90909091" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "66666666" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "31250000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "52631579" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "105000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "82500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "4752000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2050000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "16500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "8000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "88000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6800000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "4300000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "236806824" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "250" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "10000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase twenty" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser