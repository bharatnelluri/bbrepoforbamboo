
@HomeScenario41to60
Feature: ZPC Home rating Scenarios

Background: ZPC login page
Given user is on login page
When user enters username "sriram.c49@wipro.com.muat2"
And user enters password "London@07"
And user clicks on Login button
Then user gets the title of the page
And page title should be "Lightning Experience"

@HRScenario29_67
  Scenario: 29_67
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario sixty seven" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Other" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1660" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home Fully Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "2" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 1" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cedar/Wood Shingle Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Dualcom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "75000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario sixty seven" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser



@HRScenario29_188
  Scenario: 29_188
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario one hundred eighty eight" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Other" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1785" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "7" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Other" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Dualcom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "172121428" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "43636" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "31000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "500" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario one hundred eighty eight" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    

@HRScenario30_69
  Scenario: 30_69
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario sixty nine" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Unknown" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1700" in "Risk Location" page
    And Select "Property Usage" value as "Weekend / Weekday / 2nd Home Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade B" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Lath and Plaster" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "116000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "100" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario sixty nine" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser



   @HRScenario30_200
  Scenario: 30_200
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario Two hundred" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Other" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1690" in "Risk Location" page
    And Select "Property Usage" value as "Weekend / Weekday / 2nd Home Fully Occupied (staff present)" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade B" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Lath and Plaster" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - Non Approved" from the dropdown in the "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "666000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario two hundred" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario31_70
  Scenario: 31_70
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario seventy" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Bungalow" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1715" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted Family" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "6" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade C" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Other" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Pakent - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "142000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "250" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario seventy" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser



@HRScenario32_45
  Scenario: 32_45
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario forty five" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Bungalow" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1785" in "Risk Location" page
    And Select "Property Usage" value as "Weekend / Weekday / 2nd Home Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade B" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cedar/Wood Shingle Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents? ?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "8374" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario forty five" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser



@HRScenario32_208
  Scenario: 32_208
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario Two hundred eight" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Flat" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1786" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home  Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 1" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "50% Flat Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "875000" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "9200" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "12006000" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6580000" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario Two Hundred eight" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario33_49
  Scenario: 33_49
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario forty Nine" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Semi Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1830" in "Risk Location" page
    And Select "Property Usage" value as "Unoccupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed " from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents? ?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "22765" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "0" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario forty Nine" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario34_205
  Scenario: 31_205
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario Two hundred eight" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detacehd" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1750" in "Risk Location" page
    And Select "Property Usage" value as "Unoccupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "2" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber Frame/Brick Infill" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Pakent - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "17255000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario Two Hundred five" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser



@HRScenario34_209
  Scenario: 34_209
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario Two hundred nine" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Other" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1795" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home Let " from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade C" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cedar/Wood Shingle Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "131000" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3500" in "Risk Location" page  
    And Enter "Valuables Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario Two Hundred nine" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser




@HRScenario35_198
  Scenario: 35_198
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario one hundred nighty eight" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1670" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "2" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2*" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cedar/Wood Shingle Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "104000020" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellerys" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "97452500" in "Risk Location" page  
    And Enter "Jewellery Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario one hundred nighty eight" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser



@HRScenario36_213
  Scenario: 36_213
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario two hundred thirteen" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Unkown" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1845" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted Family" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "1" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "209876542" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "33465000" in "Risk Location" page  
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2200" in "Risk Location" page  
    And Enter "Valuables Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario Scenario two hundred thirteen" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser




@HRScenario37_50
  Scenario: 37_50
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario Fifty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1845" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "6" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Thatched" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents? ?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "26000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "100" in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario fifty" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser



@HRScenario37_51
  Scenario: 37_51
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario Fifty one" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1855" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home Fully Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 1" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Pakent - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "32000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "250" in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario fifty one" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario37_189
  Scenario: 37_189
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario one hundred eight nine" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Holiday Home Let" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1600" in "Risk Location" page
    And Select "Property Usage" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2*" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "15454545" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "11333333" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "164000" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified Collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "111988" in "Risk Location" page  
    And Select "Which valuable product would you like to cover?" value as "specified Collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "34527" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "54000000" in "Risk Location" page      
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "46030000" in "Risk Location" page   
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "43900" in "Risk Location" page   
   And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7150946" in "Risk Location" page   
    And Enter "Valuables Excess" in the text area as "0" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario one hundred eighty nine" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


    
  @HRScenario37_214
  Scenario: 37_214
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario two hundred fourteen" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Semi Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1860" in "Risk Location" page
    And Select "Property Usage" value as "Other / Business" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Thatched" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "163000500" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "4000" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3200" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario two hundred fourteen" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario38_11
  Scenario: 38_11
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario Eleven" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Bungalow" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1750" in "Risk Location" page
    And Select "Property Usage" value as "Unoccupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "2" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Wattle and Daub" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Pakent - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "675000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "100" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "75000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "12500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6250" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "10714" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "13636" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7895" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "64000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "8374" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "184000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "8400" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "500" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "100" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario Eleven" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser



@HRScenario38_12
  Scenario: 38_12
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario Twelve" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1760" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation Area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Pakent - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "500000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "250" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "80000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "13750" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6666" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "90000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "21000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "16800" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "9820" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5210" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "1000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "250" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario Twelve" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
  @HRScenario38_13
  Scenario: 38_13
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario Thirteen" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Flat" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1777" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home Fully Occupied " from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 1" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "500001" in "Risk Location" page
    And Enter "Building Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "100000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "15000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "8333" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "14286" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "18182" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "13600" in "Risk Location" page
     Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "12200" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "4482" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "other valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "13805" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "5000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
   And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario Thirteen" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
   

 @HRScenario38_52
  Scenario: 38_52
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Scenario Fifty Two" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Unknown" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1865" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home Partially Occupied " from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber Frame/Brick Infill" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Pakent - Non Approved" from the dropdown in the "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "36000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Scenario Fifty two" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
   









 
    