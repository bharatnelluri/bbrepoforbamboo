@MasterTestCase
Feature: ZPC Motor Quote Creation

Background: ZPC login page
Given user is on login page
When user enters username "sriram.c49@wipro.com.muat2"
And user enters password "London@07"
And user clicks on Login button
Then user gets the title of the page
And page title should be "Lightning Experience"

  @MasterTestCase
  Scenario: MasterTestCase
    #Given Verify the Chrome profiling is enabled
    #Then Enter the "zpctest.underwriter@zpc.zpce2e" and "London@01" in salesForce page.
    Then Close all the open tabs
    And Search account "MotorScenario Hundredandtwentyone" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "AUDI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "A6 SPORT TDI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1993" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "4021003" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1896" in "Vehicle Details" page
    And Enter "Value" in the text area as "4999" in "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Enter "Milometer Reading" in the text area as "5000" in "Vehicle Details" page
    And Select "Annual Mileage" value as "0-1000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Select "CountryOfRegistration" value as "UK" from the dropdown in the "Vehicle Details" page
    And Enter "Garaging Postcode" in the type ahead text area as "AB10 7AY" in "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "MotorScenario Hundredandtwentyone" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Select Inception date field date "31-12-2018" in the "Summary/Rating" page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Claim1 - PH
    And Click "New" on the "Related" tab in the "Prior Claims" page
    And Select the Account type as "Motor"
    And Click on the "Next" Button
    And Select "Claim Type" value as "Fault Accident" in "New Prior Claims: Motor" Page
    And Select the "Contact" and provide the Driver last Name details as "MotorScenario Hundredandtwentyone" and Account Name value as "MotorScenario Hundredandtwentyone" in "New Prior Claims: Motor" Page
    #And Enter "Cost" value as "600" in "New Conviction" Page
    And Select date for "Incident Date" field as "01/10/2013" in "New Prior Claims: Motor" Page
    And Click on the "Save" Button
    And Click on the "Close" Icon in "Prior Claims" tab
    And Close the tab "zpc ClaimHistory"
    #Conviction
    And Click "New" on the "Related" tab in the "Convictions" page
    And Select "Type of Conviction" value as "Criminal Conviction" in "New Conviction" Page
    And Select the "Contact" and provide the Driver last Name details as "MotorScenario Hundredandtwentyone" and Account Name value as "MotorScenario Hundredandtwentyone" in "New Contact: Contact" Page
    And Select date for "Conviction Date" field as "01/01/2015" in "New Conviction" Page
    And Enter "Points" value as "1500" in "New Conviction" Page
    And Enter "Fine" value as "500" in "New Conviction" Page
    And Enter "Circumstances" value as "Emergency" in textarea in "New Conviction" Page
    And Search the "Conviction Code" and select the value as "SP30" in "New Conviction" Page
    And Click on the "Save" Button
    And Click on the "Close" Icon in "Coviction" tab
    And Close the tab "Conviction"
    #Verifying Premium
    And Close the tab "Quote"
    And Close the tab "Quotes"
    And Click on the "Overview" link in the "Related" tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    #And Click on page refresh
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser
