@HomeScenario1to20
Feature: ZPC Home rating Scenarios

Background: ZPC login page
Given user is on login page
When user enters username "sriram.c49@wipro.com.muat2"
And user enters password "London@07"
And user clicks on Login button
Then user gets the title of the page
And page title should be "Lightning Experience"

@HRScenario21_44
  Scenario: 21_44
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "AB Regression" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1769" in "Risk Location" page
    And Select "Property Usage" value as "Weekend / Weekday / 2nd Home, Fully Occupied (staff present)" from the dropdown in the "Risk Location" page
    And Select "No of Bathrooms/Wet Rooms" value as "2" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade A" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "50% Flat Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "5800" in "Risk Location" page
	And Select "Contents Excess" value as "500" from the dropdown in the "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium generated
    And Close the browser
    
    
    @HRScenario21_71
  Scenario: 21_71
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1721" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Non Family" from the dropdown in the "Risk Location" page
    And Select "No of Bathrooms/Wet Rooms" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "Pakent - Non Approved" from the dropdown in the "Risk Location" page
     And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "158000" in "Risk Location" page
    And Select "Buildings Excess" value as "500" from the dropdown in the "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    
    @HRScenario22_48
  Scenario: 22_48
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Other" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1816" in "Risk Location" page
    And Select "Property Usage" value as "Other / Business" from the dropdown in the "Risk Location" page
    And Select "No of Bathrooms/Wet Rooms" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Preservation Order" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Other" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "Dualcom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "17730" in "Risk Location" page
   And Select "Contents Excess" value as "10000" from the dropdown in the "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario22_74
  Scenario: 22_74
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Semi Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1757" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Select "No of Bathrooms/Wet Rooms" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber Frame/Brick Infill" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
     And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "285000" in "Risk Location" page
    And Select "Buildings Excess" value as "5000" from the dropdown in the "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Sterling" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    
    @HRScenario23_54
  Scenario: 23_54
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Bungalow" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1887" in "Risk Location" page
    And Select "Property Usage" value as "Weekend / Weekday / 2nd Home, Fully Occupied (staff present)" from the dropdown in the "Risk Location" page
    And Select "No of Bathrooms/Wet Rooms" value as "1" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade A" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "68000" in "Risk Location" page
   And Select "Contents Excess" value as "2500" from the dropdown in the "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario23_173
  Scenario: 23_173
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1804" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Family, Fully Occupied (staff present)" from the dropdown in the "Risk Location" page
    And Select "Number of Bedrooms" value as "6" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 1" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Wattle and Daub" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "16,700,000" in "Risk Location" page
   And Select "Contents Excess" value as "100" from the dropdown in the "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Unspecified jewellery in Bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Total" in the text area as "92600" in "Risk Location" page
    And Select "jewelleryExcess" value as "1000" from the dropdown in the "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
   
    @HRScenario24_170
  Scenario: 24_170
    #Given Verify the Chrome profiling is enabled
    Then Launch the Browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Unknown" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1828" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Family" from the dropdown in the "Risk Location" page
    And Select "No of Bathrooms/Wet Rooms" value as "1" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade C" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Thatched" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "440000" in "Risk Location" page
#    And Enter "Contents Excess" in the text area as "500" in "Risk Location" page
    And Select "Contents Excess" value as "500" from the dropdown in the "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified jewellery in Bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "80000" in "Risk Location" page
    And Select "jewelleryExcess" value as "5000" from the dropdown in the "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails_nextBtn" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
      And Verify the "Home" Premium
#    And Close the browser
    
    
     @HRScenario24_196
  Scenario: 24_196
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1559" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Select "No of Bathrooms/Wet Rooms" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber Frame/Brick Infill" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "None" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Unspecified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Total" in the text area as "280000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    Then click on the "Valuables" button in "Add" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Unspecified Collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Total" in the text area as "1310" in "Risk Location" page
    And Minimize "Valuables 2" tab in "Risk Location" page
    Then click on the "Valuables 2" button in "Add" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Other Valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Total" in the text area as "786252" in "Risk Location" page
    And Minimize "Valuables 3" tab in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "1000" in "Risk Location" page
    And Select "jewelleryExcess" value as "250" from the dropdown in the "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" Premium
    And Close the browser
    
    
    
    @HRScenario25_46
  Scenario: 25_46
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1792" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Family" from the dropdown in the "Risk Location" page
    And Select "Number of Bedrooms" value as "1" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade C" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cobb" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "9493" in "Risk Location" page
   And Select "Contents Excess" value as "2500" from the dropdown in the "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    
    
    
    @HRScenario25_68
  Scenario: 25_68
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Semi Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1665" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Family, Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "No of Bathrooms/Wet Rooms" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cobb" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
     And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "170000" in "Risk Location" page
    And Select "Buildings Excess" value as "5000" from the dropdown in the "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Sterling" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    
    
     @HRScenario25_204
  Scenario: 25_204
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Unknown" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1733" in "Risk Location" page
    And Select "Property Usage" value as "Other / Business" from the dropdown in the "Risk Location" page
    And Select "No of Bathrooms/Wet Rooms" value as "1" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Unspecified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Total" in the text area as "154000" in "Risk Location" page
    And Select "jewelleryExcess" value as "100" from the dropdown in the "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
     @HRScenario25_211
  Scenario: 25_211
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1816" in "Risk Location" page
    And Select "Property Usage" value as "Weekend / Weekday / 2nd Home, Fully Occupied (staff present)" from the dropdown in the "Risk Location" page
    And Select "Number of Bedrooms" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Lath and Plaster" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Unspecified Collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Total" in the text area as "23445600" in "Risk Location" page
    Then click on the "Valuables" button in "Add" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1500" in "Risk Location" page
    And Minimize "Valuables 2" tab in "Risk Location" page
    And Select "Valuables Exces" value as "500" from the dropdown in the "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
 @HRScenario26_63
  Scenario: 26_63
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Flat" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1923" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Family, Fully Occupied (staff present)" from the dropdown in the "Risk Location" page
    And Select "Number of Bedrooms" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 1" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Lath and Plaster" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "90000000" in "Risk Location" page
    And Select "Contents Excess" value as "500" from the dropdown in the "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
     @HRScenario26_64
  Scenario: 26_64
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Bungalow" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2007" in "Risk Location" page
    And Select "Property Usage" value as "Other / Business" from the dropdown in the "Risk Location" page
    And Select "Number of Bedrooms" value as "6" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Preservation Order" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
     And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "45000" in "Risk Location" page
    And Select "Buildings Excess" value as "250" from the dropdown in the "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Sterling" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    @HRScenario26_201
  Scenario: 26_201
    #Given Verify the Chrome profiling is enabled	
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Semi Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1698" in "Risk Location" page
    And Select "Property Usage" value as "Weekend / Weekday / 2nd Home, Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "Number of Bedrooms" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade C" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Other" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "Dualcom - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Unspecified jewellery in Bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Total" in the text area as "755000" in "Risk Location" page
    And Select "jewelleryExcess" value as "2500" from the dropdown in the "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
    
    
     @HRScenario27_62
  Scenario: 27_62
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1912" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Select "Number of Bedrooms" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cobb" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "70000000" in "Risk Location" page
    And Select "Contents Excess" value as "250" from the dropdown in the "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    
     @HRScenario27_65
  Scenario: 27_65
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1805" in "Risk Location" page
    And Select "Property Usage" value as "Unoccupied" from the dropdown in the "Risk Location" page
    And Select "Number of Bedrooms" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber Frame/Brick Infill" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Digicom - Non Approved" from the dropdown in the "Risk Location" page
     And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "55000" in "Risk Location" page
    And Select "Buildings Excess" value as "500" from the dropdown in the "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Sterling" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "Policy Twenty" from global search navigator in the home page
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "ZPC Rating" link in the "Overview" tab
    And Click on the "Validate" Button in "quoteDetails" page
    And Close the tab "ZPC Rating"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
    