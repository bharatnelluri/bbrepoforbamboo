@HomeScenario1to20
Feature: ZPC Home Policy Creation

Background: ZPC login page
Given user is on login page
When user enters username "sriram.c49@wipro.com.muat2"
And user enters password "London@07"
And user clicks on Login button
Then user gets the title of the page
And page title should be "Lightning Experience"
    
  @HomeScenario123
  Scenario: 1
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "first testcase" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Flat" from the dropdown in the "Risk Location" page
    And Enter "Which Floor?" in the text area as "2" in "Risk Location" page
    And Enter "Property Age" in the text area as "2017" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "90000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "54000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Furs" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "55000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "first testcase" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    #And Capture the details for "Home" product
    #And Close the tab "Quote"
    #And Close the tab "Quotes"
    #And Click on the "Overview" tab in the "Quote Details" page
    #And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    #And Close the tab "Rate Motor"
    #And Close the browser

  @HomeScenario2
  Scenario: 2
     #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "second testcase" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2014" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "80000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "5000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "15000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "second testcase" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario3
  Scenario: 3
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "third testcase" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2016" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Non Family" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "12000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "24000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "17800" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    #And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page 
    Then Close all the open tabs
    And Search account "third testcase" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario4
  Scenario: 4
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "four testcase" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    #And Enter "RiskAddressTypeAheadAddress" in the text area as "AB12 5AA" in "Risk Location" page
    And Select "Property Type" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2012" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Non Family" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "75000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "27000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "25000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
		#And Click on the "Next" Button and select "Continue" Popup in "Summary" page 
    Then Close all the open tabs
    And Search account "four testcase" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario5
  Scenario: 5
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "five testcase" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2010" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Family" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "58000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "43000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "70000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "700" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "five testcase" from global search navigator in the home page
     And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
  @HomeScenario6
  Scenario: 6
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "six testcase" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on "Edit" Iframe button next to type ahead "Broker Organisation" in "Quote : Broker" page
    And Enter "Individual Agent" in the text area as "Test Agent" in "Quote : Broker" page
    And Enter "New BusineUnderwriter" in the text area as "Zurich Underwritter" in "Quote : Broker" page
    And Enter "NewBrokerUnderWriterId" in the text area as "Broker Underwriter" in "Quote : Broker" page
    And Enter "RenewalUnderwriter" in the text area as "Tester12" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2002" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "85000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "19000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "18000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1800" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "six testcase" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario7
  Scenario: 7
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "seven testcase" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on "Edit" Iframe button next to type ahead "Broker Organisation" in "Quote : Broker" page
    And Enter "Individual Agent" in the text area as "Test Agent" in "Quote : Broker" page
    And Enter "New BusineUnderwriter" in the text area as "Zurich Underwritter" in "Quote : Broker" page
    And Enter "NewBrokerUnderWriterId" in the text area as "Broker Underwriter" in "Quote : Broker" page
    And Enter "RenewalUnderwriter" in the text area as "Tester" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Select "Property Type" value as "Bungalow" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2016" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Non Family" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "10000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "15000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "24500" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2400" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "seven testcase" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario8
  Scenario: 8
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "eight testcase" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on "Edit" Iframe button next to type ahead "Broker Organisation" in "Quote : Broker" page
    And Enter "Individual Agent" in the text area as "Test Agent" in "Quote : Broker" page
    And Enter "New BusineUnderwriter" in the text area as "Zurich Underwritter" in "Quote : Broker" page
    And Enter "NewBrokerUnderWriterId" in the text area as "Broker Underwriter" in "Quote : Broker" page
    And Enter "RenewalUnderwriter" in the text area as "Tester" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Select "Property Type" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
   And Enter "Property Age" in the text area as "2006" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Wattle and Daub" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "75000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "12500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "24500" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1250" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "eight  testcase" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
  @HomeScenario9
  Scenario: 9
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "nine testcase" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1968" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Let property" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Preservation Order" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "50% Flat Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "75000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "12500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Fine Art" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "24500" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "nine testcase" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario10
  Scenario: 10
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "ten testcase" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2012" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Non Family" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "75000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "50000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Fine Art" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "24000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "ten testcase" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
  @HomeScenario11
  Scenario: 11
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "eleven tests" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as " Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Flat" from the dropdown in the "Risk Location" page
    And Enter "Which Floor?" in the text area as "2" in "Risk Location" page"2" in "Risk Location" page
    And Enter "Property Age" in the text area as "2017" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "75500" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    #And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "60000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Fine Art" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "14500" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1600" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "eleven tests" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    

  @HomeScenario12
  Scenario: 12
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "twelve tests" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2014" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade B" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "27000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    #And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "24000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "50000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "4500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "twelve tests" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario13
  Scenario: 13
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "thirteen tests" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
     And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2016" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Non Family" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "64000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    #And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "74000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "45000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "thirteen tests" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario14
  Scenario: 14
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "fourteen tests" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2012" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Non Family" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "74000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page 
    #And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "57000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "90000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "fourteen tests" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario15
  Scenario: 15
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "fifteen tests" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2010" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Family" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "85000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page 
    #And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "86000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "14000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1400" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "fifteen tests" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario16
  Scenario: 16
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "sixteen tests" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Test Engineer" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
     And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2002" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "85000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page   
    #And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "100000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "15000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1700" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "sixteen tests" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario17
  Scenario: 17
    #Given Verify the Chrome profiling is enabled
    Then Enter the "koushal.patel@wipro.com.mcre2e" and "London@01" in salesForce page.
    Then Close all the open tabs
    And Search account "seventeen tests" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Email" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Bungalow" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2016" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Non Family" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "20000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    #And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "70000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "50000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "seventeen tests" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario18
  Scenario: 18
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "eighteen tests" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2003" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Wattle and Daub" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "58000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    #And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "45000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "70000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "eighteen tests" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser

  @HomeScenario19
  Scenario: 19
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "nineteen tests" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1968" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Let property" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Preservation Order" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "50% Flat Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "75000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    #And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "70000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "75000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "nineteen tests" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
    
  @HomeScenario20
  Scenario: 20
    #Given Verify the Chrome profiling is enabled
     #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "twenty test" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    #And Enter "Broker Organisation" in the type ahead text area as "Broker Test 1" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2011" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Non Family" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "80000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page    
    #And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "90000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Specified Jewellery" from the dropdown in the "Risk Location" page
    And Enter "Valuables Excess" in the text area as "50000" in "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Select Inception date field date "12/12/2019" in the "Summary/Rating" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "twenty test" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser
