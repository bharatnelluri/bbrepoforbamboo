
@HomeScenario81to100
Feature: ZPC Home rating Scenarios

Background: ZPC login page
Given user is on login page
When user enters username "sriram.c49@wipro.com.muat2"
And user enters password "London@07"
And user clicks on Login button
Then user gets the title of the page
And page title should be "Lightning Experience"

@HRScenario42_98
  Scenario:42_98
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Testcase one" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Unknown" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1936" in "Risk Location" page
    And Select "Property Usage" value as "Other/Business" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "15000000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "50000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase one" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario42_99
  Scenario:42_99
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase two" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Semi Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1948" in "Risk Location" page
    And Select "Property Usage" value as "Unoccupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Conservation Area" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "50% Flat Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "18500000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "10000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "60000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Alliance" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase two" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario42_178
  Scenario:42_178
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase three" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Semi Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1663" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home-Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired / linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "14200" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "250" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "5000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2800" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3650" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "12737240" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2087400" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "48000000" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Sterling" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase three" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser



@HRScenario42_179
  Scenario:42_179
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase four" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Bungalow" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1853" in "Risk Location" page
    And Select "Property Usage" value as "Other / Business" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Preservation Order" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Other" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Pakent - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "480000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "11500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "48500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "62500" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "16800" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "12500" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Alliance" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase four" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario42_215
  Scenario: 42_215
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase five" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Bungalow" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1865" in "Risk Location" page
    And Select "Property Usage" value as "Unoccupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "Dualcom - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "109550000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "33987262" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "34000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "34000000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "112500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "7700" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "100" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Azur" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase five" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario44_118
  Scenario:44_118
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase six" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Flat" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1770" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Fully Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 1" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Pakent - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "500001" in "Risk Location" page
    And Enter "Building Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "100000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "2500" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "13600" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "12200" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "4482" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1290" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Other Valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "13805" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Azur" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase six" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario45_148
  Scenario:45_148
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase seven" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Semi Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "2007" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Fully Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "1" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 1" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Other" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "RedCare - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "100000000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "10000" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "8100600" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "145454543" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "125465543" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2766000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "Other Valuables" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "2630686" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "5000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Azur" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase seven" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario46_216
  Scenario:46_216
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase eight" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1876" in "Risk Location" page
    And Select "Property Usage" value as "Main residence" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "6" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Timber Frame/Brick Infill" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Mains wired/linked audible system" in "Risk Location" Page
    And Select "Burglar alarm" value as "Dualcom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "68865340" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Alliance" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase eight" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario46_217
  Scenario:46_217
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase nine" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Flat" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1888" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home - Fully Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "3" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Wattle and Daub" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Centrally monitored fire system" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "3298" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "250" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Sterling" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase nine" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario47_202
  Scenario:47_202
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase ten" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Terrace" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1710" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted-Family" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "6" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Dualcom - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "109097" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1234524" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1566500" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "100000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase ten" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser



@HRScenario47_203
  Scenario:47_203
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase eleven" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1722" in "Risk Location" page
    And Select "Property Usage" value as "Tenanted - Non Family" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Thatched" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "GSM RedCare - NACOSS/SSAIB" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified jewellery in bank" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "77735000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "1000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase eleven" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario48_192
  Scenario:48_192
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase twelve" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Tenants Improvements" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1746" in "Risk Location" page
    And Select "Property Usage" value as "Holiday Home -Partially Occupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Grade 2" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Cedar/Wood Shingle Roof" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "None" in "Risk Location" Page
    And Select "Burglar alarm" value as "Pakent - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified jewellery" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "940000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1409200" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "6250000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "38000" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "10000" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "5000" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase twelve" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser


@HRScenario49_138
  Scenario:49_138
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "testcase one thirtyeight" from global search navigator in the home page
    Then Click the "Home" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Enter "Broker Organisation" in the type ahead text area as "Broker 25" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Answer the Quesions "CCJ" as "No" in "Questions" Page
    And Click on the "Next" Button in "Questions" page
    And Click on the "Next" Button in "Convictions" page
    And Check the "Correspondence Address is the Risk Address?" is checked in "Risk Location" page
    And Answer the Quesions "suffered any loss or damage whether insured or not in the last 5 years" as "No" in "Risk Location" Page
    And Select "Property Type" value as "Detached" from the dropdown in the "Risk Location" page
    And Enter "Property Age" in the text area as "1865" in "Risk Location" page
    And Select "Property Usage" value as "Unoccupied" from the dropdown in the "Risk Location" page
    And Select "NUMBER OF BATHROOMS/WET ROOMS" value as "4" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Is the property ever left unoccupied for more than 60 days at any one time" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings used for any business or professional purposes other than paper work only" as "No" in "Risk Location" Page
    And Answer the Quesions "Is any part of the property open to the general public" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings in a good state of repair" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings undergoing or likely to undergo any building works in the next 12 months?" as "No" in "Risk Location" Page
    And Select "Listed?" value as "Not Listed" from the dropdown in the "Risk Location" page
    And Select "Construction?" value as "Standard" from the dropdown in the "Risk Location" page
    And Answer the Quesions "Are any outbuildings, thatched?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings on a site which has ever flooded" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings within 400 metres of a site which has ever flooded?" as "No" in "Risk Location" Page
    And Answer the Quesions "cliff, riverbank, lake, seafront, reservoir, quarry, excavation, or watercourse?" as "No" in "Risk Location" Page
    And Answer the Quesions "including any outbuildings, ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "suffered damage caused by or resulting from subsidence, heave or landslip" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property, including any outbuildings, within 50 metres of neighbouring properties which have ever suffered damage caused by or resulting from subsidence, heave or landslip?" as "No" in "Risk Location" Page
    And Answer the Quesions "5 lever mortice deadlocks or multi-point locking systems on all external doors" as "No" in "Risk Location" Page
    And Answer the Quesions "fitted with key operated window locks on all ground floor and upper accessible windows?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by an intruder alarm?" as "No" in "Risk Location" Page
    And Answer the Quesions "e.g. electric gates, video entry, CCTV, window grilles, manned security etc?" as "No" in "Risk Location" Page
    And Answer the Quesions "Is the property protected by a fire alarm?" as "Battery operated detectors" in "Risk Location" Page
    And Select "Burglar alarm" value as "Bells - Non Approved" from the dropdown in the "Risk Location" page
    And Check the "Would you like to add Building?" is checked in "Risk Location" page
    And Enter "Buildings Sum Insured" in the text area as "5000000" in "Risk Location" page
    And Enter "Building Excess" in the text area as "500" in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Minimize "Risk Details" tab in "Risk Location" page
    And Check the "Would you like to add Contents?" is checked in "Risk Location" page
    And Enter "Contents Sum Insured" in the text area as "500000" in "Risk Location" page
    And Enter "Contents Excess" in the text area as "0" in "Risk Location" page
    And Minimize "Contents" tab in "Risk Location" page
    And Check the "Would you like to add Valuables?" is checked in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "1500000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified fine arts" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "250000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified collections" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "100000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "unspecified furs" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "125000" in "Risk Location" page
    Then click on the "Add" button in "Valuables" in "Risk Location" page
    And Select "Which valuable product would you like to cover?" value as "specified guns" from the dropdown in the "Risk Location" page
    And Enter "Sum Insured Single Specified Item" in the text area as "25000" in "Risk Location" page
    And Enter "Valuables Excess" in the text area as "250" in "Risk Location" page
    And Enter "Jewellery Excess" in the text area as "0" in "Risk Location" page
    And Minimize "Valuables" tab in "Risk Location" page
    And Click on the "Next" Button in "Risk Location" page
    And Select Inception date field date "01/03/2020" in the "Summary/Rating" page
    And Select "Previous Insurer Name" value as "Aviva" from the dropdown in the "Summary/Rating Information_nextBtn" page
    And Click on the "Next" Button and select "Continue" Popup in "Summary" page
    Then Close all the open tabs
    And Search account "testcase one" from global search navigator in the home page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Home" link in the "Overview" tab
    And Close the tab "Rate Home"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Home" premium
    And Close the browser



