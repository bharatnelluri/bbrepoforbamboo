@Scenario101to120
Feature: Scenario101to120
Background: ZPC login page
Given user is on login page
When user enters username
And user enters password
And user clicks on Login button


  @Scenario101
  Scenario: 101
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredone testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "AUDI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "A4 S4 QUATTRO CABRIOLET" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "4036602" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "4163" in "Vehicle Details" page
    And Enter "Value" in the text area as "240000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "35001-40000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredone testcase" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredone testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/05/1981" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "10/04/2005" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "International" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mathematician" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser    

	
  @Scenario102
  Scenario: 102
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredtwo testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "AUDI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "A3 SE TDI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "4089202" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1598" in "Vehicle Details" page
    And Enter "Value" in the text area as "249999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "0-1000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver 1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredtwo testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredtwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "30/12/1948" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/03/2002" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser    
   

  @Scenario103 @regression1
  Scenario: 103
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredthree testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "BMW" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "520 I SPORT" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2005" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "7039904" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2171" in "Vehicle Details" page
    And Enter "Value" in the text area as "250000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "2001-3000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver 1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredthree testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredthree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/08/1966" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/06/2005" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario104
  Scenario: 104
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredfour testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "CITROEN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "XSARA PICASSO DESIRE HDI (110)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "12035801" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1560" in "Vehicle Details" page
    And Enter "Value" in the text area as "254999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "3001-4000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver 1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredfour testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredfour testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "12/07/1984" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "16/03/2007" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Teacher" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario105
  Scenario: 105
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredfive tests" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "XSARA PICASSO DESIRE HDI (110)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1999" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17568106" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1392" in "Vehicle Details" page
    And Enter "Value" in the text area as "255000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "7001-8000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver 1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredfive tests" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredfive tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "26/08/1974" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/06/2005" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario106
  Scenario: 106
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredsix test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "HYUNDAI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "ACCENT E COUPE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1999" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "22535902" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1341" in "Vehicle Details" page
    And Enter "Value" in the text area as "259999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "8001-9000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver 1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredsix test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Capt" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredsix test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "04/11/1951" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/01/2003" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Firefighter" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario107
  Scenario: 107
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredseven testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MAZDA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "6 TS TD (136)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2005" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "31516303" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1998" in "Vehicle Details" page
    And Enter "Value" in the text area as "260000" in "Vehicle Details" page
    And Select "Use" value as "Class 3" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "10001-11000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver 1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredseven testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredseven testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/08/1966" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "10/04/2005" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Librarian" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario108
  Scenario: 108
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredeight testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "COLT" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "SAPPORO TURBO" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1985" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "34010301" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1997" in "Vehicle Details" page
    And Enter "Value" in the text area as "264999" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "11001-12000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver 1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredeight testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/07/1975" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/05/2004" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Firefighter" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario109
  Scenario: 109
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundrednine testcase" from global search navigator in the home page
    And Click on the "Overview" link in the "Overview" tab
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "PORSCHE" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "968 CLUB SPORT" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1995" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "40035201" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2990" in "Vehicle Details" page
    And Enter "Value" in the text area as "265000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "14001-15000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver 1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundrednine testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundrednine testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/10/1947" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/04/2003" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Librarian" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario110
  Scenario: 110
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredten testcase" from global search navigator in the home page
    And Click on the "Overview" link in the "Overview" tab
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "RENAULT" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "9MEGANE PRIVILEGE DCI (130) CONVERTIBLE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2007" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "41588001" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1870" in "Vehicle Details" page
    And Enter "Value" in the text area as "265000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "15001-16000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    #Driver 1
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredten testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredten testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/09/1944" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/06/2005" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Lawyer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario111 @regression
  Scenario: 111
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredeleven testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VOLVO" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "XC60 SE R-DESIGN DRIVE D5 AWD (205)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2011" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "54623901" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2400" in "Vehicle Details" page
    And Enter "Value" in the text area as "270000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "18001-19000" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredeleven testcase" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredeleven testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "28/01/1942" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/03/2002" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Music Teacher" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "2" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario112
  Scenario: 112
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredtwelve testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VOLVO" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "C30 SE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "54624901" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1999" in "Vehicle Details" page
    And Enter "Value" in the text area as "274999" in "Vehicle Details" page
    And Select "Use" value as "Class 3" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "19001-20000" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredtwelve testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredtwelve testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/03/1981" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "03/04/2009" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mortgage Consultant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario113
  Scenario: 113
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredthirteen tests" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VOLVO" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "V40 R-DESIGN NAV D3 (150)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "54681973" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1984" in "Vehicle Details" page
    And Enter "Value" in the text area as "275000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "20001-25000" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredthirteen tests" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredthirteen tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/07/1975" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "12/08/2004" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario114
  Scenario: 114
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredfourteen testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VOLVO" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "XC60 R-DESIGN D4" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "54682153" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1984" in "Vehicle Details" page
    And Enter "Value" in the text area as "279999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "25001-30000" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredfourteen testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredfourteen testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/03/1989" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/09/2007" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Police Community Support Officer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario115
  Scenario: 115
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredfifteen testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VOLKSWAGEN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "TRANSPORTER T32 SPORTLINE 60 SWB 180" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "90313770" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1968" in "Vehicle Details" page
    And Enter "Value" in the text area as "282000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "30001-35000" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredfifteen testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredfifteen testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/07/1975" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/01/2003" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Teacher" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario116
  Scenario: 116
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredsixteen testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "PEUGEOT" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "PEUGEOT" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "90409061" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1600" in "Vehicle Details" page
    And Enter "Value" in the text area as "284999" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "35001-40000" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredsixteen testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredsixteen testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/04/1979" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/04/2003" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
   And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario117
  Scenario: 117
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredseventeen testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "HYUNDAI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "I10 COMFORT" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "22519101" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1086" in "Vehicle Details" page
    And Enter "Value" in the text area as "285000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "0-1000" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredseventeen testcase" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredseventeen testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/04/1979" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "10/09/2000" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "2" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  
  @Scenario118 @regression1
  Scenario: 118
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundredeighteen testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MAZDA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "323 LXI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "31506601" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1498" in "Vehicle Details" page
    And Enter "Value" in the text area as "285000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "1001-2000" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundredeighteen testcase" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundredeighteen testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/09/1944" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "21/07/2002" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "2" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario119
  Scenario: 119
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneHundrednineteen tests" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MERCEDES-BENZ" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "A 150 SPECIAL EDITION" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "32070103" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1498" in "Vehicle Details" page
    And Enter "Value" in the text area as "290000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "2001-3000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneHundrednineteen tests" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneHundrednineteen tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "05/11/1954" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/05/2004" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser


  @Scenario120
  Scenario: 120
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "Onetwenty testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "NISSAN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MICRA 25 SPECIAL EDITION" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "35544104" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1240" in "Vehicle Details" page
    And Enter "Value" in the text area as "294999" in "Vehicle Details" page
    And Select "Use" value as "Class 3" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured & Spouse" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "11001-12000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "Onetwenty testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "Onetwenty testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "11/09/1962" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "08/10/2001" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mortgage Consultant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser
