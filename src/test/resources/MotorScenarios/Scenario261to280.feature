@scenario261to280
Feature: Scenario261to280

Background: ZPC login page
Given user is on login page
When user enters username
And user enters password
And user clicks on Login button


@Scenario261  
Scenario: 261
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredsixtyone testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MASERATI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "GHIBLI CUP" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "31000201" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1996" in "Vehicle Details" page
    And Enter "Value" in the text area as "865000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "35001-40000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredsixtyone testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario262  
  Scenario: 262
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredsixtytwo testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MAZDA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "3 TS2 NAV" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2011" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "31533701" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1999" in "Vehicle Details" page
    And Enter "Value" in the text area as "870000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "25001-30000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredsixtytwo testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario263
  Scenario: 263
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredsixtythree testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MERCEDES-BENZ" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "CLC 180 KOMPRESSOR SE PANORAMA" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2011" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "32092801" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1796" in "Vehicle Details" page
    And Enter "Value" in the text area as "875000" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "25001-30000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredsixtythree testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario264
  Scenario: 264
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredsixtyfour test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "PORSCHE" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "BOXSTER S BLACK EDITION" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2012" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "40019902" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "3436" in "Vehicle Details" page
    And Enter "Value" in the text area as "884999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "20001-25000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredsixtyfour test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser
  
  @Scenario265
  Scenario: 265
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredsixtyfive test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VOLVO" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "XC90 R-DESIGN PREMIUM AWD" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2010" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "54615601" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "4414" in "Vehicle Details" page
    And Enter "Value" in the text area as "885000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "17001-18000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredsixtyfive test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario266
  Scenario: 266
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredsixtysix testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "GREAT WALL" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "STEED TRACKER D/CAB 143" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2014" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "90312977" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1996" in "Vehicle Details" page
    And Enter "Value" in the text area as "890000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "40001 & higher" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredsixtysix testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario267
  Scenario: 267
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSixtyseven testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "AUDI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "A5 S LINE SPECIAL EDITION QUATTRO TDI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2011" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "4090702" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2967" in "Vehicle Details" page
    And Enter "Value" in the text area as "895000" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "40001 & higher" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSixtyseven testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario268
  Scenario: 268
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSixtyeight newtest" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "BMW" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "123 D SPORT PLUS EDITION COUPE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "7120802" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1995" in "Vehicle Details" page
    And Enter "Value" in the text area as "900000" in "Vehicle Details" page
    And Select "Use" value as "Class 3" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "12001-13000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSixtyeight newtest" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario269
  Scenario: 269
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSixtynine newtest" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "CITROEN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "AX MEMPHIS" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1991" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "12009906" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1124" in "Vehicle Details" page
    And Enter "Value" in the text area as "909999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "40001 & higher" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSixtynine newtest" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario270
  Scenario: 270
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSeventy testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "ESCORT" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1980" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17532003" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1098" in "Vehicle Details" page
    And Enter "Value" in the text area as "910000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "40001 & higher" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSeventy testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario271
  Scenario: 271
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSeventyone testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "HILLMAN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MINX AUTO" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1970" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "20520402" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1725" in "Vehicle Details" page
    And Enter "Value" in the text area as "915000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "35001-40000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSeventyone testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario272  
  Scenario: 272
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSeventytwo testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "KIA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "SOUL HUNTER (138)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "25733201" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1591" in "Vehicle Details" page
    And Enter "Value" in the text area as "920000" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "35001-40000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSeventytwo testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario273  
  Scenario: 273
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSeventythree new" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MERCEDES-BENZ" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "C 200 SPORT EDITION 125 BLUEEF CDI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2012" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "32116603" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1591" in "Vehicle Details" page
    And Enter "Value" in the text area as "925000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "30001-35000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSeventythree new" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario274
  Scenario: 274
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSeventyfour testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "NISSAN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MICRA ACENTA DCI (86)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2010" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "35541902" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1461" in "Vehicle Details" page
    And Enter "Value" in the text area as "934999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "30001-35000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSeventyfour testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario275  
  Scenario: 275
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSeventyfive testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "PEUGEOT" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "504 SUPER LUXE TI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1975" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "39082101" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1971" in "Vehicle Details" page
    And Enter "Value" in the text area as "935000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "25001-30000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSeventyfive testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario276  
  Scenario: 276
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSeventysix testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "SAAB" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "9-3 AERO XWD (280)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2009" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "44534302" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2792" in "Vehicle Details" page
    And Enter "Value" in the text area as "940000" in "Vehicle Details" page
    And Select "Use" value as "Laid Up" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "ACCIDENTAL DAMAGE FIRE & THEFT" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "20001-25000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSeventysix testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario277
  Scenario: 277
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSeventyseven testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "TOYOTA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "YARIS HYBRID T3" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "50071801" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1497" in "Vehicle Details" page
    And Enter "Value" in the text area as "945000" in "Vehicle Details" page
    And Select "Use" value as "Laid Up" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "FIRE & THEFT ONLY" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "25001-30000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSeventyseven testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser
    
  @Scenario278  
  Scenario: 278
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSeventyeight testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VAUXHALL" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "CAVALIER AUTO" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1992" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "52170604" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1598" in "Vehicle Details" page
    And Enter "Value" in the text area as "950000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "20001-25000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSeventyeight testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario279  
  Scenario: 279
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredSeventynine new" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VOLKSWAGEN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "PASSAT S TDI PD (100)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2005" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "53554004" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1896" in "Vehicle Details" page
    And Enter "Value" in the text area as "959999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "20001-25000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredSeventynine new" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario280  
  Scenario: 280
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredEighty testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VOLVO" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "S70 BI-FUEL 10V AUTO" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1999" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "54519401" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2435" in "Vehicle Details" page
    And Enter "Value" in the text area as "960000" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured only to drive" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "30001-35000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredEighty testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser