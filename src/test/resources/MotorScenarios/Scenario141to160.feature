@Scenario141to160
Feature: Scenario141to160
Background: ZPC login page
Given user is on login page
When user enters username
And user enters password
And user clicks on Login button


 @Scenario141 @regression1
  Scenario: 141
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFortyone testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FERRARI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "412 AUTO" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1989" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "16009202" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "4941" in "Vehicle Details" page
    And Enter "Value" in the text area as "345000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "6001-7000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Select "CountryOfRegistration" value as "UK" from the dropdown in the "Vehicle Details" page
    #And Enter "Garaging Postcode" in the type ahead text area as "TR1 3NQ" in "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFortyone testcase" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFortyone testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/09/1944" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/03/2002" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Not In Employment" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "2" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario142
  Scenario: 142
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFortytwo testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FERRARI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "512 I BERLINETTA BOXER" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1984" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "16011201" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "4942" in "Vehicle Details" page
    And Enter "Value" in the text area as "349999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "7001-8000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFortytwo testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFortytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "28/01/1942" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "08/10/2001" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mature Student - Living At Home" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario143
  Scenario: 143
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFortythree testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FERRARI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MONDIAL CABRIOLET" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1986" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "16015601" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2926" in "Vehicle Details" page
    And Enter "Value" in the text area as "350000" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "THIRD PARTY ONLY" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "9001-10000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFortythree testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFortythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "26/08/1974" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/01/2003" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Firefighter" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario144
  Scenario: 144
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFortyfour testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FERRARI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MONDIAL" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1984" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "16016501" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "3186" in "Vehicle Details" page
    And Enter "Value" in the text area as "354000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "10001-11000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFortyfour testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFortyfour testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "25/03/1988" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/07/2006" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Librarian" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario145
  Scenario: 145
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFortyfive testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FIAT" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "STRADA 105 TC" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1983" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "16567501" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1585" in "Vehicle Details" page
    And Enter "Value" in the text area as "355000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "11001-12000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFortyfive testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFortyfive testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/05/1982" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/05/2004" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Lawyer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario146
  Scenario: 146
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFortysix testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FIAT" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "TEMPRA SX IE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1993" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "16574002" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1995" in "Vehicle Details" page
    And Enter "Value" in the text area as "359999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "12001-13000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFortysix testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFortysix testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "21/03/1969" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "10/09/2000" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mathematician" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario147 @regression1
  Scenario: 147
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFortyseven testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MONDEO TITANIUM X TDCI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2010" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17575702" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1753" in "Vehicle Details" page
    And Enter "Value" in the text area as "360000" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "THIRD PARTY FIRE & THEFT" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "13001-14000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFortyseven testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFortyseven testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/05/1983" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "12/08/2004" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Music Teacher" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario148
  Scenario: 148
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFortyeight testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FORD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "ESCORT POPULAR DIESEL" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1992" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "17576502" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1753" in "Vehicle Details" page
    And Enter "Value" in the text area as "360000" in "Vehicle Details" page
    And Select "Use" value as "Class 3" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "14001-15000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFortyeight testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFortyeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "31/10/1985" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/05/2006" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mortgage Consultant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario149
  Scenario: 149
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFortynine testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MAZDA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "121 ZXI 16V" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1997" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "31502302" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1242" in "Vehicle Details" page
    And Enter "Value" in the text area as "365000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "15001-16000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFortynine testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFortynine testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/03/1989" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/09/2007" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Road Sweeper" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario150
  Scenario: 150
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFifty testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MAZDA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "323 EXECUTIVE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1998" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "31502801" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1840" in "Vehicle Details" page
    And Enter "Value" in the text area as "369999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "16001-17000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFifty testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFifty testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "26/08/1974" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/03/2013" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Restaurant Manager" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario151
  Scenario: 151
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFiftyone testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MAZDA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "323 ZXI V6 24V" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1998" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "31503001" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1995" in "Vehicle Details" page
    And Enter "Value" in the text area as "370000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "0-1000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFiftyone testcase" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFiftyone testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "22/06/1954" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/01/1999" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Scaffolder" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "2" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario152 @regression1
  Scenario: 152
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFiftytwo testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MAZDA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "323 GXI 16V" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1998" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "31503801" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1489" in "Vehicle Details" page
    And Enter "Value" in the text area as "374999" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "2001-3000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFiftytwo testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFiftytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "31/10/1982" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/05/2004" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Fast Food Proprietor" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario153
  Scenario: 153
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFiftythree testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "LAMBORGHINI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "GALLARDO SUPERLEGGERA COUPE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "26501201" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "4961" in "Vehicle Details" page
    And Enter "Value" in the text area as "375000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "5001-6000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFiftythree testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFiftythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "25/12/1964" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/01/1999" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Hostess" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario154
  Scenario: 154
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFiftyfour testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "LAMBORGHINI" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "JALPA 350 GTS" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1989" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "26503601" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "3485" in "Vehicle Details" page
    And Enter "Value" in the text area as "379999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "8001-9000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFiftyfour testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFiftyfour testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "14/02/1959" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/01/1999" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Retired" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario155
  Scenario: 155
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFiftyfive testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "LANCIA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "BETA" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1980" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "27502301" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1279" in "Vehicle Details" page
    And Enter "Value" in the text area as "380000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "11001-12000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFiftyfive testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFiftyfive testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/08/1966" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "25/12/1998" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Housewife or Househusband" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario156 @regression1
  Scenario: 156
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFiftysix testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MERCEDES-BENZ" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "A 170 ELEGANCE SE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2008" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "32051803" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1699" in "Vehicle Details" page
    And Enter "Value" in the text area as "384999" in "Vehicle Details" page
    And Select "Use" value as "Laid Up" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "FIRE & THEFT ONLY" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "17001-18000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFiftysix testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFiftysix testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/03/1982" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/10/2009" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Not In Employment" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario157
  Scenario: 157
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFiftyseven testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MERCEDES-BENZ" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "300 E" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1992" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "32052601" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2962" in "Vehicle Details" page
    And Enter "Value" in the text area as "385000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "40001 & higher" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFiftyseven testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFiftyseven testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/05/1983" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/07/2006" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Unemployed" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario158
  Scenario: 158
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFiftyeight testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MERCEDES-BENZ" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "300 TE-24" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1992" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "32053401" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2962" in "Vehicle Details" page
    And Enter "Value" in the text area as "389999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "8001-9000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFiftyeight testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFiftyeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/10/1992" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/10/2009" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mature Student - Living At Home" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario159
  Scenario: 159
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneFiftynine testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MERCEDES-BENZ" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "A 200 ELEGANCE SE CDI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2008" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "32053502" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1992" in "Vehicle Details" page
    And Enter "Value" in the text area as "390000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "11001-12000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneFiftynine testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneFiftynine testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "16/03/1960" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "10/09/2000" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario160 @regression1
  Scenario: 160
    #Given Verify the Chrome profiling is enabled
   #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "OneSixty testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on "Edit" Iframe button next to type ahead "Broker Organisation" in "Quote : Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "MERCEDES-BENZ" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "C 200 ESPRIT" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1995" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "32102201" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1998" in "Vehicle Details" page
    And Enter "Value" in the text area as "394999" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and 1 named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "11001-12000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "OneSixty testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "OneSixty testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "26/08/1974" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "08/10/2001" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Police Community Support Officer" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser
