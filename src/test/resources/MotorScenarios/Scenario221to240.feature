@scenario221to240
Feature: Scenario221to240

Background: ZPC login page
Given user is on login page
When user enters username
And user enters password
And user clicks on Login button


  @Scenario221 @regression1
  Scenario: 221
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredtwentyone tests" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "DATSUN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "260 C" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1975" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "35409501" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2565" in "Vehicle Details" page
    And Enter "Value" in the text area as "665000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "5001-6000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredtwentyone tests" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyone tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "16/03/1960" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "12/03/1978" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mortgage Consultant" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyone tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/09/1956" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "24/03/2015" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Police Community Support Officer" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter complete address for postcode "PO15 6SA"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyone tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/05/1985" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "26/10/2011" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Boiler Maker" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter complete address for postcode "PO15 7JZ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario222  @regression1
  Scenario: 222
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredtwentytwo testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "NISSAN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MICRA XS DCI (65)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2006" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "35526802" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1461" in "Vehicle Details" page
    And Enter "Value" in the text area as "674999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "30001-35000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredtwentytwo testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/03/1928" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "06/03/1946" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Lawyer" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "12/12/1978" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "19/08/1996" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Receptionist" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter complete address for postcode "PO15 6SA"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "22/06/1954" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "17/12/1971" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "International" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Quality Manager" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter complete address for postcode "PO15 7JZ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario223 @regression1
  Scenario: 223
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredtwentythree testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "NISSAN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "PRIMERA SX" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2007" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "35527203" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1998" in "Vehicle Details" page
    And Enter "Value" in the text area as "679999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "35001-40000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredtwentythree testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/06/1971" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/05/1989" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "International" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Music Teacher" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter complete address for postcode "NR3 2PQ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/07/1948" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "28/03/1966" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Cooper" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter complete address for postcode "PO15 6SA"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "12/07/1986" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "06/01/2010" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "IT Manager" in "New Contact: Contact" Page
    #And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter complete address for postcode "PO15 7JZ"
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

 @Scenario224
  Scenario: 224
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredtwentyfour tests" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "TALBOT" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "ALPINE GLS AUTO" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1984" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "49309502" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1592" in "Vehicle Details" page
    And Enter "Value" in the text area as "680000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "6001-7000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredtwentyfour tests" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyfour tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "12/07/1986" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "07/07/2004" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Music Teacher" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyfour tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/01/1984" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "08/09/2001" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Building Society Staff" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyfour tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/04/1970" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "05/10/2004" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Justice Of The Peace" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario225
  Scenario: 225
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredtwentyfive test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "THINK" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "(ELECTRIC)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2002" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "49500101" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "0" in "Vehicle Details" page
    And Enter "Value" in the text area as "685000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "7001-8000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredtwentyfive test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyfive test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/08/1966" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/07/1984" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mortgage Consultant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyfive test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "21/11/1972" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "30/07/1990" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Timber Worker" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyfive test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "22/06/1954" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "17/12/2000" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Book Binder" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario226
  Scenario: 226
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredtwentysix test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "TD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "2000 SILVERSTONE CONVERTIBLE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "49600101" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1998" in "Vehicle Details" page
    And Enter "Value" in the text area as "690000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "13001-14000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredtwentysix test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentysix test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/09/1990" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "05/09/2008" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Road Sweeper" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentysix test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "30/09/1966" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "07/06/1984" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Sail Maker" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentysix test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "05/11/1954" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/05/1985" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Park Attendant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario227
  Scenario: 227
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredtwentyseven tests" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "TD" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "2000 SILVERSTONE CONVERTIBLE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "49600102" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1998" in "Vehicle Details" page
    And Enter "Value" in the text area as "699999" in "Vehicle Details" page
    And Select "Use" value as "Class 3" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "40001 & higher" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredtwentyseven tests" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyseven tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/10/1947" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "26/09/1965" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Restaurant Manager" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyseven tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "28/02/1995" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "24/08/2012" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Magician" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyseven tests" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "28/02/1995" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "24/08/2012" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Biologist" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario228
  Scenario: 228
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredtwentyeight testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "TOYOTA" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "CARINA E SOLAIR AIR CON" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1995" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "50000204" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1587" in "Vehicle Details" page
    And Enter "Value" in the text area as "700000" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "8001-9000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredtwentyeight testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/01/1931" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/03/1949" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Hostess" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "16/06/1980" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "22/02/1998" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Traffic Engineer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentyeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/01/1984" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/06/2001" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Rent Collector" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario229
  Scenario: 229
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredtwentynine test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "PORSCHE" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "PANAMERA PLATINUM EDITION" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2013" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "40035410" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "3604" in "Vehicle Details" page
    And Enter "Value" in the text area as "705000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "15001-16000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredtwentynine test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentynine test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "25/03/1988" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "21/03/2006" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "International" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentynine test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "28/07/1964" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "05/04/1982" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Dietician" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredtwentynine test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "14/02/1959" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "10/08/1976" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Coach Builder" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario230
  Scenario: 230
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredthirty testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "PORSCHE" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "PANAMERA TURBO S" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "40035434" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "4806" in "Vehicle Details" page
    And Enter "Value" in the text area as "710000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "40001 & higher" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredthirty testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirty testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "03/05/1915" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "28/05/1933" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Retired" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirty testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/06/1927" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "20/02/1945" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Glazier" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirty testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/04/1945" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "26/09/1962" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Interpreter" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario231
  Scenario: 231
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredthirtyone testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "PORSCHE" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "CAYENNE PLATINUM EDITION (300)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "40035437" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "3598" in "Vehicle Details" page
    And Enter "Value" in the text area as "719999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "1001-2000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredthirtyone testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyone testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "31/10/1985" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/10/2003" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Housewife or Househusband" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyone testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "14/02/1959" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "10/08/1976" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Computer Manager" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyone testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "14/02/1959" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "10/08/1976" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Barber" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario232 
  Scenario: 232
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredthirtytwo testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "VAUXHALL" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "ZAFIRA LIFE CDTI (125)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "52116501" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1686" in "Vehicle Details" page
    And Enter "Value" in the text area as "720000" in "Vehicle Details" page
    And Select "Use" value as "Class 3" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "THIRD PARTY FIRE & THEFT" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "8001-9000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredthirtytwo testcase" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/04/1970" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "05/04/2014" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Not In Employment" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "2" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "23/06/1942" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "29/02/1960" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Music Teacher" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtytwo testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/04/1950" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "26/09/1967" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Calibration Manager" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario233 
  Scenario: 233
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredthirtythree testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "LIGIER" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "AMBRA GLX AUTO" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2001" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "800201" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "505" in "Vehicle Details" page
    And Enter "Value" in the text area as "725000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Alarmed BSST Garage" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "1001-2000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredthirtythree testcase" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "11/09/1962" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "06/09/1980" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mathematician" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "12/01/1935" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "19/09/1952" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Bank Manager" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "3" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtythree testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/04/1952" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "26/09/1969" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Literary Agent" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario234
  Scenario: 234
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredthirtyfour testlaunch" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "AIXAM" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "500-5 LUXE DIESEL" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2005" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "901701" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "479" in "Vehicle Details" page
    And Enter "Value" in the text area as "730000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Road" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "17001-18000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredthirtyfour testlaunch" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyfour testlaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/03/1957" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "25/02/1975" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Student" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyfour testlaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/07/1955" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "22/03/1973" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Marketing Executive" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyfour testlaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "31/01/1944" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/07/1961" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Nuclear Scientist" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "4" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

 @Scenario235 
  Scenario: 235
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredthirtyfive testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "ALFA ROMEO" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "156 TWIN SPARK" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2001" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "1002601" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1747" in "Vehicle Details" page
    And Enter "Value" in the text area as "735000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "25001-30000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredthirtyfive testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyfive testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "31/01/1982" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/01/2000" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Accountant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyfive testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "29/09/1972" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "07/06/1990" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Zoologist" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyfive testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/04/1953" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "26/09/1970" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "International" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Shop Keeper" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "5" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver5" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyfive testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/09/1944" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/04/1990" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Justice Of The Peace" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PB" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "5" value as "Driver5" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario236 
  Scenario: 236
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredthirtysix test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "BENTLEY" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "MK VI" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1952" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "6005001" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "4257" in "Vehicle Details" page
    And Enter "Value" in the text area as "740000" in "Vehicle Details" page
    And Select "Use" value as "Class 1" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Manned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "1001-2000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredthirtysix test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtysix test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/03/1930" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "05/03/1948" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Soldier" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtysix test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/06/1980" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "21/02/1998" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Lawyer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtysix test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/04/1963" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "01/01/2015" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Retired" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "5" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver5" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtysix test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "12/04/1978" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "18/06/1996" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Book Binder" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PB" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "5" value as "Driver5" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

 @Scenario237
  Scenario: 237
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredthirtyseven testfulllaunch" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FIAT" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "126" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1977" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "16506501" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "649" in "Vehicle Details" page
    And Enter "Value" in the text area as "745000" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Secure Car Park (Unmanned)" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "9001-10000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredthirtyseven testfulllaunch" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyseven testfulllaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "28/01/1942" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "24/01/2009" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Firefighter" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyseven testfulllaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "06/09/1987" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "14/05/2005" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Actor or Actress" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyseven testfulllaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "31/01/1995" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/07/2012" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Careers Officer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "5" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver5" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyseven testfulllaunch" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "03/05/1998" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "18/11/2015" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Park Attendant" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PB" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "5" value as "Driver5" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario238 
  Scenario: 238
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredthirtyeight testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "FSO" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "POLONEZ SLE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1991" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "19005701" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "1598" in "Vehicle Details" page
    And Enter "Value" in the text area as "754999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Underground Parking" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "18001-19000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredthirtyeight testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/10/1992" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "27/09/2010" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Librarian" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/07/1995" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "22/03/2013" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Floor Layer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "28/01/1996" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "03/03/2014" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Night Watchman or Woman" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "5" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver5" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtyeight testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "28/01/1996" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "03/03/2014" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Biologist" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PB" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "5" value as "Driver5" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Verifying Premium
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario239 
  Scenario: 239
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredthirtynine testcase" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "GEM" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "E2 (ELECTRIC)" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "2015" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "19300101" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "0" in "Vehicle Details" page
    And Enter "Value" in the text area as "759999" in "Vehicle Details" page
    And Select "Use" value as "SDP" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "Drive behind Electric Gates" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "18001-19000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Private" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredthirtynine testcase" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtynine testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "01/05/1991" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "26/04/2009" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Lawyer" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtynine testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "09/04/1992" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "16/12/2009" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Pastry Chef" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtynine testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/05/1983" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "08/11/2000" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Customer Advisor" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "5" in the "Driver" Popup Page
    And Select "Salutation" value as "His" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver5" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredthirtynine testcase" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "16/03/1980" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "23/08/2000" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Provisional (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Rent Collector" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PB" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "5" value as "Driver5" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser

  @Scenario240
  Scenario: 240
    #Given Verify the Chrome profiling is enabled
    #Then Launch the browser and enter valid credentials
    Then Close all the open tabs
    And Search account "TwoHundredforty test" from global search navigator in the home page
    Then Click the "Motor" icon from "Overview" tab
    And Click on the "Next" Button in "Customer" page
    And Select "Channel" value as "Phone" from the dropdown in the "Broker" page
    And Click on the "Next" Button in "Broker" page
    And Select Inception date field date "01/01/2022" in the "Questions" page
    And Click on the "Next" Button in "Questions" page
    And Answer the Quesions "Registration" as "No" in "Vehicle Details" Page
    And Enter "Vehicle_Make" in the text area as "GILBERN" in "Vehicle Details" page
    And Enter "Vehicle_Model" in the text area as "GENIE" in "Vehicle Details" page
    And Enter "Manufactured_Year" in the text area as "1971" in "Vehicle Details" page
    And Enter "ABICode" in the text area as "19502101" in "Vehicle Details" page
    And Enter "EngineSize" in the text area as "2994" in "Vehicle Details" page
    And Enter "Value" in the text area as "760000" in "Vehicle Details" page
    And Select "Use" value as "Class 3" from the dropdown in the "Vehicle Details" page
    And Select "Cover Type" value as "COMPREHENSIVE" from the dropdown in the "Vehicle Details" page
    And Select "Tracking Device" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Driving Restriction" value as "Insured and named" from the dropdown in the "Vehicle Details" page
    And Select "Right Hand Drive" value as "Yes" from the dropdown in the "Vehicle Details" page
    And Select "Modifications" value as "No" from the dropdown in the "Vehicle Details" page
    And Select "Garaging" value as "BSST Garaged" from the dropdown in the "Vehicle Details" page
    And Select "Annual Mileage" value as "12001-13000" from the dropdown in the "Vehicle Details" page
    And Select "Vehicle Type" value as "Classic" from the dropdown in the "Vehicle Details" page
    And Click on the "Next" Button in "Vehicle Details" page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Map the Driver "1" value as "TwoHundredforty test" from "Driver" Popup Page
    And select drivertype "Main Driver" for driver "1" from "Driver" Popup Page
    And select drivertype "Policy Holder" for driver "1" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "2" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver2" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredforty test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "20/03/1989" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "16/03/2007" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Mathematician" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PQ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "2" value as "Driver2" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "3" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver3" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredforty test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "04/08/1993" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "12/04/2011" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Tax Analyst" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 6SA" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "3" value as "Driver3" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "4" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver4" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredforty test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "15/05/1983" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "08/11/2000" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "Full (UK)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Customer Advisor" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "PO15 7JZ" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "4" value as "Driver4" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "New" Button from the Driver Icon in "Driver" page
    And Click on "New Driver" hyperlink for the driver "5" in the "Driver" Popup Page
    And Select "Salutation" value as "Her" in "New Contact: Contact" Page
    And Enter "First Name" field randomly value as "Motor" in "New Contact: Contact" Page
    And Enter "Last Name" field randomly value as "Driver5" in "New Contact: Contact" Page
    And Enter the "Account Name" and select the value as "TwoHundredforty test" in "New Contact: Contact" Page
    And Select date for "Date of Birth" field as "10/04/1996" in "New Contact: Contact" Page
    And Select date for "Licence Year" field as "20/10/2014" in "New Contact: Contact" Page
    And Select "Type of Licence" value as "EEC (EU)" in "New Contact: Contact" Page
    And Select "Preferred Contact Method" value as "Telephone" in "New Contact: Contact" Page
    And Enter the "Occupation Search" and select the value as "Coach Builder" in "New Contact: Contact" Page
    And Enter "Mailing Address" from lookup and search value "NR3 2PB" in the Account Information page
    And Enter "Mailing House Name/Number" field randomly value as "50" in "New Contact: Contact" Page
    And Click on the "Save" Button
    And Close the tab "Contact"
    And Map the Driver "5" value as "Driver5" from "Driver" Popup Page
    And Click on "Save" from "Driver" Popup Page
    And Click on the "Next" Button in "Motor Driver" page
    And Select "Previous Insurer Name" value as "Chartis" from the dropdown in the "Vehicle Insurance" page
    And Click on the "Next" Button in Vehicle Insurance page
    And Click on the "Done" Button in "Summary/Rating" page
    And Click on the "Related" link in the "Overview" tab
    And Select the QuoteNumber from "Related" tab
    And Click on the "QuoteNumber" from the list
    And Capture the Quote Number
    #Calculating Premium at the end
    And Close the tab "Quote"
    And Click on the "Overview" link in the "Related" tab
    And Click on the Refresh button in the Overview tab
    And Get the "QuoteNumber" from Application and Click "Rate Motor" link in the "Overview" tab
    And Close the tab "Rate Motor"
    And Get the "QuoteNumber" from Application and Click "View Details" link in the "Overview" tab
    And Navigate to "Details" tab in the "Quote Details" Page
    And Verify the "Motor" premium
    And Close the browser
