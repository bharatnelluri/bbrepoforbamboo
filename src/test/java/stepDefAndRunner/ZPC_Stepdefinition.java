package stepDefAndRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
//import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
//import java.util.HashMap;
import java.util.Iterator;
//import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.*;
import com.pages.Functional_Library;
//import com.pages.LoginPage;
import com.qa.factory.DriverFactory;
import com.qa.util.EncryptAndDecrypt;

public class ZPC_Stepdefinition {

	public  String accountNumberrandom =null;
	public  String driverLastNamerandom = null;
	public  String capturedValue =new String();
	public  String accountNamerandom =null;
	
	//My elements
	public  WebElement iFrame;
	public  String actual_premium;
	public  String env;
	public  String url;
	public  String username;
	public  String password;
	//public  String scenario;
	public  double expected_premium;
	public   String scenario_name;
	public  double diff;
	public  String status;
	public  String file_path;
	public  String project_path;
	public  String tab;
	public  String QuoteNumber="";
	public Scenario scenario;
	public String ScenarioOrTestCaseNum;
	public EncryptAndDecrypt encrypt;


	private Functional_Library functionalLib = new Functional_Library(DriverFactory.getDriver());
	
	public Random rand= new Random();
	static DateFormat date_time = new SimpleDateFormat("dd_MM_YYYY_HHmmss");
	DateFormat dateFormat2 = new SimpleDateFormat("dd_MM_yyyy_HHmmss");

	static Date date1 = new Date();

	//Execution start date and time //Execution date and time
	protected static String execution_start_date_time = date_time.format(date1);
	
	Calendar cal = Calendar.getInstance();
	String month = new SimpleDateFormat("MMM").format(cal.getTime());
	
	
	@Before(order = 1)
	 public void scenarioName(Scenario scenario) { 
		scenario_name =	 scenario.getName(); //return scenario_name; 
	 }
	 
		
		
		
//	@Before(order = 2) public void saveEncryptedADPassword(){ 
//		try{
//
//			HashMap<String, String> paramMap = new HashMap<String, String>();
//
//			String filePath = System.getProperty("user.dir")+"\\src\\test\\resources\\config\\config.properties"; 
//			encrypt = new EncryptAndDecrypt(); 
//			String password = encrypt.getRunTimeInputText("Enter password", true);
//			paramMap.put("password",password); 
//			encrypt = new EncryptAndDecrypt();
//			encrypt.saveDataToPropertiesFile(filePath, paramMap); 
//		}catch(Exception t){
//
//		}
//
//	}
		 
		 
	public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{

        //Convert web driver object to TakeScreenshot

        TakesScreenshot scrShot =((TakesScreenshot)webdriver);

        //Call getScreenshotAs method to create image file

                File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

            //Move image file to new destination

                File DestFile=new File(fileWithPath);

                //Copy file at destination

                FileUtils.copyFile(SrcFile, DestFile);

    }
	public static String dateTimeGenerate(){
	    Format formatter = new SimpleDateFormat("YYYYMMdd_HHmmssSSS");
	    Date date = new Date(System.currentTimeMillis());
	   return formatter.format(date);
	} 
	
	@After(order = 1)
	public void tearDown(Scenario scenario) throws Exception {
		if (scenario.isFailed()) {
			/*
			 * String screenshotName = scenario.getName().replaceAll(" ",
			 * "_")+"_"+dateTimeGenerate(); TakesScreenshot scrShot
			 * =((TakesScreenshot)driver); //File
			 * SrcFile=scrShot.getScreenshotAs(OutputType.FILE); scrShot.
			 * 
			 * byte[] sourcePath = ((TakesScreenshot)
			 * driver).getScreenshotAs(OutputType.BYTES); scenario.attach(sourcePath,
			 * "image/png", screenshotName);
			 */
			String destFile = System.getProperty("user.dir")+"\\ScreenShots\\"+ScenarioOrTestCaseNum+"_"+dateTimeGenerate()+".png";
			String destFile1 = System.getProperty("user.dir")+"\\ScreenShots\\"+scenario.getName()+"_"+dateTimeGenerate()+".png";
			if(ScenarioOrTestCaseNum!=null) {
			this.takeSnapShot(DriverFactory.getDriver(), destFile);
			}else
			{
				this.takeSnapShot(DriverFactory.getDriver(), destFile1);
			}

		}
	}
	
		
	@Then("^Select the Existing Account \"(.*?)\" from the \"(.*?)\" Page$")
	public void select_the_Existing_Account_from_the_Page(String accountName, String page) throws Throwable {
		try {
			DriverFactory.getDriver().findElement(By.xpath("//table[contains(@class,'VirtualDataTable')]//a[contains(@title,'"+accountName+"')]")).click();
			System.out.println("Select the Existing account");
		}catch(Exception e) {
			Assert.fail("Unable to select the Existing Amount");
		}
	}

	@Then("^Select the Account type as \"(.*?)\"$")
	public void select_the_Account_type_as(String accountType) throws Throwable {
		try{
			DriverFactory.getDriver().manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS) ;
			DriverFactory.getDriver().findElement(By.xpath("//span[contains(@class,'radio--label') and contains(text(),'"+accountType+"')]/preceding-sibling::span")).click();	
		}catch(Exception e){
			Assert.fail("unable to select on"+accountType+" "+e.getMessage());	
		}
	}

	@Then("^Select the dropdown from the \"(.*?)\" menu$")
	public void select_the_dropdown_from_the_menu(String DropdownMenu) throws Throwable {
		functionalLib.selectmenu(DropdownMenu);
	}

	@Then("^Enter the \"(.*?)\" and \"(.*?)\" in salesForce page\\.$")
	public void enter_the_and_in_salesForce_page(String username, String password) throws Throwable {
		functionalLib.loginSalesforce(username, password);
	}

	@Then("^Close all the open tabs$")
	public void close_all_the_open_tabs() throws Throwable {
		functionalLib.closeOpenTab();
	}

	@Then("^Click on the \"(.*?)\" Button$")
	public void click_on_the_Button(String buttonName) throws Throwable {
		functionalLib.clickButton(buttonName);
		functionalLib.waitforspinnerincontact();
	}

	@Then("^Enter the \"(.*?)\" and select the value as \"(.*?)\" in the Account Information page$")
	public void enter_the_and_select_the_value_as_in_the_Account_Information_page(String fieldName, String value) throws Throwable {
		int rand_int1 = rand.nextInt(1000);     
		value=value+rand_int1;
		accountNumberrandom=value;
		functionalLib.selectAutoPopup(fieldName, accountNumberrandom);
	}

	@Then("^Select \"(.*?)\" value as \"(.*?)\" from the dropdown in the Account Information page$")
	public void select_value_as_from_the_dropdown_in_the_Account_Information_page(String fieldName, String value) throws Throwable {
		functionalLib.selectDropdown(fieldName, value);
	}

	@Then("^Enter \"(.*?)\" in the text area as \"(.*?)\" in the Account Information page$")
	public void enter_in_the_text_area_as_in_the_Account_Information_page(String textField, String inputvalue) throws Throwable {
		functionalLib.enterText(textField, inputvalue);
	}

	@Then("^Click the \"(.*?)\" icon from \"(.*?)\" tab$")
	public void click_the_icon_from_tab(String value, String tabName) throws Throwable {
		Thread.sleep(4000);
		functionalLib.selectInsuranceType(value);
	}

	@Then("^Check the \"(.*?)\" is checked in \"(.*?)\" page$")
	public void check_the_is_checked_in_page(String checkBoxName, String pageName) throws Throwable {
		functionalLib.clickCheckBoxInframe(checkBoxName);
	}

	// Entering values in Iframe
	@Then("^Enter \"(.*?)\" in the text area as \"(.*?)\" in \"(.*?)\" page$")
	public void enter_in_the_text_area_as_in_page(String textField, String inputvalue, String pageName) throws Throwable {
		functionalLib.enterTextInIframe(textField, inputvalue,pageName);
	}

	@Then("^Select \"(.*?)\" as \"(.*?)\" from Calender in the \"(.*?)\" page$")
	public void select_as_from_Calender_in_the_page(String fieldName, String date, String pageName) throws Throwable {
		functionalLib.selectDate(fieldName, date,pageName);
	}

	@Then("^Click on the \"(.*?)\" Button in \"(.*?)\" page$")
	public void click_on_the_Button_in_page(String buttonName, String pageName) throws Throwable {
		functionalLib.clickButtonInIframe(buttonName,pageName);
		functionalLib.	waitforspinnerinframe();
	}

	@Then("^Select \"(.*?)\" value as \"(.*?)\" from the dropdown in the \"(.*?)\" page$")
	public void select_value_as_from_the_dropdown_in_the_page(String fieldName, String value, String pageName) throws Throwable {
		DriverFactory.getDriver().manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		//Thread.sleep(7000);
		if(fieldName.equalsIgnoreCase("Use")||fieldName.equalsIgnoreCase("Garaging")) {
			functionalLib.selectDropdownforGarageAndUse(fieldName, value, pageName);
		}else {
			functionalLib.selectDropdownInIframe(fieldName, value,pageName);
		}
	}

	@Then("^Answer the Quesions \"(.*?)\" as \"(.*?)\" in \"(.*?)\" Page$")
	public void answer_the_Quesions_as_in_Page(String Question, String Answer, String pageName) throws Throwable {
		functionalLib.answerQuestion(Question, Answer);
	}

	@Then("^Enter \"(.*?)\" as \"(.*?)\" in the \"(.*?)\" page$")
	public void enter_as_in_the_page(String textField, String inputvalue, String pageName) throws Throwable {
		functionalLib.entervalueAndSearchInIframe(textField, inputvalue,pageName);
	}

	@Then("^Click on \"(.*?)\" Iframe button next to type ahead \"(.*?)\" in \"(.*?)\" page$")
	public void click_on_Iframe_button_next_to_type_ahead_in_page(String buttonName, String typeAheadField, String pageName) throws Throwable {
		functionalLib.clickOnEditAfterTypeAheadIframe(buttonName, typeAheadField);
	}

	@Then("^Select the \"(.*?)\" Popup in the \"(.*?)\" page$")
	public void select_the_Popup_in_the_page(String alertName, String pageName) throws Throwable {
		functionalLib.handleAlerts(alertName);
	}

	@Then("^Enter \"(.*?)\" in the type ahead text area as \"(.*?)\" in \"(.*?)\" page$")
	public void enter_in_the_type_ahead_text_area_as_in_page(String typeAheadName, String value, String pageName) throws Throwable {
		functionalLib.enterTypeAheadIframe(typeAheadName,value);
	}

	@Then("^Click on the \"(.*?)\" Button in Vehicle Insurance page$")
	public void click_on_the_Button_in_Vehicle_Insurance_page(String buttonName) throws Throwable {
		iFrame = DriverFactory.getDriver().findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		DriverFactory.getDriver().switchTo().frame(iFrame);
		WebElement element = DriverFactory.getDriver().findElement(By.xpath("(//div[contains(@id,'nextBtn') and contains(@id,'Vehicle')]/p[text()='"+buttonName+"'])[2]"));
		if(functionalLib.isClickable(element) == true)
		{

		}
		else
		{
			functionalLib.findelementinframe(element,iFrame);
		}
		DriverFactory.getDriver().switchTo().defaultContent();
		functionalLib.waitforspinnerinframe();
	}

	@Then("^Click on the \"(.*?)\" link in the \"(.*?)\" tab$")
	public void click_on_the_link_in_the_tab(String linkName, String tabName) throws Throwable {
		functionalLib.linkIframe(linkName);
	}

	@Then("^Verify the message \"(.*?)\" has been dispaleyd in the \"(.*?)\" tab$")
	public void verify_the_message_has_been_dispaleyd_in_the_tab(String message, String tabName) throws Throwable {
		functionalLib.verifyMessage(message, tabName);
	}

	@Then("^Click on the \"(.*?)\" Icon in \"(.*?)\" tab$")
	public void click_on_the_Icon_in_tab(String tabName, String pageName)  throws Throwable {
		functionalLib.closetheFlashMessage(tabName);
	}

	@Then("^Close the tab \"(.*?)\"$")
	public void close_the_tab(String tabName) throws Throwable {
		if(tabName.equals("Rate Motor")) {
			functionalLib.verifyRating();
			System.out.println("");
		}
		else
		{
			if(tabName.equals("Contact")) {
				Thread.sleep(10000);
			}
			functionalLib.closesubtab(tabName);	
		}
	}

	@Then("^Click on the \"(.*?)\" tab in the \"(.*?)\" page$")
	public void click_on_the_tab_in_the_page(String tabName, String pageName) throws Throwable {
		tab = DriverFactory.getDriver().getWindowHandle();
		DriverFactory.getDriver().switchTo().window(tab);
		DriverFactory.getDriver().findElement(By.xpath("(//span[@class='title' and contains(text(),'"+tabName+"')])[2]")).click();
		DriverFactory.getDriver().switchTo().defaultContent();
		Thread.sleep(2000);
	}

	@Then("^Click on the \"(.*?)\" Button from the Driver Icon in \"(.*?)\" page$")
	public void click_on_the_Button_from_the_Driver_Icon_in_page(String buttonName, String pageName) throws Throwable {
		functionalLib.clickDriverIcon(buttonName);
	}

	// Newly Added By Arunn

	@When("^Click on \"(.*?)\" hyperlink for the driver \"(.*?)\" in the \"(.*?)\" Popup Page$")
	public void click_on_hyperlink_for_the_driver_in_the_Popup_Page(String buttonName,int driver_number,String pageName) throws Throwable {
		functionalLib.clickpopupHyperLink(buttonName,driver_number,pageName);      
	}

	@When("^Select \"(.*?)\" value as \"(.*?)\" in \"(.*?)\" Page$")
	public void select_value_as_in_Page(String dropdownName, String value, String pageName) throws Throwable {	
		functionalLib.selectdropdownfromhyperlink(dropdownName,value);
	}

	@When("^Select date for \"(.*?)\" field as \"(.*?)\" in \"(.*?)\" Page$")
	public void select_date_for_field_as_in_Page(String fieldName, String date, String pageName) throws Throwable {
		functionalLib.selectDateinDriverpage(fieldName,date);
	}

	@Then("^Map the Driver \"(.*?)\" value as \"(.*?)\" from \"(.*?)\" Popup Page$")
	public void map_the_Driver_value_as_from_Popup_Page(int driver_number, String driverName, String pageName) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		if(driver_number==1) {
			functionalLib.Mapdriverdetails(driver_number,driverName);
		}else {
			functionalLib.Mapdriverdetails(driver_number,driverLastNamerandom);
		}
	}

	@When("^Click on \"(.*?)\" from \"(.*?)\" Popup Page$")
	public void click_on_from_Popup_Page(String popupValue, String pageName) throws Throwable {
		functionalLib.selectpopvalue(popupValue);
	}

	@When("^Click \"(.*?)\" on the \"(.*?)\" tab in the \"(.*?)\" page$")
	public void click_on_the_tab_in_the_page(String buttonName, String tabName, String recordType) throws Throwable {
		functionalLib.createRecordinRelatedtab(buttonName,recordType);   
	}

	@When("^Enter \"(.*?)\" value as \"(.*?)\" in \"(.*?)\" Page$")
	public void enter_value_as_in_Page(String textField, String inputvalue, String pageName) throws Throwable {
		functionalLib.enterText(textField, inputvalue);
	}

	@When("^Enter \"(.*?)\" value as \"(.*?)\" in textarea in \"(.*?)\" Page$")
	public void enter_value_as_in_textarea_in_Page(String textField, String inputvalue, String pageName) throws Throwable {
		functionalLib.enterDetailsinTextArea(textField, inputvalue);
	}

	@When("^Search the \"(.*?)\" and select the value as \"(.*?)\" in \"(.*?)\" Page$")
	public void search_the_and_select_the_value_as_in_Page(String fieldName, String value, String pageName) throws Throwable {
		functionalLib.selectCodefromSearchPopup(fieldName,value);
	}


	@When("^Capture the \"(.*?)\" value in the \"(.*?)\" tab$")
	public void capture_the_value_in_the_tab(String fieldName, String pageName) throws Throwable {
		capturedValue =functionalLib.capturevaluesinQuotedetailspage(fieldName);
	}

	@When("^Close the tab \"(.*?)\" of \"(.*?)\" detail page$")
	public void close_the_tab_of_detail_page(String tabName, String pageName) throws Throwable {
		functionalLib.closeDriverdetailTab(driverLastNamerandom);
	}

	@When("^Enter \"(.*?)\" in the text area as \"(.*?)\" in \"(.*?)\" Page$")
	public void enter_in_the_text_area_as_in_Page(String textField, String inputvalue, String pageName) throws Throwable {
		functionalLib.enterText(textField, inputvalue);
	}

	@When("^Enter the \"(.*?)\" and select the value as \"(.*?)\" in \"(.*?)\" Page$")
	public void enter_the_and_select_the_value_as_in_Page(String fieldName, String value, String pageName) throws Throwable {
		functionalLib.selectAutoPopup(fieldName, value);
	}

	@Then("^Enter \"(.*?)\" from lookup and search value \"(.*?)\" in the Account Information page$")
	public void enter_from_lookup_and_search_value_in_the_Account_Information_page(String fieldName, String lookupValue) throws Throwable {
		functionalLib.searchLookUpSearchAddress(fieldName,lookupValue);
	}

	@Then("^select on \"(.*?)\" hyperlink for the driver \"(.*?)\" with driver type as \"(.*?)\" in the \"(.*?)\" Popup Page$")
	public void select_on_hyperlink_for_the_driver_with_driver_type_as_in_the_Popup_Page(String buttonName, int driver_number, String driverType, String pageName) throws Throwable {
		functionalLib.clickpopupHyperLinkwithdriverLink(buttonName, driver_number, driverType);
	}


	@Then("^Fetch \"(.*?)\" from the \"(.*?)\" page$")
	public void fetch_from_the_page(String buttonName, String pageName) throws Throwable {
		functionalLib.fetchGetVehicleDetails(buttonName);
		Thread.sleep(2000);
	}

	@Then("^Navigate to \"(.*?)\" tab in the \"(.*?)\" Page$")
	public void navigate_to_tab_in_the_Page(String tabName, String pageName) throws Throwable {
		functionalLib.NavigatetabinQuoteDetailspage(tabName);
	}

	@Then("^Select the \"(.*?)\" and provide the Driver last Name details as \"(.*?)\" and Account Name value as \"(.*?)\" in \"(.*?)\" Page$")
	public void select_the_and_provide_the_Driver_last_Name_details_as_and_Account_Name_value_as_in_Page(String fieldName, String driverName, String accountName, String pageName) throws Throwable {
		if(fieldName.equalsIgnoreCase("contact")) {
			functionalLib.selectContact(fieldName, accountName);
		}else {
			functionalLib.selectContactDetailsFromLookup(fieldName, driverLastNamerandom, accountName, pageName);	
		}
	}


	@Then("^Enter Searchlookup  \"(.*?)\" field with \"(.*?)\" in \"(.*?)\" page$")
	public void enter_Searchlookup_field_with_in_page(String fieldName, String value, String pageName) throws Throwable {
		functionalLib.clickautocompleteValue(fieldName,value,pageName);
	}

	@Then("^Click on the \"(.*?)\" check button in \"(.*?)\" page$")
	public void click_on_the_check_button_in_page(String checkBoxFieldName, String pageName) throws Throwable {
		functionalLib.clickcheckBox(checkBoxFieldName);
	}

	@Then("^Select Inception date field date \"(.*?)\" in the \"(.*?)\" page$")
	public void select_Inception_date_field_date_in_the_page(String date, String pageName) throws Throwable {
		functionalLib.selectInceptionDate(date);
	}


	@Then("^Click on \"(.*?)\" and verify the System Premium is generated$")
	public void click_on_and_verify_the_System_Premium_is_generated(String tabName) throws Throwable {
		functionalLib.clicksystemPremiumInRateMotor(tabName);
	}

	@Then("^select drivertype \"(.*?)\" for driver \"(.*?)\" from \"(.*?)\" Popup Page$")
	public void select_drivertype_for_driver_from_Popup_Page(String driverType, int driverNumber, String pageName) throws Throwable {
		functionalLib.selectDriverType(driverType,driverNumber);
	}

	@Then("^Close the browser$")
	public void close_the_browser() throws Throwable {
		
		DriverFactory.getDriver().quit();
	}

	@Then("^Enter \"(.*?)\" value in the address search and select value as \"(.*?)\" in \"(.*?)\" page$")
	public void enter_value_in_the_address_search_and_select_value_as_in_page(String textField, String inputvalue, String pageName) throws Throwable {
		functionalLib.selctAddressfromPopUp(textField, inputvalue,pageName);
	}

	@Then("^Enter \"(.*?)\" field randomly value as \"(.*?)\" in \"(.*?)\" Page$")
	public void enter_field_randomly_value_as_in_Page(String textField, String inputvalue, String pageName) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		int rand_int1 = rand.nextInt(1000);    
		if(textField.equalsIgnoreCase("Last Name")){
			inputvalue=inputvalue+rand_int1;
			driverLastNamerandom=inputvalue;
			functionalLib.enterText(textField, driverLastNamerandom);
		}else{
			functionalLib.enterText(textField, inputvalue);
		}

	}


	@Then("^Click on page refresh$")
	public void click_on_page_refresh() throws Throwable {
		DriverFactory.getDriver().navigate().refresh();
	}

	@Then("^Select \"(.*?)\" value from scroll page name \"(.*?)\" in \"(.*?)\" Page$")
	public void select_value_from_scroll_page_name_in_Page(String fieldName, String multiselectValue, String pageName) throws Throwable {
		functionalLib.multiselectClaimPage(fieldName, multiselectValue, pageName);
	}

	@Then("^Click on the \"(.*?)\" Button and select \"(.*?)\" Popup in \"(.*?)\" page$")
	public void click_on_the_Button_and_select_Popup_in_page(String buttonName, String alertName, String pageName) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		functionalLib.clickDoneButtonInIframe(buttonName,alertName, pageName);
	}

	@Then("^Click on the \"(.*?)\" hyperlink in the \"(.*?)\" menu in the \"(.*?)\" tab$")
	public void click_on_the_hyperlink_in_the_menu_in_the_tab(String linkName, String tabName, String pageName) throws Throwable {
		functionalLib.clickandnavigateinQuotedetails(linkName, tabName);
	}

	@Then("^Click on the \"(.*?)\" and navigate  in the \"(.*?)\" tab$")
	public void click_on_the_and_navigate_in_the_tab(String fieldName, String tabName) throws Throwable {
		functionalLib.clickvaluesinQuotedetailspage(fieldName);
	}

	@Then("^Click on the \"(.*?)\" Button in \"(.*?)\" tab$")
	public void click_on_the_Button_in_tab(String buttonName, String pageName) throws Exception {
		functionalLib.clickRefresh(buttonName);
	}

	@Then("^Click on the \"(.*?)\" button search the \"(.*?)\" to create the Account$")
	public void click_on_the_button_search_the_to_create_the_Account(String interactionbutton, String accountname) throws Throwable {
		int rand_int1 = rand.nextInt(1000);     
		accountname=accountname+rand_int1;
		accountNamerandom=accountname;
		System.out.println(accountNamerandom);
		functionalLib.CreateAccountfromInteractionLauncher(interactionbutton,accountNamerandom);
	}


	@Then("^Click on \"(.*?)\" from \"(.*?)\"$")
	public void click_on_from(String iconName, String arg2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		functionalLib.ClickCreateAccountIcon(iconName);
	}

	@Then("^Edit the Account \"(.*?)\" from \"(.*?)\" tab$")
	public void edit_the_Account_from_tab(String accountName, String tabName) throws Throwable {
		functionalLib.clickanAccountNameandEdit(accountName);
	}

	@Then("^Click on \"(.*?)\" Under \"(.*?)\" from \"(.*?)\" tab$")
	public void click_on_Under_from_tab(String buttonName, String inputtype, String tabName) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		functionalLib.ClickEditUnderdropdown(buttonName);
	}

	@Then("^Minimize \"(.*?)\" tab in \"(.*?)\" page$")
	public void minimize_tab_in_page(String tabName, String pageName) throws Throwable {
		functionalLib.minimizetab(tabName);
	}

	@Then("^Search account \"(.*?)\" from global search navigator in the home page$")
	public void search_account_from_global_search_navigator_in_the_home_page(String accountName) throws Throwable {
		ScenarioOrTestCaseNum =accountName;
		functionalLib.searchaccountfromglobalsearch(accountName);
		functionalLib.waitforspinner();
	}

	@Then("^Select the QuoteNumber from \"(.*?)\" tab$")
	public void select_the_QuoteNumber_from_tab(String tabName) throws Throwable {
		functionalLib.selectRecentQuoteNumber();
	}

	@Then("^Click on the \"(.*?)\" from the list$")
	public void click_on_the_from_the_list(String arg1) throws Throwable {
		Thread.sleep(20000);
		functionalLib.selectRecentQuoteNumberfromtable();
	}

	@Then("^Get the \"(.*?)\" from Application and Click \"(.*?)\" link in the \"(.*?)\" tab$")
	public void get_the_from_Application_and_Click_link_in_the_tab(String quoteNumber,String linkName,String tabName) throws Throwable {
		Thread.sleep(4000);
		functionalLib.clickLinktab(QuoteNumber, linkName);
	}

	@Then("^Capture the Quote Number$")
	public void capture_the_Quote_Number() throws Throwable {	
		QuoteNumber = DriverFactory.getDriver().findElement(By.xpath("//table[contains(@class,'slds-table forceRecordLayout slds-table')]//tr[1]//td[3]//a")).getText();
		System.out.println(QuoteNumber);
	}

	@Then("^Verify the \"(.*?)\" premium$")
	public void verify_the_premium(String product) throws Throwable {
		//String filename = "ZPC_Pricing_Results_"+execution_start_date_time+"";z
		String filename = "ZPC_Pricing_Results_"+month+"";
		String actualpremiumwithcalculation=null;
		float premiumwithcalculation = 0;
		Path path = Paths.get(""+System.getProperty("user.dir")+"\\Output\\"+filename+".xlsx");
		if (Files.notExists(path))
		{
			Workbook wb = new XSSFWorkbook();
			Sheet sh = wb.createSheet("Results");
			Sheet sh1 = wb.createSheet("Passed");
			Sheet sh2 = wb.createSheet("Failed");
			
			FileOutputStream out = new FileOutputStream(new File(""+System.getProperty("user.dir")+"\\Output\\"+filename+".xlsx"));
			String[] headers = new String[] { "S.No", "Test ID", "Scenario/TestCase Number","Product", "Policy/Quote Number", "Actual Premium", "Actual Premium with calculation","Expected Premium", "Date Created","Status"};
			//String[] headers = new String[] { "S.No", "Test ID","Product", "Policy/Quote Number", "Actual Premium", "Actual Premium with calculation","Expected Premium", "Date Created","Status"};
			Row r = sh.createRow(0);
			Row r1 = sh1.createRow(0);
			Row r2 = sh2.createRow(0);
			
			for (int rn=0; rn<headers.length; rn++) 
			{
				r.createCell(rn).setCellValue(headers[rn]);
				r1.createCell(rn).setCellValue(headers[rn]);
				r2.createCell(rn).setCellValue(headers[rn]);
			}

			wb.write(out);
			out.close();
			wb.close();
		}
		FileInputStream f=new FileInputStream(""+System.getProperty("user.dir")+"\\Output\\"+filename+".xlsx");
		Workbook w = WorkbookFactory.create(f);
		Sheet s = w.getSheet("Results");
		Sheet s1 = w.getSheet("Passed");
		Sheet s2 = w.getSheet("Failed");
		int counter = 1;
		//System.out.println(scenario_name);Scenario/TestCase Number
		String Expected_Premium=readPremiumFromExcel(scenario_name);
		//String Expected_Premium=readPremiumFromExcel("1");
		outputloop:
			while(counter <= 10000)
			{
				try
				{
					@SuppressWarnings("unused")
					double sno = s.getRow(counter).getCell(0).getNumericCellValue();
					counter++;				
				}
				catch (NullPointerException NPE)
				{
					try{
						actual_premium = DriverFactory.getDriver().findElement(By.xpath("(//span[contains(text(),'System Premium')]/../following::div)[1]")).getText();
						if(actual_premium.isEmpty()) {
							System.out.println("The System Premium value is not generated");
							actual_premium = " ";
							actualpremiumwithcalculation=" ";
						}else {
							actual_premium = actual_premium.replaceAll("[£,]", "");
							float premiumvalue=Float.parseFloat(actual_premium);
							premiumwithcalculation=(float) (premiumvalue * 1.12);
							premiumwithcalculation =Float.parseFloat(new DecimalFormat("##.##").format(premiumwithcalculation));
							actualpremiumwithcalculation=Float.toString(premiumwithcalculation);
						}
					}catch(StaleElementReferenceException e) {
						actual_premium = DriverFactory.getDriver().findElement(By.xpath("(//span[contains(text(),'System Premium')]/../following::div)[1]")).getText();
						if(actual_premium.isEmpty()) {
							System.out.println("The System Premium value is not generated");
							actual_premium = " ";
							actualpremiumwithcalculation=" ";
						}else {
							actual_premium = actual_premium.replaceAll("[£,]", "");
							float premiumvalue=Float.parseFloat(actual_premium);
							premiumwithcalculation=(float) (premiumvalue * 1.12);
							premiumwithcalculation =Float.parseFloat(new DecimalFormat("##.##").format(premiumwithcalculation));
							actualpremiumwithcalculation=Float.toString(premiumwithcalculation);
						}
					}

					expected_premium=(double) expected_premium;

					diff = premiumwithcalculation - expected_premium;
                   try {
					if(diff<=0.05 && diff>=-0.05)
					{
						status = "Passed";
						System.out.println("The Actual System Premium is:"+premiumwithcalculation);
						System.out.println(status);
						/** Writing values into 'Results' sheet */
						Row r = s.createRow(counter);
						r.createCell(0).setCellValue(counter);//ScenarioOrTestCaseNum
						r.createCell(1).setCellValue(scenario_name);
						r.createCell(2).setCellValue(ScenarioOrTestCaseNum);
						r.createCell(3).setCellValue(product);
						r.createCell(4).setCellValue(QuoteNumber);
						r.createCell(5).setCellValue(actual_premium);
						r.createCell(6).setCellValue(actualpremiumwithcalculation);
						r.createCell(7).setCellValue(Expected_Premium);
						r.createCell(8).setCellValue(functionalLib.current_date_time());
						r.createCell(9).setCellValue(status);
						//FileOutputStream out = new FileOutputStream(new File(""+System.getProperty("user.dir")+"\\Output\\"+filename+".xlsx"));
						//w.write(out);
						//out.close();
						//w.close();
						/** Writing values into 'Passed' sheet */
						int lastrow =s1.getLastRowNum();
						//Row r1 = s1.createRow(counter);
						Row r1 = s1.createRow(lastrow+1);
						r1.createCell(0).setCellValue(lastrow+1);
						r1.createCell(1).setCellValue(scenario_name);
						r1.createCell(2).setCellValue(ScenarioOrTestCaseNum);
						r1.createCell(3).setCellValue(product);
						r1.createCell(4).setCellValue(QuoteNumber);
						r1.createCell(5).setCellValue(actual_premium);
						r1.createCell(6).setCellValue(actualpremiumwithcalculation);
						r1.createCell(7).setCellValue(Expected_Premium);
						r1.createCell(8).setCellValue(functionalLib.current_date_time());
						r1.createCell(9).setCellValue(status);
						FileOutputStream out = new FileOutputStream(new File(""+System.getProperty("user.dir")+"\\Output\\"+filename+".xlsx"));
						w.write(out);
						out.close();
						w.close();
						
						break outputloop;
					}
					else
					{
						status = "Failed";
						System.out.println("The Actual System Premium is:"+premiumwithcalculation);
						System.out.println(status);
						throw new Exception("Expected premium and actual premium is not matched");
					}
                   }catch(Exception e) {
                	   
                	   /** Writing values into 'Results' sheet */
						Row r = s.createRow(counter);
						r.createCell(0).setCellValue(counter);
						r.createCell(1).setCellValue(scenario_name);
						r.createCell(2).setCellValue(ScenarioOrTestCaseNum);
						r.createCell(3).setCellValue(product);
						r.createCell(4).setCellValue(QuoteNumber);
						r.createCell(5).setCellValue(actual_premium);
						r.createCell(6).setCellValue(actualpremiumwithcalculation);
						r.createCell(7).setCellValue(Expected_Premium);
						r.createCell(8).setCellValue(functionalLib.current_date_time());
						r.createCell(9).setCellValue(status);
						
						/** Writing values into 'Failed' sheet */
						
						int lastrow =s2.getLastRowNum();
                	   //Row r2 = s2.createRow(counter);
						Row r2 = s2.createRow(lastrow+1);
						r2.createCell(0).setCellValue(lastrow+1);
						r2.createCell(1).setCellValue(scenario_name);
						r2.createCell(2).setCellValue(ScenarioOrTestCaseNum);
						r2.createCell(3).setCellValue(product);
						r2.createCell(4).setCellValue(QuoteNumber);
						r2.createCell(5).setCellValue(actual_premium);
						r2.createCell(6).setCellValue(actualpremiumwithcalculation);
						r2.createCell(7).setCellValue(Expected_Premium);
						r2.createCell(8).setCellValue(functionalLib.current_date_time());
						r2.createCell(9).setCellValue(status);
						FileOutputStream out = new FileOutputStream(new File(""+System.getProperty("user.dir")+"\\Output\\"+filename+".xlsx"));
						w.write(out);
						out.close();
						w.close();
						throw new RuntimeException("Premium not matched");
						//sbreak outputloop;
			}
				}
			}
	}

	@SuppressWarnings("deprecation")
	public String readPremiumFromExcel(String rowName) throws Exception {
		String expectedPremium=null;
		FileInputStream fis = new FileInputStream(""+System.getProperty("user.dir")+"\\Data\\ZPC_Pricing_Data_Input.xlsx");
		Workbook wb = new XSSFWorkbook(fis);
		try {
			Sheet sheet = wb.getSheet("PricingPremium");
			Iterator<Row> rowIterator = sheet.iterator(); 
			while (rowIterator.hasNext()) { 
				Row row = rowIterator.next(); 
				int rownumber=row.getRowNum();
				String numberAsString = String.valueOf(rownumber);
				if(rowName.equals(numberAsString)) {
					Iterator<Cell> cellIterator = row.cellIterator(); 
					while (cellIterator.hasNext()) { 
						Cell cell = cellIterator.next(); 
						// Check the cell type and format accordingly 
						switch (cell.getCellType()) { 
						//cell.getCell
						case NUMERIC: 
						expected_premium=cell.getNumericCellValue();
							break; 
						case STRING: 
							System.out.print(cell.getStringCellValue()); 
							break; 
						} 
					} 
				} 
			}
			expectedPremium = Double.toString(expected_premium);
			expected_premium =Double.parseDouble(new DecimalFormat("##.##").format(expected_premium));
			expectedPremium=Double.toString(expected_premium);
			System.out.println("The Expected System Premium is:"+expectedPremium);
			wb.close();  
		} catch (Exception e) {
			e.printStackTrace();
		}
		return expectedPremium;
	}

	@Then("^select the contact to \"(.*?)\" it$")
	public void select_the_contact_to_it(String EditButton) throws Throwable {
		DriverFactory.getDriver().manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		WebElement element = DriverFactory.getDriver().findElement(By.xpath("//*[contains(@class,'slds-var-p-horizontal_medium slds-var-p-vertical')]//a[contains(@class,'rowActionsPlaceHolder slds-button slds-button')]"));
		try {		
			WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 15);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[contains(@class,'slds-var-p-horizontal_medium slds-var-p-vertical')]//a[contains(@class,'rowActionsPlaceHolder slds-button slds-button')]")));
			element.click();
					}
		catch(Exception e)
		{
			JavascriptExecutor js = (JavascriptExecutor)DriverFactory.getDriver();
			js.executeScript("arguments[0].click();", element);

			DriverFactory.getDriver().findElement(By.xpath("//a[@title='"+EditButton+"']")).click();
		}

	}
	@Then("^Select date for \"(.*?)\" datepicker as \"(.*?)\" in \"(.*?)\" Page$")
	public void select_date_for_datepicker_as_in_Page(String fieldName, String date, String pageName) throws Throwable {
		functionalLib.selectFromDatePicker(fieldName, date,pageName);
	}
	@Then("^Click on the \"(.*?)\" Button in the contact page$")
	public void click_on_the_Button_in_the_contact_page(String arg1) throws Throwable {
		DriverFactory.getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		DriverFactory.getDriver().findElement(By.xpath("//button[@title='Save']")).click();
	}

	@Then("^fetch the account name from the application$")
	public void fetch_the_account_name_from_the_application() throws Throwable {
		String AccountName = DriverFactory.getDriver().findElement(By.xpath("//div[@class='test-id__field-label-container slds-form-element__label']/span[contains(text(),'Account Name')]/../following-sibling::div/span/span")).getText();
		System.out.println(AccountName);
	}

	@Then("^Click on the QUOTE NAME$")
	public void Click_on_the_QUOTE_NAME() throws Throwable {
		JavascriptExecutor jse = (JavascriptExecutor)DriverFactory.getDriver();
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("//section[contains(@class,'tabs__content active uiTab')]/div/div/div/div[1]//child::tbody/tr[1]/th/div/a"));
		jse.executeScript("arguments[0].click();",element);
				
	}

	@Then("^Click on the Driver1$")
	public void Click_on_the_Driver1() throws Throwable {

		JavascriptExecutor jse = (JavascriptExecutor)DriverFactory.getDriver();
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("//a[contains(@class,'textUnderline outputLookupLink slds-truncate forceOutputLookup')]//parent::div//parent::th//parent::tr//preceding-sibling::tr[2]/th/div/a"));
		jse.executeScript("arguments[0].click();",element);
		
	}

	@Then("^Click on the driver contact$")
	public void Click_on_the_driver_contact() throws Throwable {		
		JavascriptExecutor jse = (JavascriptExecutor)DriverFactory.getDriver();
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("//div[contains(@class,'slds-col slds-grid slds-has-flexi-truncate  full forcePageBlockItem forcePageBlockItemView')]//a[contains(@class,'textUnderline outputLookupLink slds-truncate forceOutputLookup') and text()='Onetwentyone testcase']"));
		jse.executeScript("arguments[0].click();",element);
		
	}

	@Then("^Add new conviction to the driver1$")
	public void Add_new_conviction_to_the_driver1() throws Throwable {	
		JavascriptExecutor jse = (JavascriptExecutor)DriverFactory.getDriver();
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("//span[contains(@class,'slds-card__header-title slds-truncate slds-m-right--xx-small') and text()='Convictions']//parent::a//parent::h2//parent::div//parent::header//following-sibling::div[contains(@class,'slds-no-flex')]/div/ul/li/a/div"));
		//driver.findElement(By.xpath("//a[contains(@class,'textUnderline outputLookupLink slds-truncate forceOutputLookup')]//parent::div//parent::th//parent::tr//preceding-sibling::tr[2]/th/div/a")).click();
		jse.executeScript("arguments[0].click();",element);
		//element.click();
	}

	@Then("^Select the Type Of Conviction as \"(.*?)\" from the dropdown$")
	public void Select_the_Type_Of_Conviction_as_from_the_dropdown(String value) throws Throwable {	
		Select dropdown=new Select(DriverFactory.getDriver().findElement(By.xpath("//span[text()='Type of Conviction']//parent::span//parent::div//a[contains(@class,'select')]")));
		dropdown.selectByVisibleText(value);
	}

	//account creation part
	@Then("^Click on the Interaction Launcher tab$")
	public void Click_on_the_Interaction_Launcher_tab() throws Throwable {	
		JavascriptExecutor jse = (JavascriptExecutor)DriverFactory.getDriver();
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("//span[text()='Interaction Launcher']"));
		jse.executeScript("arguments[0].click();",element);
	}

	@Then("^Enter the \"(.*?)\" in the Search box$")
	public void Enter_the_in_the_Search_box(String accountname) throws Throwable {	
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("//input[@name='Name']"));
		element.sendKeys(accountname);
	}

	@Then("^Click on the \"(.*?)\" button$")
	public void Click_on_the_button(String buttonname) throws Throwable {
		JavascriptExecutor jse = (JavascriptExecutor)DriverFactory.getDriver();
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("(//span[text()='Search'])[1]"));
		jse.executeScript("arguments[0].click();",element);  
	}

	@Then("^Click on the Create Account icon$")
	public void Click_on_the_Create_Account_icon() throws Throwable {
		Thread.sleep(5000);
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("(//div[@class='slds-button-group slds-float--right'])[1]"));
		element.click();	
	}

	@Then("^Select \"(.*?)\" as \"(.*?)\" from the dropdown in the \"(.*?)\" page$")
	public void	Select_from_the_dropdown_in_the_page(String fieldName, String value, String pageName) throws Throwable {
		iFrame = DriverFactory.getDriver().findElement(By.xpath("//iframe[@title='accessibility title']"));
		DriverFactory.getDriver().switchTo().frame(iFrame);
		if(fieldName.equalsIgnoreCase("title")) {
			DriverFactory.getDriver().manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			WebElement element = DriverFactory.getDriver().findElement(By.xpath("//select[@id='"+fieldName+"']/option[@value='"+value+"']"));
			if(functionalLib.isClickable(element) == true)
			{ 
			}else
			{
				functionalLib.findelementinframe(element,iFrame);
			}
		}
		DriverFactory.getDriver().switchTo().defaultContent();
	}
	@Then("^Enter \"(.*?)\" as \"(.*?)\" in \"(.*?)\" page$")
	public void Enter_as_in_page(String textField, String inputvalue, String pageName)  throws Throwable { 
		iFrame = DriverFactory.getDriver().findElement(By.xpath("//iframe[@title='accessibility title']"));
		DriverFactory.getDriver().switchTo().frame(iFrame);
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("//input[@id='"+textField+"']"));
		if(functionalLib.isClickable(element) == true)
		{

		}
		else
		{
			functionalLib.findelementinframe(element,iFrame);			
		}
		DriverFactory.getDriver().findElement(By.xpath("//input[@id='"+textField+"']")).clear();
		DriverFactory.getDriver().findElement(By.xpath("//input[@id='"+textField+"']")).sendKeys(inputvalue);
		DriverFactory.getDriver().switchTo().defaultContent();
	}
	@Then("^Enter the \"(.*?)\" and search as \"(.*?)\" in \"(.*?)\" page$") 
	public void Enter_the_and_search_as_in_page(String textField,String inputvalue,String pageName) throws Exception{
		if(pageName.equals("Account Creation Customer Details")) {
			iFrame = DriverFactory.getDriver().findElement(By.xpath("//iframe[contains(@name,'vfFrameId_')]"));}
		DriverFactory.getDriver().switchTo().frame(iFrame);
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("//input[@id='"+textField+"']"));
		if(functionalLib.isClickable(element) == true) {
			DriverFactory.getDriver().findElement(By.xpath("//input[@id='"+textField+"']")).click();
			DriverFactory.getDriver().findElement(By.xpath("//input[@id='"+textField+"']")).sendKeys(inputvalue);
			Thread.sleep(100);
			functionalLib.SelectValue(textField,inputvalue);
		}else {
			
			functionalLib.findelementinframe(element,iFrame);
			DriverFactory.getDriver().findElement(By.xpath("//input[@id='"+textField+"']")).click();
			DriverFactory.getDriver().findElement(By.xpath("//input[@id='"+textField+"']")).sendKeys(inputvalue);
			functionalLib.SelectValue(textField, inputvalue);
		}
		
		DriverFactory.getDriver().switchTo().defaultContent();
	}

	@Then("^Enter the \"(.*?)\" in the typebox as \"(.*?)\" in \"(.*?)\" page$")
	public void Enter_the_in_the_typebox_as_in_page(String fieldName,String value,String Pagename) throws Exception{
		iFrame = DriverFactory.getDriver().findElement(By.xpath("//iframe[contains(@name,'vfFrameId_')]"));
		DriverFactory.getDriver().switchTo().frame(iFrame);
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("(//span[contains(text(),'"+fieldName+"')]/..//preceding-sibling::input[contains(@id,'TypeAhead')])[1]"));
		if(functionalLib.isClickable(element) == true)
		{
			//driver.findElement(By.xpath("//div[contains(@id,'nextBtn') and contains(@id,'"+pageName+"')]/p[text()='"+buttonName+"']")).click();
		}
		else
		{
			functionalLib.findelementinframe(element,iFrame);
		}

		element.sendKeys(value);
		Thread.sleep(1000);
		DriverFactory.getDriver().findElement(By.xpath("//ul[contains(@class,'typeahead dropdown-menu')]/li[1]/a[contains(text(),'"+value+"')]")).click();

		DriverFactory.getDriver().switchTo().defaultContent();
	}	
	@Then("^Click on the \"(.*?)\" details in the contact tab$")
	public void Click_on_the_details_in_the_contact_tab(String linkname)  throws Exception{
		Thread.sleep(10000);
		DriverFactory.getDriver().findElement(By.xpath("//div[@class='listItemBody withActions']//a")).click();
	}

	@Then("^Select the \"(.*?)\" tab in \"(.*?)\" page$")
	public void Select_the_tab_in_page(String tabname,String pagename)throws Exception{
		WebDriverWait wait= new WebDriverWait(DriverFactory.getDriver(),20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@class='tabContent active oneConsoleTab']//span[text()='Details']")));
		//JavascriptExecutor jse = (JavascriptExecutor)driver;

		WebElement element=DriverFactory.getDriver().findElement(By.xpath("//section[@class='tabContent active oneConsoleTab']//span[text()='Details']"));
		element.click();
		//jse.executeScript("arguments[0].click();",element);
	}

	@Then("^Click on the \"(.*?)\" link in the overview page$")
	public void Click_on_the_link_in_the_overview_page(String linkName) throws Exception{
		if(linkName.equalsIgnoreCase("Overview")||linkName.equalsIgnoreCase("Related")||linkName.equalsIgnoreCase("Details")) {
			WebElement element = DriverFactory.getDriver().findElement(By.xpath("(//a[contains(.,'"+linkName+"')])[1]/span[text()='"+linkName+"']"));
			if(functionalLib.isClickable(element) == true) {

			}else {
				functionalLib.findelement(element);
			}
		}else {
			iFrame = DriverFactory.getDriver().findElement(By.xpath("//iframe[contains(@src,'https://zpc--mcre2e.lightning.force.com/apex/vlocity_ins__universalcardpage?layout=ZPC%20360')]"));
			DriverFactory.getDriver().switchTo().frame(iFrame);
			WebElement element = DriverFactory.getDriver().findElement(By.xpath("(//a[contains(.,'"+linkName+"')])[1]"));
			if(functionalLib.isClickable(element) == true) {

			}else {
				functionalLib.findelementinframe(element,iFrame);
			}
			DriverFactory.getDriver().switchTo().defaultContent();
		}
	}

	@Then("^Click and enter the \"(.*?)\" as \"(.*?)\" in \"(.*?)\" page$")
	public void Click_and_enter_the_as_in_page(String fieldname,String value,String Pagename)throws Exception{
		DriverFactory.getDriver().manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		WebDriverWait wait= new WebDriverWait(DriverFactory.getDriver(),20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Licence Year']/../../div[2]/span")));
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("//span[text()='Licence Year']/../../div[2]/span"));
		if(functionalLib.isClickable(element) == true) {

			Actions actions = new Actions(DriverFactory.getDriver());
			actions.doubleClick(element).perform();
			DriverFactory.getDriver().findElement(By.xpath("//div[contains(@class,'SectionRow')]//div[2]//input[@class=' input']")).sendKeys(value);

		}else
		{
			functionalLib.findelement(element);
			Actions actions = new Actions(DriverFactory.getDriver());
			actions.doubleClick(element).perform();
			DriverFactory.getDriver().findElement(By.xpath("//div[contains(@class,'SectionRow')]//div[2]//input[@class=' input']")).sendKeys(value);

		}
	}

	@Then("^Select the \"(.*?)\" as \"(.*?)\" in \"(.*?)\" page$")
	public void Select_the_as_in_page(String buttonName,String value,String Pagename)throws Exception{

		DriverFactory.getDriver().manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		WebElement element = DriverFactory.getDriver().findElement(By.xpath("//span[contains(text(),'"+buttonName+"')]/../../div[1]//a"));

		if(functionalLib.isClickable(element) == true) {
			DriverFactory.getDriver().findElement(By.xpath("//span[contains(text(),'"+buttonName+"')]/../../following::a[@title='"+value+"']")).click();

		}else

			functionalLib.findelement(element);
		DriverFactory.getDriver().findElement(By.xpath("//span[contains(text(),'"+buttonName+"')]/../../following::a[@title='"+value+"']")).click();

		DriverFactory.getDriver().manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	}

	@Then("^Click On the Save Button in the contact page$")
	public void Click_On_the_Save_Button_in_the_contact_page() throws Exception{
		JavascriptExecutor jse = (JavascriptExecutor)DriverFactory.getDriver();
		WebElement element=DriverFactory.getDriver().findElement(By.xpath("//button[@title='Save']"));
		//element.click();
		jse.executeScript("arguments[0].click();",element); 
	}

	@Then("^Click on the Refresh button in the Overview tab$")
	public void click_on_the_Refresh_button_in_the_Overview_tab() throws Throwable {
		Thread.sleep(10000);
		iFrame = DriverFactory.getDriver().findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__universalcardpage?layout=ZPC%')]"));
		DriverFactory.getDriver().switchTo().frame(iFrame);
		Thread.sleep(10000);
		//driver.findElement(By.xpath("//button[contains(@onclick,'reload')]")).click();
		JavascriptExecutor executor = (JavascriptExecutor)DriverFactory.getDriver();
		executor.executeScript("arguments[0].click();", DriverFactory.getDriver().findElement(By.xpath("//button[contains(@onclick,'reload')]")));
		//Thread.sleep(5000);
		DriverFactory.getDriver().switchTo().defaultContent();
	}
	@Then("^Verify the \"(.*?)\" Premium$")
	public void verify_the_premium1(String product) throws Throwable {
		String filename = "ZPC_Pricing_Results_"+execution_start_date_time+"";
		String actualpremiumwithcalculation=null;
		float premiumwithcalculation = 0;
		Path path = Paths.get(""+System.getProperty("user.dir")+"\\Output\\"+filename+".xlsx");
		if (Files.notExists(path))
		{
			Workbook wb = new XSSFWorkbook();
			Sheet sh = wb.createSheet("Results");
			FileOutputStream out = new FileOutputStream(new File(""+System.getProperty("user.dir")+"\\Output\\"+filename+".xlsx"));
			String[] headers = new String[] { "S.No", "Test ID", "Product", "Policy/Quote Number", "Actual Premium", "Actual Premium with calculation","Expected Premium", "Date Created","Status"};
			Row r = sh.createRow(0);
			for (int rn=0; rn<headers.length; rn++) 
			{
				r.createCell(rn).setCellValue(headers[rn]);
			}

			wb.write(out);
			out.close();
			wb.close();
		}
		FileInputStream f=new FileInputStream(""+System.getProperty("user.dir")+"\\Output\\"+filename+".xlsx");
		Workbook w = WorkbookFactory.create(f);
		Sheet s = w.getSheet("Results");
		int counter = 1;
		String Expected_Premium=readPremiumFromExcel1(scenario_name);
		outputloop:
			while(counter <= 10000)
			{
				try
				{
					@SuppressWarnings("unused")
					double sno = s.getRow(counter).getCell(0).getNumericCellValue();
					counter++;				
				}
				catch (NullPointerException NPE)
				{
					try{
						actual_premium = DriverFactory.getDriver().findElement(By.xpath("//span[contains(text(),'System Premium')]/../following::div[1]")).getText();
						if(actual_premium.isEmpty()) {
							System.out.println("The System Premium value is not generated");
							actual_premium = " ";
							actualpremiumwithcalculation=" ";
						}else {
							actual_premium = actual_premium.replaceAll("[£,]", "");
							float premiumvalue=Float.parseFloat(actual_premium);
							premiumwithcalculation=(float) (premiumvalue * 1.12);
							premiumwithcalculation =Float.parseFloat(new DecimalFormat("##.##").format(premiumwithcalculation));
							actualpremiumwithcalculation=Float.toString(premiumwithcalculation);
						}
					}catch(StaleElementReferenceException e) {
						actual_premium = DriverFactory.getDriver().findElement(By.xpath("//span[contains(text(),'System Premium')]/../following::div[1]")).getText();
						if(actual_premium.isEmpty()) {
							System.out.println("The System Premium value is not generated");
							actual_premium = " ";
							actualpremiumwithcalculation=" ";
						}else {
							actual_premium = actual_premium.replaceAll("[£,]", "");
							
						}
					}

					expected_premium=(double) expected_premium;

					diff = premiumwithcalculation - expected_premium;

					if(diff<=0.05 && diff>=-0.05)
					{
						status = "Passed";
						System.out.println("The Actual System Premium is:"+premiumwithcalculation);
						System.out.println(status);
					}
					else
					{
						status = "Failed";
						System.out.println("The Actual System Premium is:"+premiumwithcalculation);
						System.out.println(status);
					}

					Row r = s.createRow(counter);
					r.createCell(0).setCellValue(counter);
					r.createCell(1).setCellValue(scenario_name);
					r.createCell(2).setCellValue(product);
					r.createCell(3).setCellValue(QuoteNumber);
					r.createCell(4).setCellValue(actual_premium);
					r.createCell(5).setCellValue(actualpremiumwithcalculation);
					r.createCell(6).setCellValue(Expected_Premium);
					r.createCell(7).setCellValue(functionalLib.current_date_time());
					r.createCell(8).setCellValue(status);
					FileOutputStream out = new FileOutputStream(new File(""+System.getProperty("user.dir")+"\\Output\\"+filename+".xlsx"));
					w.write(out);
					out.close();
					w.close();
					break outputloop;
				}
			}
	}

	@SuppressWarnings("deprecation")
	public String readPremiumFromExcel1(String rowName) throws Exception {
		String expectedPremium=null;
		FileInputStream fis = new FileInputStream(""+System.getProperty("user.dir")+"\\Data\\ZPC_HRA_Data_Input.xlsx");
		Workbook wb = new XSSFWorkbook(fis);
		try {
			Sheet sheet = wb.getSheet("HRAPremium");
			Iterator<Row> rowIterator = sheet.iterator(); 
			while (rowIterator.hasNext()) { 
				Row row = rowIterator.next(); 
				int rownumber=row.getRowNum();
				String numberAsString = String.valueOf(rownumber);
				if(rowName.equals(numberAsString)) {
					Iterator<Cell> cellIterator = row.cellIterator(); 
					while (cellIterator.hasNext()) { 
						Cell cell = cellIterator.next(); 
						// Check the cell type and format accordingly 
						switch (cell.getCellType()) { 
						case NUMERIC: 
						expected_premium=cell.getNumericCellValue();
							break; 
						case STRING: 
							System.out.print(cell.getStringCellValue()); 
							break; 
						} 
					} 
				} 
			}
			expectedPremium = Double.toString(expected_premium);
			expected_premium =Double.parseDouble(new DecimalFormat("##.##").format(expected_premium));
			expectedPremium=Double.toString(expected_premium);
			System.out.println("The Expected System Premium is:"+expectedPremium);
			wb.close();  
		} catch (Exception e) {
			e.printStackTrace();
		}
		return expectedPremium;
	}

	@Then("^click on the \"(.*?)\" button in \"(.*?)\" in \"(.*?)\" page$")
	public void click_on_the_button_in_in_page(String value1, String value2, String page) throws Throwable {
		DriverFactory.getDriver().manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS) ;
		iFrame = DriverFactory.getDriver().findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		DriverFactory.getDriver().switchTo().frame(iFrame);
		Thread.sleep(3000);
		WebElement element = DriverFactory.getDriver().findElement(By.xpath("//div[text()[contains(.,'"+value1+"')]]//following::span[text()[contains(.,'"+value2+"')]]"));
		if(functionalLib.isClickable(element) == true)
		{
		}
		else
		{
			functionalLib.findelementinframe(element, iFrame);
			//driver.findElement(By.xpath("//a[@title='Edit']")).click();
		}
		DriverFactory.getDriver().switchTo().defaultContent();
		Thread.sleep(3000);
	}

	@Then("^click on the \"(.*?)\" button in the \"(.*?)\" in \"(.*?)\" page$")
	public void click_on_the_button_in_in_page1(String value1, String value2, String page) throws Throwable {
		iFrame = DriverFactory.getDriver().findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		DriverFactory.getDriver().switchTo().frame(iFrame);
		Thread.sleep(100);
		WebElement element = DriverFactory.getDriver().findElement(By.xpath("//div[text()[contains(.,'"+value1+"')]]//following::span[text()[contains(.,'"+value2+"')]]//following::span[1]"));
		if(functionalLib.isClickable(element) == true)
		{
		}
		else
		{
			functionalLib.findelementinframe(element, iFrame);
			//driver.findElement(By.xpath("//a[@title='Edit']")).click();
		}
		DriverFactory.getDriver().switchTo().defaultContent();

	}
	@Then("wait for few seconds")
	public void wait_for_few_Seconds() throws InterruptedException{

		Thread.sleep(5000);
	}
	@And("^Enter complete address for postcode \"(.*?)\"$")
	public void enter_complete_addrss(String postcode) {
		functionalLib.enterCompleteAddress(postcode);
			
		
	}
}
