package stepDefAndRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.qa.util.ConfigReader;
import com.qa.util.EncryptAndDecrypt;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(plugin = { "pretty", "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
		"timeline:test-output-thread/" }, monochrome = true, glue = { "stepDefAndRunner" },
		
		tags = "@regression1",

		features = { "src/test/resources/MotorScenarios/RegressionScenarios2.feature" }

)

public class ParallelRun2 extends AbstractTestNGCucumberTests {

	@Override
	@DataProvider(parallel = true)
	public Object[][] scenarios() {
		return super.scenarios();
	}
	
	@Parameters("browserType")
	@BeforeClass
	public void beforeClass(String browserValue) throws ConfigurationException, IOException {
		ConfigReader configReader =  new ConfigReader();
		Properties prop =configReader.init_prop();
		//prop.setProperty("browser", browserValue);
		HashMap<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("browser", browserValue);
		String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\config\\config.properties";
		EncryptAndDecrypt.saveDataToPropertiesFile(filePath, paramMap);
	}

}  