package stepDefAndRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.testng.annotations.*;

import com.qa.util.ConfigReader;
import com.qa.util.EncryptAndDecrypt;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(plugin = { "pretty", "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
		"timeline:test-output-thread/" }, monochrome = true, glue = { "stepDefAndRunner" },
		
		tags = "@regression123",

		features = { "src/test/resources/MotorScenarios/RegressionScenarios.feature" }

)

public class ParallelRun extends AbstractTestNGCucumberTests {

	@Override
	@DataProvider(parallel = true)
	public Object[][] scenarios() {
		return super.scenarios();
	}
	
	
}  