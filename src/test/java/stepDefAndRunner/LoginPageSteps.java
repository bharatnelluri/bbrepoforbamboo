package stepDefAndRunner;

import org.junit.Assert;

import com.pages.LoginPage;
import com.qa.factory.DriverFactory;
import com.qa.util.ConfigReader;
import com.qa.util.EncryptAndDecrypt;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginPageSteps {

	private static String title;
	private LoginPage loginPage = new LoginPage(DriverFactory.getDriver());
	private ConfigReader configReader;

	@Given("user is on login page")
	public void user_is_on_login_page() {
		configReader = new ConfigReader();
		
		/*DriverFactory.getDriver()
		.get("https://zpc--muat2.my.salesforce.com/");*/
		DriverFactory.getDriver()
		.get(configReader.init_prop().getProperty("URL"));
	}

	@When("user enters username")
	public void user_enters_username() {
		configReader = new ConfigReader();
		loginPage.enterUserName(configReader.init_prop().getProperty("userName"));
	}

	@When("user enters password")
	public void user_enters_password() {
	    configReader = new ConfigReader();
		loginPage.enterPassword(EncryptAndDecrypt.getPassword(configReader.init_prop().getProperty("userName")));
	}

	@When("user clicks on Login button")
	public void user_clicks_on_login_button() {
		loginPage.clickOnLogin();
	}


}
