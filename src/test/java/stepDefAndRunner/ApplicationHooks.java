package stepDefAndRunner;

import java.io.File;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.FileUtils;
//import org.junit.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
//import org.testng.annotations.*;
import org.testng.annotations.Parameters;

import com.qa.factory.DriverFactory;
import com.qa.util.ConfigReader;
import com.qa.util.EncryptAndDecrypt;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class ApplicationHooks {

	private DriverFactory driverFactory;
	private WebDriver driver;
	private ConfigReader configReader;
	Properties prop;
	Properties prop1;
	String scenario_name;
	public EncryptAndDecrypt encrypt;

	public static String dateTimeGenerate() {
		Format formatter = new SimpleDateFormat("YYYYMMdd_HHmmssSSS");
		Date date = new Date(System.currentTimeMillis());
		return formatter.format(date);
	}

	@Before(order = 0)
	public void getProperty() throws ConfigurationException, IOException {
		configReader = new ConfigReader();
		prop = configReader.init_prop();
		//System.out.println(dateTimeGenerate());
		prop1 = configReader.init_extentprop();
		//System.out.println(prop1.getProperty("extent.reporter.spark.out"));
		//encrypt = new EncryptAndDecrypt();
		HashMap<String, String> paramMap = new HashMap<String, String>();
		String reportName = "Reports/SparkReport/Spark_" + dateTimeGenerate() + ".html";
		paramMap.put("extent.reporter.spark.out", reportName);
		String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\extent.properties";
		EncryptAndDecrypt.saveDataToPropertiesFile(filePath, paramMap);

	}
    //@Parameters("browserType")
    @Before(order = 1)
	public void launchBrowser() throws IOException {
		configReader = new ConfigReader();
		prop = configReader.init_prop();
		String browserName = prop.getProperty("browser");
		//prop.setProperty("browser", browserValue);
		driverFactory = new DriverFactory();
		driver = driverFactory.init_driver(browserName);
	}

	@After(order = 0)
	public void quitBrowser() {

		driver.quit();

	}
	

}
