package com.qa.factory;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverFactory {

	public WebDriver driver;

	public static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<>();
	
	/**
	 * this is used to get the driver with ThreadLocal
	 *
	 * @return
	 */
	public static synchronized WebDriver getDriver() {
		return tlDriver.get();
	}

	/**
	 * This method is used to initialize the thradlocal driver on the basis of given
	 * browser
	 *
	 * @param browser
	 * @return this will return tldriver.
	 * @throws IOException 
	 */
	public WebDriver init_driver(String browser) { 

		System.out.println("browser value is: " + browser);
		 
		if (browser.equals("chrome")) {
			WebDriverManager.chromedriver().setup();
			/*
			 * //String Chrome_Profile_Path = ""+project_path+"\\Chrome profile";
			 * ChromeOptions options = new ChromeOptions();
			 * options.addArguments("--no-sandbox"); // Bypass OS security model, MUST BE
			 * THE VERY FIRST OPTION
			 * options.setExperimentalOption("excludeSwitches",Collections.singletonList(
			 * "enable-automation")); options.addArguments("--window-size=1366x768");
			 * options.addArguments("--start-maximized"); // open Browser in maximized mode
			 * options.addArguments("--disable-gpu"); // applicable to windows OS only
			 * options.addArguments("--disable-dev-shm-usage"); // overcome limited resource
			 * problems options.addArguments("--disable-notifications");
			 * //options.addArguments("user-data-dir=" + Chrome_Profile_Path);
			 * 
			 * Map<String, Object> prefs = new HashMap<String, Object>();
			 * prefs.put("googlegeolocationaccess.enabled", true);
			 * prefs.put("profile.default_content_setting_values.geolocation", 2); //
			 * 1:allow 2:block
			 * prefs.put("profile.default_content_setting_values.notifications", 1);
			 * prefs.put("profile.managed_default_content_settings", 1);
			 * prefs.put("profile.exit_type", "Normal");
			 * options.setExperimentalOption("prefs", prefs);
			 */
						
			tlDriver.set(new ChromeDriver());

		} else if (browser.equals("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			tlDriver.set(new FirefoxDriver());
		} else if (browser.equals("safari")) {
			tlDriver.set(new SafariDriver());
		} else if (browser.equals("Edge")) {
			WebDriverManager.edgedriver().setup();
			tlDriver.set(new EdgeDriver());
		}
		else {
			System.out.println("Please pass the correct browser value: " + browser);
		}

		getDriver().manage().deleteAllCookies();
		getDriver().manage().window().maximize();
		return getDriver();

	}

	
}