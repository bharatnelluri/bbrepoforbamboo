package com.qa.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.Key;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import javax.swing.*;

import java.lang.reflect.Method;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.PropertiesConfigurationLayout;

import sun.misc.*;

public class EncryptAndDecrypt {

	public static final String ALGO = "AES";

	public static final byte[] KEYVALUE = new byte[] { 'T', 'h', 'e', 'B', 'e', 's', 't', 'S', 'e', 'c', 'r', 'e', 't',
			'K', 'e', 'y' };

	@SuppressWarnings("restriction")

	public static String encrypt(String string) {

		String encryptedString = null;

		try {

			Key key = generateKey();

			Cipher c = Cipher.getInstance(ALGO);

			c.init(Cipher.ENCRYPT_MODE, key);

			byte[] encVal = c.doFinal(string.getBytes());

			encryptedString = Base64.getEncoder().encodeToString(encVal);

		} catch (Exception e) {

			e.printStackTrace();

		}

		return encryptedString;

	}

	private static Key generateKey() throws Exception {

		Key key = new SecretKeySpec(KEYVALUE, ALGO);

		return key;

	}

	public static String decrypt(String encryptedString) {

		String decryptedString = null;

		try {

			Key key = generateKey();

			Cipher c = Cipher.getInstance(ALGO);

			c.init(Cipher.DECRYPT_MODE, key);

			@SuppressWarnings("restriction")

			byte[] decordedValue = Base64.getDecoder().decode(encryptedString);

			byte[] decValue = c.doFinal(decordedValue);

			decryptedString = new String(decValue);

		} catch (Exception e) {

			e.printStackTrace();

		}

		return decryptedString;

	}

	public static void saveDataToPropertiesFile(String filePath, Map<String, String> paramMap)
			throws ConfigurationException, IOException {

		PropertiesConfiguration propConfig = new PropertiesConfiguration();

		PropertiesConfigurationLayout propConfigLayOut = new PropertiesConfigurationLayout(propConfig);

		File file = new File(filePath);

		propConfigLayOut.load(new InputStreamReader(new FileInputStream(file)));

		FileWriter fileWriter = new FileWriter(filePath, false);

		if (paramMap != null && paramMap.size() > 0) {

			for (String key : paramMap.keySet()) {

				if (key != null && key != "" && paramMap.get(key) != null) {

					propConfig.setProperty(key, paramMap.get(key).trim());

				}

			}

			propConfigLayOut.save(fileWriter);
		}

	}

	public static String getRunTimeInputText(String alertName, boolean isPassword) {

		JLabel jPassword = new JLabel(alertName);

		JTextField text = null;

		if (isPassword)

			text = new JPasswordField();

		else

			text = new JTextField();

		Object[] ob = { jPassword, text };

		// String pwd = "";

		int result = JOptionPane.showConfirmDialog(null, ob, "", JOptionPane.OK_CANCEL_OPTION);

		if (result == JOptionPane.OK_OPTION)

			return isPassword ? EncryptAndDecrypt.encrypt(text.getText()) : text.getText();

		else
			return "";

	}

	/*
	 * public static String getPassword(){
	 * 
	 * ConfigReader configReader = new ConfigReader();
	 * 
	 * return
	 * EncryptAndDecrypt.decrypt(configReader.init_prop().getProperty("password"));
	 * 
	 * }
	 */
	
	public static String getPassword(String userName){

        Xls_Reader xlsData = new Xls_Reader(System.getProperty("user.dir")+"\\Login\\Login.xlsx");

        int rowNumber = xlsData.getCellRowNum("LoginDetails", "userName", userName);

        String password = xlsData.getCellData("LoginDetails", "password", rowNumber);        

        return EncryptAndDecrypt.decrypt(password);

 }

}
