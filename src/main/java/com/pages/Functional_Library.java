package com.pages;

import org.testng.*;

import com.qa.factory.DriverFactory;

//import io.cucumber.java.Scenario;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

//@SuppressWarnings("deprecation")
public class Functional_Library {

	public WebDriver driver;
	public static String env;
	public static String url;
	public static String username;
	public static String password;
	public static String scenario;
	public static double expected_premium;
	public static String actual_premium;
	public static String scenario_name;
	public static double diff;
	public static String status;
	public static String file_path;
	public static String project_path;
	public static WebElement iFrame;
	public static String tab;
	public static String QuoteNumber="";
	
	//Constructor of the page class:
		public Functional_Library(WebDriver driver) {
			this.driver = driver;
		}
	
		/*
		 * public void scenarioName(Scenario scenario) { scenario_name =
		 * scenario.getName(); System.out.println(scenario_name); }
		 */
		
	public Random rand= new Random();
	static DateFormat date_time = new SimpleDateFormat("dd_MM_YYYY_HHmmss");
	DateFormat dateFormat2 = new SimpleDateFormat("dd_MM_yyyy_HHmmss");

	static Date date1 = new Date();

	//Execution start date and time //Execution date and time
	protected static String execution_start_date_time = date_time.format(date1);

	//Function for current date and time calculation
	public String current_date_time()
	{
		Date datetime = new Date();
		String datetime_s = dateFormat2.format(datetime);
		return datetime_s;
	}

	//Function for capturing screen shots
	public void screenshots(String file_path)
	{
		File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try 
		{
			FileUtils.copyFile(src, new File(""+file_path+"\\"+scenario_name+".png"));
			//Reporter.addScreenCaptureFromPath(""+file_path+"\\"+scenario_name+".png");
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}

	//Function to find whether an element is clickable or not
	public static boolean isClickable(WebElement webelement)      
	{
		try
		{
			WebDriverWait wait=	new WebDriverWait(DriverFactory.getDriver(), 15);
			wait.until(ExpectedConditions.elementToBeClickable(webelement));
			//JavascriptExecutor executor = (JavascriptExecutor)DriverFactory.getDriver();
			//executor.executeScript("arguments[0].click();", webelement);
			webelement.click();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	public boolean staleElement(WebElement element) {
		boolean result = false;
		int attempts = 0;
		while(attempts < 5) {
			try {
				element.click();
				result = true;
				break;
			} catch(Exception e) {
			}
			attempts++;
		}
		return result;
	}

	//Function to find whether an element is displayed or not
	public static boolean isDisplayed(WebElement webelement)      
	{
		try
		{
			webelement.isDisplayed();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}


	//Method to find the element by scrolling through the web page
	public void findelement(WebElement element) throws InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.MILLISECONDS);
		Thread.sleep(2000);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		for(int i=0;i<=10000;i=i+50)
		{
			jse.executeScript("window.scrollBy(0,"+i+")","");
			if(isClickable(element) == true)
			{
				break;
			}
			if(i == 10000)
			{
				if(isClickable(element) == false)
				{
					element.click();
				}
			}
		}
	}

	//Method to find the element by scrolling through the frame in web page
	public void findelementinframe(WebElement element, WebElement iFrame) throws InterruptedException 
	{
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.MILLISECONDS);
		driver.switchTo().defaultContent();
		JavascriptExecutor js = (JavascriptExecutor)driver;
		outerloop:
			for(int i=0;i<=10000;i=i+100)
			{
				driver.switchTo().defaultContent();

				JavascriptExecutor jse = (JavascriptExecutor)driver;
				Long pageyoffset_bf = (Long) jse.executeScript("return window.pageYOffset;");
				jse.executeScript("window.scrollBy(0,100)","");
				//This step is added by Bharat to handle StaleElement reference exception
				for(int j=0; j<=2;j++){
					try {
				driver.switchTo().frame(iFrame);
				break;
					}catch(StaleElementReferenceException e) {
						iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
					}
				}

				if(isClickable(element) == true)
				{
					break;
				}
				if(i>pageyoffset_bf)
				{
					driver.switchTo().defaultContent();
					js.executeScript("window.scrollTo(0,0)","");
					waitforscroll();
					for(int j=0;j<=10000;j=j+100)
					{
						driver.switchTo().defaultContent();

						pageyoffset_bf = (Long) jse.executeScript("return window.pageYOffset;");
						jse.executeScript("window.scrollBy(0,100)","");

						driver.switchTo().frame(iFrame);

						if(isClickable(element) == true)
						{
							break outerloop;

						}
						if(j>pageyoffset_bf)
						{
							if(isClickable(element) == false)
							{
								element.click();					
							}
						}
					}
				}
			}
	}

	//Method to wait till the spinning icon disappears in omniscript
	public void waitforspinner() throws InterruptedException
	{
		try {
			boolean spinner = true;
			int time = 1;
			do
			{
				Thread.sleep(500);
				spinner = driver.findElement(By.xpath("//div[@class='slds-spinner slds-spinner_medium']")).isDisplayed();
				if(time == 500)
				{
					AssertJUnit.fail("Maximum wait time reached for spinner");
				}
				time += 1;
			}
			while(spinner == true);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	//Method to wait till the spinning icon disappears in frame
	public void waitforspinnerinframe() throws InterruptedException
	{
		try {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
			driver.switchTo().frame(iFrame);
			boolean spinner = true;
			int time = 1;
			do
			{
				Thread.sleep(500);
				spinner = driver.findElement(By.xpath("//div[@class='slds-spinner__dot-a']")).isDisplayed();
				if(time == 200)
				{
					AssertJUnit.fail("Maximum wait time reached for spinner");
				}
				time += 1;
			}
			while(spinner == true);
			driver.switchTo().defaultContent();
		}
		catch(Exception e) {
			//e.printStackTrace();
		}
	}
	
	public void waitforspinnerincontact() throws InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(500,TimeUnit.MILLISECONDS);
		try {
			boolean spinner = true;
			int time = 1;
			do
			{
				spinner = driver.findElement(By.xpath("//div[@class='spinnerWrapper forceComponentSpinner']")).isDisplayed();
				Thread.sleep(50);
				if(time == 200)
				{
					AssertJUnit.fail("Maximum wait time reached for spinner");
				}
				time += 1;
			}
			while(spinner == true);
		}
		catch(Exception e) {
			
		}
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	}

	//Method to wait till the scroll bar reaches the top of the page
	public void waitforscroll() throws InterruptedException
	{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		Long value = (Long) executor.executeScript("return window.pageYOffset;");
		int time = 1;
		do
		{
			Thread.sleep(1000);
			value = (Long) executor.executeScript("return window.pageYOffset;");
			if(time == 90)
			{
				AssertJUnit.fail("Maximum wait time reached");
			}
			time += 1;
		}
		while(value != 0);
	}

	//Function to find whether an xpath exist or not
	public  boolean isExist(String xpath) {
		try {
			driver.manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);
			driver.findElement(By.xpath(""+xpath+"")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			return true;
		}catch(Exception e) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			return false;
		}
	}

	/* 
	 * Method to login salesforce application
	 */
	public void loginSalesforce(String username,String password) throws Exception{
		try {
			driver.findElement(By.xpath("//input[@id='username']")).sendKeys(username);
			driver.findElement(By.xpath("//input[@id='password']")).sendKeys(password);
			driver.findElement(By.xpath("//input[@value='Log In to Sandbox']")).click();
			Thread.sleep(50000);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/*
	 * Method to close all the open tabs
	 */
	public void closeOpenTab() throws Exception{
		Thread.sleep(30000);
		driver.manage().timeouts().implicitlyWait(2,TimeUnit.SECONDS);
		//WebElement ele= driver.findElement(By.xpath("(//span[text()='Account']/../../following::div[contains(@title,'Actions')]//button[contains(@title,'Actions for')])[100]"));
		List<WebElement> openButtons = driver.findElements(By.xpath("(//span[text()='Account']/../../following::div[contains(@title,'Actions')]//button[contains(@title,'Actions for')])[1]"));
		int opentabs=openButtons.size();
		while(opentabs == 1)
		{
			driver.findElement(By.xpath("(//button[contains(@title,'Close') and @type='button'])[1]")).click();
			Thread.sleep(25000);
			openButtons = driver.findElements(By.xpath("(//span[text()='Account']/../../following::div[contains(@title,'Actions')]//button[contains(@title,'Actions for')])[1]"));
			opentabs=openButtons.size();
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	//Method to close the sub tab
	public void closesubtab(String tabName) throws InterruptedException {
		
		Thread.sleep(20000);
		tab = driver.getWindowHandle();
		driver.switchTo().window(tab);
		WebElement element = driver.findElement(By.xpath("//li[contains(@class,'oneConsoleTabItem')]/a[contains(.,'"+ tabName + "') ]/following::button[contains(@title,'Close') and @type='button']"));
		Thread.sleep(25000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		driver.switchTo().defaultContent();
	}

	/*
	 *  Method to select the value from auto popup option
	 */
	public void selectAutoPopup(String fieldName,String value) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		WebElement element = driver.findElement(By.xpath("//label[text()='"+fieldName+"']/following::input[@aria-haspopup='listbox'][1]"));
		if(isClickable(element) == true)
		{
			element.sendKeys(value);
			WebDriverWait wait = new WebDriverWait(driver,30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//lightning-base-combobox-formatted-text[@title='"+value+"']")));
			driver.findElement(By.xpath("//lightning-base-combobox-formatted-text[@title='"+value+"']")).click();
		}
		else
		{
			findelement(element);
			element.sendKeys(value);
			WebDriverWait wait = new WebDriverWait(driver,30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//lightning-base-combobox-formatted-text[@title='"+value+"']")));
			driver.findElement(By.xpath("//lightning-base-combobox-formatted-text[@title='"+value+"']")).click();
		}
	}

	/* 
	 * Method to click the button
	 */
	public void clickButton(String buttonName) throws Exception{
		try {
			if(buttonName.equalsIgnoreCase("Save") || buttonName.equalsIgnoreCase("Cancel")||buttonName.equalsIgnoreCase("Save & New")){
				WebElement element =driver.findElement(By.xpath("//button[text()='"+buttonName+"']"));
				if(isClickable(element) == true) {

				}else{
					findelement(element);
				}
			}else if(buttonName.equalsIgnoreCase("next")){
				WebElement element =driver.findElement(By.xpath("//div[@class='inlineFooter']//span[text()='"+buttonName+"']"));
				if(isClickable(element) == true) {

				}else
				{
					findelement(element);
				}

			}else if(buttonName.equalsIgnoreCase("Import")|| buttonName.equalsIgnoreCase("New")){
				driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
				WebElement element =driver.findElement(By.xpath("//div[contains(@class,'truncate') and @title='"+buttonName+"']"));
				if(isClickable(element) == true) {

				}else
				{
					findelement(element);
				}
			}else if(buttonName.equalsIgnoreCase("Savepopup")) {
				driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
				WebElement element =driver.findElement(By.xpath("//div[contains(@class,'actionsContainer')]/following::button/span[text()='Save']"));
				if(isClickable(element) == true) {

				}else
				{
					findelement(element);
				}
			}
			else {
				System.out.println("The button is not available in the page");
			}
			driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS) ;	
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	/* 
	 * Method to select value from the Menu 
	 */
	public void selectmenu(String DropdownMenu) throws Exception{
		try{
			Actions action = new Actions(driver);
			driver.findElement(By.xpath("//div[contains(@class,'selectedListItem')]/following-sibling::div")).click();
			action.moveToElement(driver.findElement(By.xpath("//span[contains(@class,'menuLabel') and text()='"+DropdownMenu+"']"))).perform();
			action.build().perform();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;

		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	/* 
	 * Method to select the Dropdown value
	 */
	public void selectDropdown(String fieldName,String value) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		WebElement element = driver.findElement(By.xpath("//span[contains(text(),'"+fieldName+"')]"));
		if(isClickable(element) == true)
		{
			driver.findElement(By.xpath("//span[contains(text(),'"+fieldName+"')]/following::a[contains(text(),'None')]")).click();
			driver.findElement(By.xpath("//span[contains(text(),'"+fieldName+"')]/following::a[contains(text(),'"+value+"')]")).click();
		}
		else
		{
			findelement(element);
			driver.findElement(By.xpath("//span[contains(text(),'"+fieldName+"')]/following::a[contains(text(),'None')]")).click();
			driver.findElement(By.xpath("//span[contains(text(),'"+fieldName+"')]/following::a[contains(text(),'"+value+"')]")).click();
		}
	}

	public void selectDropdownInIframe(String fieldName,String value,String pageName) throws Exception{
		if(pageName.equals("Account Creation Customer Details")) {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@name,'vfFrameId_')]"));
		}else 
		{
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		}
		String randomTitle="";
		driver.switchTo().frame(iFrame);
		if(fieldName.equalsIgnoreCase("title")) {
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			WebElement element = driver.findElement(By.xpath("//select[@id='"+fieldName+"']/option[@value='"+value+"']"));
			List<WebElement> se = new Select(driver.findElement(By.xpath("//select[@id='"+fieldName+"']"))).getOptions();
			ArrayList<String> titleValues = new ArrayList<String>();
			for(int i=0;i<se.size();i++){
				String dvalues =se.get(i).getText();
				titleValues.add(dvalues);
				Random rand = new Random();
				int randomTitlemNumber = rand.nextInt(titleValues.size()- 0) + 0;
				randomTitle=titleValues.get(randomTitlemNumber);
				if(randomTitle.contains("Clear")) {
					randomTitle=titleValues.get(randomTitlemNumber);
				}
			}
			element = driver.findElement(By.xpath("//select[@id='"+fieldName+"']/option[@value='"+randomTitle+"']"));
			if(isClickable(element) == true)
			{
			}else
			{
				findelementinframe(element,iFrame);
			}
		}else {
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			WebElement element = driver.findElement(By.xpath("//select[@id='"+fieldName+"']/option[@value='"+value+"']"));
			if(isClickable(element) == true)
			{

			}else
			{
				Thread.sleep(5000);
				findelementinframe(element,iFrame);
			}	
		}
		driver.switchTo().defaultContent();
	}

	/*
	 * Method to enter values into input 
	 */
	public void enterText(String textField,String inputvalue) throws Exception{
		WebElement element = driver.findElement(By.xpath("//label[text()='"+textField+"']/following::input[@type='text'][1]"));
		if(isClickable(element) == true)
		{
			element.sendKeys(inputvalue);
		}
		else
		{
			findelement(element);
			element.sendKeys(inputvalue);
		}			
	}

	/*
	 * Method to select Insurance Type from account Page Iframe Screen
	 */
	public void selectInsuranceType(String value) throws Exception{
		Thread.sleep(3000);
		//WebElement ele= driver.findElement(By.xpath("(//span[text()='Account']/../../following::div[contains(@title,'Actions')]//button[contains(@title,'Actions for')])[100]"));
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__universalcardpage?layout=ZPC%')]"));
		for(int j=0; j<=2;j++){
			try {
		driver.switchTo().frame(iFrame);
		break;
			}catch(StaleElementReferenceException e) {
				iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__universalcardpage?layout=ZPC%')]"));
			}
		}
		try {	
			WebElement element = driver.findElement(By.xpath("//span[contains(@class,'icon-v-plus')]/following::span[contains(text(),'"+value+"')]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		}
		catch(NoSuchWindowException e){
			e.printStackTrace();
			Thread.sleep(3000);
			WebElement element = driver.findElement(By.xpath("//span[contains(@class,'icon-v-plus')]/following::span[contains(text(),'"+value+"')]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		}
		driver.switchTo().defaultContent();
	}

	/*
	 * Method to Click CheckBox page
	 */
	public void clickCheckBoxInframe(String checkBoxName) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		try {
			WebElement element = driver.findElement(By.xpath("//span[contains(text(),'"+checkBoxName+"')]/..//preceding-sibling::span[contains(@class,'checkbox')]"));
			if(isClickable(element) == true) {

			}else
			{
				findelementinframe(iFrame, element);
			}
		}catch(StaleElementReferenceException e) {
			System.out.println("stale element exception is happened ");
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
			driver.switchTo().frame(iFrame);
			{
				driver.findElement(By.xpath("//span[contains(text(),'"+checkBoxName+"')]/..//preceding-sibling::span[contains(@class,'checkbox')]")).click();
			}
		} finally {

		}
		driver.switchTo().defaultContent();
	}

	/* 
	 * Method to enter the values in  Iframe TextBox
	 */
	public void enterTextInIframe(String textField,String inputvalue,String pageName) throws Exception{
		if(pageName.equals("Account Creation Customer Details")) {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@name,'vfFrameId_')]"));
		}else {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));		
		}
		if(textField.equalsIgnoreCase("Fore Name")||textField.equalsIgnoreCase("Last Name")) {
			String alphabet= "abcdefghijklmnopqrstuvwxyz";
			Random random = new Random();
			int randomLen = 5;
			for (int i = 0; i < randomLen; i++) {
				char c = alphabet.charAt(random.nextInt(26));
				inputvalue+=c;
			}
		}
		driver.switchTo().frame(iFrame);
		WebElement element=driver.findElement(By.xpath("//input[@id='"+textField+"']"));
		if(isClickable(element) == true)
		{

		}
		else
		{
			findelementinframe(element,iFrame);			
		}
		driver.findElement(By.xpath("//input[@id='"+textField+"']")).clear();
		driver.findElement(By.xpath("//input[@id='"+textField+"']")).sendKeys(inputvalue);
		if(textField.equalsIgnoreCase("RiskAddressTypeAheadAddress")) {
			findelementinframe(driver.findElement(By.xpath("//ul[contains(@class,'typeahead dropdown-menu')]/li[1]/a[contains(text(),'"+inputvalue+"')]")),iFrame);
		}else if(textField.equalsIgnoreCase("Buildings Sum Insured")||textField.equalsIgnoreCase("Buildings Excess")) {
			try {
				findelementinframe(driver.findElement(By.xpath("//span[contains(text(),'Would you like to add Building?')]/..//preceding-sibling::span[contains(@class,'checkbox')]")),iFrame);
			}catch(StaleElementReferenceException e) {
				System.out.println("Stale element is occured");
			}finally {
				driver.findElement(By.xpath("//span[contains(text(),'Would you like to add Building?')]/..//preceding-sibling::span[contains(@class,'checkbox')]")).click();
			}
		}
		driver.switchTo().defaultContent();
	}

	/* 
	 * Method to enter the type ahead values in  Iframe TextBox
	 */
	public void enterTypeAheadIframe(String typeAheadField,String inputvalue) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		//WebElement element = driver.findElement(By.xpath("(//span[contains(text(),'"+typeAheadField+"')]/..//preceding-sibling::input[contains(@id,'TypeAhead')])[1]"));
		WebElement element = driver.findElement(By.xpath("(//span[contains(text(),'"+typeAheadField+"')]/..//preceding-sibling::input[contains(@id,'TypeAhead')])[1]"));
		if(isClickable(element) == true)
		{
		}
		else
		{
			findelementinframe(element,iFrame);			
		}
		element.sendKeys(inputvalue);
		//		element.sendKeys(Keys.TAB);
		Thread.sleep(3000);
		element = driver.findElement(By.xpath("//div[@class='pac-item']//span"));
		if(typeAheadField.equalsIgnoreCase("Garaging Postcode")) 
		{
			if(isClickable(element) == true)
			{
			}
			else
			{
				findelementinframe(element,iFrame);
			}
		}
		driver.switchTo().defaultContent();
	}

	/* 
	 * Method to enter the values in  Iframe TextBox
	 */
	public void entervalueAndSearchInIframe(String textField,String inputvalue,String pageName) throws Exception{
		if(pageName.equals("Account Creation Customer Details")) {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@name,'vfFrameId_')]"));
		}else {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		}
		driver.switchTo().frame(iFrame);
		WebElement element=driver.findElement(By.xpath("//input[@id='"+textField+"']"));
		if(isClickable(element) == true) {
			driver.findElement(By.xpath("//input[@id='"+textField+"']")).click();
			driver.findElement(By.xpath("//input[@id='"+textField+"']")).sendKeys(inputvalue);
			Thread.sleep(100);
			SelectValue(textField,inputvalue);
		}else {
			findelementinframe(element,iFrame);
			driver.findElement(By.xpath("//input[@id='"+textField+"']")).click();
			driver.findElement(By.xpath("//input[@id='"+textField+"']")).sendKeys(inputvalue);
			SelectValue(textField, inputvalue);
		}
		driver.switchTo().defaultContent();
	}

	public void SelectValue(String textField,String inputvalue) throws Exception{
		WebElement element=driver.findElement(By.xpath("//input[@id='OccupationLookup']"));
		if(isClickable(element) == true)
		{

		}
		else
		{
			findelementinframe(element,iFrame);
		}

		element=driver.findElement(By.xpath("//li[contains(text(),'"+inputvalue+"')]"));
		if(isClickable(element) == true) {

		}
		else {
			findelementinframe(element,iFrame);
		}
	}

	/*
	 * Method to select the date from calender
	 */
	public void selectDate(String fieldName, String date,String pageName) throws Exception{
		if(pageName.equals("Account Creation Customer Details")) {
			//iFrame = driver.findElement(By.xpath("//iframe[contains(@id,'vfFrameId')]"));
			iFrame = driver.findElement(By.xpath("//iframe[contains(@name,'vfFrameId_')]"));
		}else {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		}
		driver.switchTo().frame(iFrame);
		Actions action = new Actions(driver);
		WebElement element = driver.findElement(By.xpath("//input[@id='"+fieldName+"']"));
		if(isClickable(element) == true)
		{
			driver.findElement(By.xpath("//input[@id='"+fieldName+"']")).sendKeys(date);
			driver.findElement(By.xpath("//input[@id='"+fieldName+"']")).click();
			action.moveToElement(driver.findElement(By.xpath("//td[contains(@class,'slds-is-selected')]/span"))).build();
			action.build().perform();
		}
		else
		{
			findelementinframe(element,iFrame);
			driver.findElement(By.xpath("//input[@id='"+fieldName+"']")).sendKeys(date);
			driver.findElement(By.xpath("//input[@id='"+fieldName+"']")).click();
			action.moveToElement(driver.findElement(By.xpath("//td[contains(@class,'slds-is-selected')]/span"))).build();
			action.build().perform();
		}
		driver.switchTo().defaultContent();
	}

	/* 
	 * Method to click the button
	 */
	public void clickButtonInIframe(String buttonName,String pageName) throws Exception{
		if(buttonName.equals("Create Account")) {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@name,'vfFrameId_')]"));
		}else {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		}
		driver.switchTo().frame(iFrame);
		if(buttonName.contains("Previous")) {
			WebElement element = driver.findElement(By.xpath("//div[contains(@id,'prevBtn') and contains(@id,'"+pageName+"')]/p[contains(text(),'"+buttonName+"')]"));
			if(isClickable(element) == true)
			{
			}
			else
			{
				findelementinframe(element,iFrame);
			}
		}else {
			WebElement element = driver.findElement(By.xpath("//div[contains(@id,'nextBtn') and contains(@id,'"+pageName+"')]/p[text()='"+buttonName+"']"));			
			if(isClickable(element) == true)
			{
			}
			else
			{
				findelementinframe(element,iFrame);
				System.out.println("");
			}
		}
		driver.switchTo().defaultContent();
	}

	public void answerQuestion(String Question,String Answer) throws Exception{
		Thread.sleep(4000);
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		WebElement element = driver.findElement(By.xpath("//label[contains(text(),'"+Question+"')]/..//input[@value='"+Answer+"']/following-sibling::span[contains(@class,'radio')]"));
		if(isClickable(element) == true) {

		}else
		{
			findelementinframe(element,iFrame);
		}
		driver.switchTo().defaultContent();
	}

	public void clickDriverIcon(String buttonName) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		WebElement element = driver.findElement(By.xpath("//div[contains(@class,'addbutton')]//span[contains(text(),'"+buttonName+"')]"));
		if(isClickable(element) == true) {

		}else
		{
			findelementinframe(element,iFrame);
		}
		driver.switchTo().defaultContent();
	}

	public void clickOnEditAfterTypeAheadIframe(String buttonName, String typeAheadField) throws Exception{
		iFrame = driver.findElement(By.xpath("(//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')])"));
		driver.switchTo().frame(iFrame);
		WebElement element=driver.findElement(By.xpath("//span[contains(text(),'"+typeAheadField+"')]/preceding::input[contains(@id,'TypeAhead')][1]/preceding::*[text()='"+buttonName+"'][1]"));
		if(isClickable(element) == true)
		{
			//driver.findElement(By.xpath("//input[@id='TypeAheadBrokerOrganisation']/preceding::*[text()='"+buttonName+"'][1]")).click();
		}
		else
		{
			findelementinframe(element,iFrame);
			//driver.findElement(By.xpath("//input[@id='TypeAheadBrokerOrganisation']/preceding::*[text()='"+buttonName+"'][1]")).click();
		}
		driver.switchTo().defaultContent();
	}

	public void handleAlerts(String alertName) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		WebElement element=driver.findElement(By.xpath("//button[contains(text(),'"+alertName+"')]"));
		if(isClickable(element) == true)
		{

		}
		else
		{
			findelementinframe(element,iFrame);
		}
		driver.switchTo().defaultContent();
	}

	public void linkIframe(String linkName) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
         Thread.sleep(5000);
		if(linkName.equalsIgnoreCase("Overview")||linkName.equalsIgnoreCase("Related")||linkName.equalsIgnoreCase("Details")) {
			WebElement element = driver.findElement(By.xpath("//a[contains(@class,'tab') and text()='"+linkName+"']"));
			if(isClickable(element) == true) {

			}else {
				findelement(element);
			}
		}else {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'https://zpc--mcre2e.lightning.force.com/apex/vlocity_ins__universalcardpage?layout=ZPC%20360')]"));
			driver.switchTo().frame(iFrame);
			WebElement element = driver.findElement(By.xpath("(//a[contains(.,'"+linkName+"')])[1]"));
			if(isClickable(element) == true) {

			}else {
				findelementinframe(element,iFrame);
			}
			driver.switchTo().defaultContent();
		}

	}

	public void verifyMessage(String message, String tabName) throws Exception{
		tab = driver.getWindowHandle();
		driver.switchTo().window(tab);
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/POCIP?ContextId')]"));
		driver.switchTo().frame(iFrame);
		WebElement element = driver.findElement(By.xpath("//b[contains(text(),'"+message+"')]"));
		try{
			findelementinframe(element,iFrame);
		}
		catch (Exception e)
		{
			AssertJUnit.fail("Quote not rated");
		}
		driver.switchTo().defaultContent();
	}

	//Newly added 
	public void clickpopupHyperLink(String buttonName,int driver_number,String pageName) throws Exception{
		Thread.sleep(5000);
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		int i = driver_number+1;
		WebElement element = driver.findElement(By.xpath("(//div[@data-select='multi']/following::input[@id='Driver'])["+i+"]/following::a[text()='"+buttonName+"']"));
		if(isClickable(element) == true) {

		}else
		{
			findelementinframe(element,iFrame);
		}
		driver.switchTo().defaultContent();
	}

	public void selectdropdownfromhyperlink(String dropdownName,String value) throws Exception{
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		//WebElement element = driver.findElement(By.xpath("//label[text()='"+dropdownName+"']/following::input[@aria-haspopup='listbox'][1]"));
		Thread.sleep(3000);
		WebElement element = driver.findElement(By.xpath("//label[text()='"+dropdownName+"']/following::div//button[contains(@aria-label,'"+dropdownName+"')]"));
			
		if(isClickable(element) == true) {
			Thread.sleep(5000);
			//WebElement lookup = driver.findElement(By.xpath("//label[text()='"+dropdownName+"']/following::input[@aria-haspopup='listbox']/following::span[@title='"+value+"']"));
			WebElement lookup = driver.findElement(By.xpath("//label[text()='"+dropdownName+"']/following::div//button[contains(@aria-label,'"+dropdownName+"')]/../../..//lightning-base-combobox-item//span[@title='"+value+"']"));
			if(isClickable(lookup) == true) {
				
			}
			else
			{
				findelement(lookup);
			}
		}
		else
		{
			findelement(element);
			WebElement lookup = driver.findElement(By.xpath("//label[text()='"+dropdownName+"']/following::input[@aria-haspopup='listbox']/following::span[@title='"+value+"']"));
			Thread.sleep(500);
			if(isClickable(lookup) == true) {
				
			}
			else
			{
				findelement(lookup);
			}
		}
		
	}

	public void selectDateinDriverpage(String fieldName, String date) throws Exception{
		WebElement element = driver.findElement(By.xpath("//label[text()='"+fieldName+"']/following::input[@type='text'][1]"));
		if(isClickable(element) == true)
		{
			element.sendKeys(date);
		}
		else
		{
			findelement(element);
			element.sendKeys(date);
		}
	}

	public void Mapdriverdetails(int driver_number,String driverName) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		int i = driver_number+1;
		String[] driverdetails=driverName.split(" "); 
		driver.findElement(By.xpath("//div[@data-select='multi']/following::input[@id='Driver']["+i+"]")).click();
		if(driver_number == 1)
		{
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement element = driver.findElement(By.xpath("//div[@data-select='multi']/following::input[@id='Driver']["+i+"]/following::li[contains(text(),'"+driverdetails[0]+"') and contains(text(),'"+driverdetails[1]+"')]"));
			if(isClickable(element) == true)
			{

			}
			else
			{
				findelementinframe(element,iFrame);
			}
		}
		else
		{
			WebElement element = driver.findElement(By.xpath("//div[@data-select='multi']/following::input[@id='Driver']["+i+"]/following::li[contains(text(),'"+driverName+"')]"));
			if(isClickable(element) == true)
			{

			}
			else
			{
				findelementinframe(element,iFrame);
			}
		}
		driver.switchTo().defaultContent();
	}

	public void selectpopvalue(String popupvalue) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		WebElement element = driver.findElement(By.xpath("//button[contains(@class,'button--neutral') and text()='"+popupvalue+"']"));
		if(isClickable(element) == true)
		{

		}
		else
		{
			findelementinframe(element,iFrame);
		}
		driver.switchTo().defaultContent();
	}

	public void createRecordinRelatedtab(String buttonName,String recordType) throws Exception{
		Actions actions = new Actions(driver);
		WebElement element = driver.findElement(By.xpath("//span[text()='"+recordType+"']"));
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
		Thread.sleep(1000);
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).perform();
		if(isDisplayed(element)){
			findelement(driver.findElement(By.xpath("//span[text()='"+recordType+"']/../../../preceding-sibling::div[@class='slds-media__figure']/../following-sibling::div//div[text()='"+buttonName+"']/../div")));			
		}	else {
			AssertJUnit.fail("Unable to locate element");
		}
	}

	/*
	 * Method to enter values into input 
	 */
	public void enterDetailsinTextArea(String textField,String inputvalue) throws Exception{
		WebElement element = driver.findElement(By.xpath("//label[contains(@class,'Label')]/span[text()='"+textField+"']/../following-sibling::textarea"));
		if(isClickable(element) == true)
		{
			driver.findElement(By.xpath("//label[contains(@class,'Label')]/span[text()='"+textField+"']/../following-sibling::textarea")).click();
			driver.findElement(By.xpath("//label[contains(@class,'Label')]/span[text()='"+textField+"']/../following-sibling::textarea")).sendKeys(inputvalue);
		}
		else
		{
			findelement(element);
			driver.findElement(By.xpath("//label[contains(@class,'Label')]/span[text()='"+textField+"']/../following-sibling::textarea")).click();
			driver.findElement(By.xpath("//label[contains(@class,'Label')]/span[text()='"+textField+"']/../following-sibling::textarea")).sendKeys(inputvalue);	
		}	
	}


	public void selectCodefromSearchPopup(String fieldName,String value) throws Exception{
		WebElement element = driver.findElement(By.xpath("//div[@role='listitem']//span[text()='"+fieldName+"']/following::input"));
		if(isClickable(element) == true)
		{
			driver.findElement(By.xpath("//div[@role='listitem']//span[text()='"+fieldName+"']/following::input[1]")).click();
			driver.findElement(By.xpath("//div[@role='listitem']//span[text()='"+fieldName+"']/following::input[1]")).sendKeys(value);
			driver.findElement(By.xpath("//div[@class='listContent']/ul/li[1]/a/div[1]/span")).click();
		}
		else
		{
			findelement(element);
			driver.findElement(By.xpath("//div[@role='listitem']//span[text()='"+fieldName+"']/following::input[1]")).click();
			driver.findElement(By.xpath("//div[@role='listitem']//span[text()='"+fieldName+"']/following::input[1]")).sendKeys(value);
			driver.findElement(By.xpath("//div[@class='listContent']/ul/li[1]/a/div[1]/span")).click();
		}
	}

	public void closeDriverdetailTab(String tabName)
	{
		tab = driver.getWindowHandle();
		driver.switchTo().window(tab);
		driver.findElement(By.xpath("//span[contains(text(),'Actions for') and contains(text(),'"+tabName+"') ]/..")).click();
		driver.findElement(By.xpath("//li[contains(@class,'oneConsoleTabItem')]/following::button[contains(@title,'Close') and contains(@title,'"+tabName+"')]")).click();
		driver.switchTo().defaultContent();
	}

	public void searchLookUpSearchAddress(String fieldName,String lookupValue) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		WebElement element = driver.findElement(By.xpath("//legend[text()='"+fieldName+"']/following::input[@aria-haspopup='listbox'][1]"));
		if(isClickable(element) == true)
		{
			element.sendKeys(lookupValue);
			WebDriverWait wait = new WebDriverWait(driver,30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//lightning-base-combobox-formatted-text[@title='"+lookupValue+"']")));
			driver.findElement(By.xpath("//lightning-base-combobox-formatted-text[@title='"+lookupValue+"']")).click();
		}
		else
		{
			findelement(element);
			element.sendKeys(lookupValue);
			WebDriverWait wait = new WebDriverWait(driver,30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//lightning-base-combobox-formatted-text[@title='"+lookupValue+"']")));
			driver.findElement(By.xpath("//lightning-base-combobox-formatted-text[@title='"+lookupValue+"']")).click();
		}	
	}

	//Newly added 
	public void clickpopupHyperLinkwithdriverLink(String buttonName,int driver_number,String driverType) throws Exception{
		Thread.sleep(5000);
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		int i = driver_number+1;
		WebElement element = driver.findElement(By.xpath("(//div[@data-select='multi']/following::input[@id='Driver'])["+i+"]/following::span[contains(text(),'"+driverType+"')]/../../span"));
		if(isClickable(element) == true) {
			driver.findElement(By.xpath("(//div[@data-select='multi']/following::input[@id='Driver'])["+i+"]/following::a[text()='"+buttonName+"']")).click();
		}else
		{
			findelementinframe(element,iFrame);
			driver.findElement(By.xpath("(//div[@data-select='multi']/following::input[@id='Driver'])["+i+"]/following::a[text()='"+buttonName+"']")).click();
		}
		driver.switchTo().defaultContent();
	}

	/* 
	 * Method to click the button
	 */
	public void fetchGetVehicleDetails(String buttonName) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		WebElement element = driver.findElement(By.xpath("//div[contains(@id,'GetVehicleDetails')]/p[text()='"+buttonName+"']"));
		if(isClickable(element) == true)
		{

		}
		else
		{
			findelementinframe(element,iFrame);
		}
		driver.switchTo().defaultContent();
	}

	public void selectOptionWithIndex(int indexToSelect) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			WebElement autoOptions = driver.findElement(By.xpath("//li[@class='splashPage-dropdown-trigger splashPage-dropdown-trigger--click splashPage-m-left--x-small']/button/span"));
			wait.until(ExpectedConditions.visibilityOf(autoOptions));

			List<WebElement> optionsToSelect = autoOptions.findElements(By.xpath("//li[@class='splashPage-dropdown-trigger splashPage-dropdown-trigger--click splashPage-m-left--x-small']/button/span"));
			if(indexToSelect<=optionsToSelect.size()) {
				System.out.println("Trying to select based on index: "+indexToSelect);
				optionsToSelect.get(indexToSelect).click();
			}
		} 		
		catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	}

	/* 
	 * Method to click the button
	 */
	public void NavigatetabinQuoteDetailspage(String tabName) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		try {
			if(tabName.equalsIgnoreCase("details")){
				WebElement element = driver.findElement(By.xpath("(//div[@role='tablist']/following::span[text()='"+tabName+"'])"));
				new WebDriverWait(driver, 25).until(ExpectedConditions.elementToBeClickable(element));
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
			}else if(tabName.equalsIgnoreCase("Related")){
				WebElement element =driver.findElement(By.xpath("//div[@role='tablist']/following::span[text()='"+tabName+"']"));
				new WebDriverWait(driver, 25).until(ExpectedConditions.elementToBeClickable(element));
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
			}else {
				System.out.println("The button is not available in the page");
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void selectContactDetailsFromLookup(String fieldName, String driverName, String accountNumberrandom, String pageName) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		Actions action=new Actions(driver);
		try {
			WebElement element = driver.findElement(By.xpath("//div[@role='listitem']//span[text()='"+fieldName+"']/following::input[1]"));
			String[] accountName=accountNumberrandom.split(" "); 
			driverName = driverName.substring(0, 1).toUpperCase() + driverName.substring(1);
			if(isClickable(element) == true)
			{
				driver.findElement(By.xpath("//div[@role='listitem']//span[text()='"+fieldName+"']/following::input[1]")).sendKeys(driverName);
				driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS) ;
				driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
				Thread.sleep(100);
				action.moveToElement(driver.findElement(By.xpath("//div[@title='"+driverName+"']/following-sibling::div[contains(@title,'"+accountName[0]+"') and contains(@title,'"+accountName[1]+"')]/.."))).build();
				action.build().perform();
			}
			else
			{
				findelement(element);
				driver.findElement(By.xpath("//div[@role='listitem']//span[text()='"+fieldName+"']/following::input[1]")).sendKeys(driverName);
				driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;	
				Thread.sleep(100);
				action.moveToElement(driver.findElement(By.xpath("//div[@title='"+driverName+"']/following-sibling::div[contains(@title,'"+accountName[0]+"') and contains(@title,'"+accountName[1]+"')]/.."))).build();
				action.build().perform();
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void clickautocompleteValue(String fieldName, String value, String pageName) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		WebElement element = driver.findElement(By.xpath("//input[@id='"+fieldName+"']"));
		if(isClickable(element) == true)
		{

		}
		else
		{
			findelementinframe(element,iFrame);
			element.sendKeys(value);
			driver.findElement(By.xpath("//*[@id='TypeAheadAddress-Block']/following::ul/li[1]/a")).click();
		}
		driver.switchTo().defaultContent();
	}

	/*
	 * Method to enter values into input 
	 */
	public void clickcheckBox(String checkBoxFieldName) throws Exception{
		WebElement element = driver.findElement(By.xpath("//label[contains(@class,'inputLabel')]//span[text()='"+checkBoxFieldName+"']/../following-sibling::input[@type='checkbox']"));
		if(isClickable(element) == true)
		{

		}
		else
		{
			findelement(element);
		}	
	}


	/*
	 * Method to select the date from calender
	 */
	public void selectInceptionDate(String date) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		WebElement element = driver.findElement(By.xpath("//input[contains(@class,'ng-invalid-required') and contains(@id,'InceptionDate')]"));
		if(isClickable(element) == true)
		{

		}
		else
		{
			findelementinframe(element,iFrame);
			element.sendKeys(date);
			driver.findElement(By.xpath("//input[contains(@class,'ng-valid-required') and contains(@id,'InceptionDate')]")).click();
		}
		driver.switchTo().defaultContent();

		/*
		 * Updated the above step due to SSR1 changes in inception date field the old code is available in the below 
		 * Updated by Arun Prakash on 19/05/2020
		 */
		/*iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		Actions action=new Actions(driver);
		action.clickAndHold(driver.findElement(By.xpath("//input[@id='InceptionDate']"))).build().perform();
		driver.findElement(By.xpath("//input[@id='InceptionDate']")).sendKeys(date);
		driver.findElement(By.xpath("//input[@id='InceptionDate']")).click();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//input[@id='InceptionDate']//following::td[contains(@class,'slds-is-selected')]/span")));
		driver.switchTo().defaultContent();*/
	}

	public void clicksystemPremiumInRateMotor(String tabName) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/POCIP?ContextId')]"));
		driver.switchTo().frame(iFrame);
		driver.findElement(By.xpath("//input[@type='submit' and contains(@onclick,'Run')]")).click();
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("//li[contains(@class,'oneConsoleTabItem')]/following::button[contains(@title,'"+tabName+"')]")).click();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
	}

	public void selectDriverType(String driverType,int driver_number) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		int i = driver_number+1;

		WebElement element = driver.findElement(By.xpath("//h1[text()='Driver']"));
		if(isClickable(element) == true) {

		}else
		{
			findelementinframe(element, iFrame);
		}
		if(driverType.equalsIgnoreCase("Policy Holder")) {
			driver.findElement(By.xpath("(//div[@data-select='multi']/following::input[@id='Driver'])["+i+"]/following::span[contains(text(),'Policy Holder')]/../../span")).click();	
		}
		else if(driverType.equalsIgnoreCase("Main Driver")){
			driver.findElement(By.xpath("(//div[@data-select='multi']/following::input[@id='Driver'])["+i+"]/following::span[contains(text(),'Main Driver')]/../../span")).click();	
		}
		else if(driverType.equalsIgnoreCase("Included to Drive")){
			driver.findElement(By.xpath("(//div[@data-select='multi']/following::input[@id='Driver'])["+i+"]/following::span[contains(text(),'Included to Drive')]/../../span")).click();
		}else {
			System.out.println("Driver Type is not available in the UI");
		}
		driver.switchTo().defaultContent();
	}

	/*
	 * Method to enter values into input 
	 */
	public String capturevaluesinQuotedetailspage(String fieldName) throws Exception{
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		String systempremium=null;
		for(int i=0;i<=10000;i=i+50)
		{
			jse.executeScript("window.scrollBy(0,"+i+")","");
			if(i == 10000)
			{
				systempremium=driver.findElement(By.xpath("//span[contains(text(),'"+fieldName+"')]/..//../following-sibling::div")).getText();
				if(systempremium.isEmpty()) {
					AssertJUnit.fail("The "+fieldName+" value is not generated");
				}else {
					System.out.println(systempremium);
				}
			}
		}
		return systempremium;
	}

	/* 
	 * Method to enter the values in  Iframe TextBox
	 */
	public void selctAddressfromPopUp(String textField,String inputvalue,String pageName) throws Exception{
		if(pageName.equals("Account Creation Customer Details")) {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@name,'vfFrameId_')]"));
		}else {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));	
		}
		driver.switchTo().frame(iFrame);
		WebElement element=driver.findElement(By.xpath("//span[contains(text(),'"+textField+"')]/preceding::input[contains(@id,'TypeAhead')][1]"));
		if(isClickable(element) == true)
		{
			if(textField.equalsIgnoreCase("Garaging Postcode")) {
				driver.findElement(By.xpath("//span[contains(text(),'"+textField+"')]/preceding::input[contains(@id,'TypeAhead')][1]")).sendKeys(inputvalue);
				driver.findElement(By.xpath("//div[@class='pac-item']//span")).click();
			}else {
				WebElement element2=driver.findElement(By.xpath("//span[contains(text(),'"+textField+"')]/preceding::input[contains(@id,'TypeAhead')][1]"));
				element2.sendKeys(inputvalue);
				findelementinframe(driver.findElement(By.xpath("//ul[contains(@class,'typeahead dropdown-menu')]/li[1]/a[contains(text(),'"+inputvalue+"')]")),iFrame);	
			}
		}
		else
		{
			findelementinframe(element,iFrame);	
			if(textField.equalsIgnoreCase("Garaging Postcode")) {
				driver.findElement(By.xpath("//label[contains(text(),'"+textField+"')]/preceding::input[contains(@id,'TypeAhead')][1]")).sendKeys(inputvalue);
				driver.findElement(By.xpath("//div[@class='pac-item']//span")).click();
			}else {
				WebElement element2=driver.findElement(By.xpath("//label[contains(text(),'"+textField+"')]/preceding::input[contains(@id,'TypeAhead')][1]"));
				element2.sendKeys(inputvalue);
				findelementinframe(driver.findElement(By.xpath("//ul[contains(@class,'typeahead dropdown-menu')]/li[1]/a[contains(text(),'"+inputvalue+"')]")),iFrame);	
			}
		}
		driver.switchTo().defaultContent();
	}

	/* 
	 * Method to click the button
	 */
	public void closetheFlashMessage(String tabName) throws Exception{
		try {
			WebElement element =driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]/../../following::button[@title='"+tabName+"']"));
			if(isClickable(element) == true) {

			}else
			{
				findelement(element);
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void multiselectClaimPage(String fieldName, String multiselectValue, String pageName) throws Exception{
		try {
			WebElement element =driver.findElement(By.xpath("//span[text()='Cause']/../following-sibling::select/option[@value='"+multiselectValue+"']"));
			if(isClickable(element) == true) {

			}else
			{
				findelement(element);
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	/* 
	 * Method to click the button
	 */
	public void clickDoneButtonInIframe(String buttonName,String AlertMessage,String pageName) throws Exception{
		try {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
			driver.switchTo().frame(iFrame);
			WebElement element = driver.findElement(By.xpath("//div[contains(@id,'nextBtn') and contains(@id,'"+pageName+"')]/p[text()='"+buttonName+"']"));
			if(isClickable(element) == true)
			{

			}
			else
			{
				findelementinframe(element,iFrame);
			}
			driver.switchTo().defaultContent();
		}catch(NoSuchElementException e) {
			System.out.println("Unable to locate element:");
		} finally {
			System.out.println("Unable to locate continue message");
		}
	}

	/*
	 * Method to enter values into input 
	 */
	public void clickvaluesinQuotedetailspage(String fieldName) throws Exception{
		Actions action = new Actions(driver);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		for(int i=0;i<=10000;i=i+50)
		{
			jse.executeScript("window.scrollBy(0,"+i+")","");
			if(i == 10000)
			{
				action.moveToElement(driver.findElement(By.xpath("//span[contains(text(),'"+fieldName+"')]/../following::button[contains(@title,'Edit') and contains(@title,'"+fieldName+"')]/preceding-sibling::span//a"))).perform();
				action.build().perform();
			}
		}
	}

	/*
	 * Method to enter values into input 
	 */
	public void clickandnavigateinQuotedetails(String linkName, String tabName) throws Exception{

		try {
			WebElement element =driver.findElement(By.xpath("//span[text()='"+tabName+"']/preceding::div[@class='navMenuList']/following::table//a[contains(text(),'"+linkName+"')]"));
			if(isClickable(element) == true) {

			}else
			{
				findelement(element);
			}
		}catch(Exception e){
		}
	}

	public void clickRefresh(String buttonName) throws Exception{
		try {
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
			driver.switchTo().frame(iFrame);
			WebElement element = driver.findElement(By.xpath("//button/*[contains(@icon,'refresh')]/.."));

			if(isClickable(element) == true) {
				element.click();
			}else
			{
				findelementinframe(element,iFrame);
				System.out.println("The Button "+buttonName+"is not available in the page");
			}
			driver.switchTo().defaultContent();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void CreateAccountfromInteractionLauncher(String interactionbutton, String accountNamerandom) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		try {
			Actions action = new Actions(driver);
			WebElement element = driver.findElement(By.xpath("//button/span[text()='"+interactionbutton+"']"));
			if(isClickable(element) == true)
			{
				action.moveToElement(driver.findElement(By.xpath("//label[text()='Account Name']/..//input"))).build();
				action.build().perform();
				driver.findElement(By.xpath("//label[text()='Account Name']/..//input")).sendKeys(accountNamerandom);
				driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
				driver.findElement(By.xpath("(//button[contains(@class,'footer')]//span[text()='Search'])[1]")).click();
				driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
				Thread.sleep(100);
				action.moveToElement(driver.findElement(By.xpath("(//span[text()='Create Account']/preceding-sibling::i)[1]"))).build();
				action.build().perform();
			}
			else
			{
				findelement(element);
				driver.findElement(By.xpath("//label[text()='Account Name']/..//input")).click();
				driver.findElement(By.xpath("//label[text()='Account Name']/..//input")).sendKeys(accountNamerandom);
				driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS) ;
				driver.findElement(By.xpath("(//button[contains(@class,'footer')]//span[text()='Search'])[1]")).click();
				driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
				action.moveToElement(driver.findElement(By.xpath("(//span[text()='Create Account']/preceding-sibling::i)[1]"))).build();
				action.build().perform();
			}

		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void ClickCreateAccountIcon(String iconName) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		try {
				WebElement element = driver.findElement(By.xpath("(//span[text()='"+iconName+"']/preceding-sibling::i/..)[1]"));
				if(isClickable(element) == true)
				{
				}
				else
				{
					findelement(element);
				}

		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void clickanAccountNameandEdit(String accountName) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		Actions action = new Actions(driver);
		try {
				WebElement element = driver.findElement(By.xpath("//a[contains(text(),'"+accountName+"')]/../../../preceding-sibling::div/a"));
				if(isClickable(element) == true)
				{
					action.moveToElement(driver.findElement(By.xpath("//a[contains(text(),'"+accountName+"')]/../../../preceding-sibling::div/a"))).build();
					action.build().perform();
					//			driver.findElement(By.xpath("//div[@class='forceActionLink' and text()='Edit']")).click();
					action.moveToElement(driver.findElement(By.xpath("//a[@title='Edit']"))).build();
					action.click();
				}
				else
				{
					findelement(element);
					action.moveToElement(driver.findElement(By.xpath("//a[contains(text(),'"+accountName+"')]/../../../preceding-sibling::div/a"))).build();
					action.build().perform();
					action.moveToElement(driver.findElement(By.xpath("//a[@title='Edit']"))).build();
					action.click();
					//driver.findElement(By.xpath("//a[@title='Edit']")).click();
				}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void minimizetab(String tabName) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		Actions actions = new Actions(driver);
		try {
			if(tabName.contains("Risk Details")) {
				actions.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).perform();	
			}
			iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
			driver.switchTo().frame(iFrame);
			Thread.sleep(100);
			WebElement element = driver.findElement(By.xpath("//div[contains(text(),'"+tabName+"')]/..//*[contains(@class,'expand')]"));
			if(isClickable(element) == true)
			{
			}
			else
			{
				findelementinframe(element, iFrame);
				//driver.findElement(By.xpath("//a[@title='Edit']")).click();
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		driver.switchTo().defaultContent();
	}

	public void ClickEditUnderdropdown(String buttonName) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		Actions action = new Actions(driver);
		try {
				WebElement element = driver.findElement(By.xpath("//a[@title='Edit']"));
				if(isClickable(element) == true)
				{
					action.moveToElement(driver.findElement(By.xpath("//a[@title='Edit']"))).build();
					action.click();
				}
				else
				{
					findelement(element);
					action.moveToElement(driver.findElement(By.xpath("//a[@title='Edit']"))).build();
					action.click();
				}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void searchaccountfromglobalsearch(String accountName) throws Exception{
		
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@class,'uiInputTextForAutocomplete')]")));
		driver.findElement(By.xpath("//input[contains(@class,'uiInputTextForAutocomplete')]")).sendKeys(accountName);
		
		try {
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(@class,'mruSearchLabel')]")));
			
			driver.findElement(By.xpath("//span[contains(@class,'mruSearchLabel')]")).click();
			
		}
		catch(Exception e) {
			driver.navigate().refresh();
			wait = new WebDriverWait(driver,30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[contains(@class,'uiInputTextForAutocomplete')]")));
			driver.findElement(By.xpath("//input[contains(@class,'uiInputTextForAutocomplete')]")).sendKeys(accountName);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(@class,'mruSearchLabel')]")));
			driver.findElement(By.xpath("//span[contains(@class,'mruSearchLabel')]")).click();
		}
		Thread.sleep(10000);
		driver.findElement(By.xpath("//a[text()='Accounts']/following::a[@title='"+accountName+"'][1]")).click();
	}

	public void navigatefromcontactpage(String accountName) throws Exception{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		try {
			WebElement element = driver.findElement(By.xpath("//p[@title='Account Name']/../p/following::a[text()='"+accountName+"']"));
			if(isClickable(element) == true)
			{
			}
			else
			{
				findelement(element);
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void selectRecentQuoteNumber() throws Exception{
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS) ;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("(//*[@class='view-all-label' and text()='View All'])[1]")));
			WebElement element = driver.findElement(By.xpath("(//*[@class='view-all-label' and text()='View All'])[1]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		}catch(Exception e) {
			System.out.println("Exception in function selectRecentQuoteNumber():"+e.getMessage());
			WebElement element = driver.findElement(By.xpath("(//*[@class='view-all-label' and text()='View All'])[1]"));
			if(isClickable(element) == true)
			{

			}
			else
			{
				findelement(element);
			}

		}
	}

	public void selectRecentQuoteNumberfromtable() throws Exception{
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		String order = driver.findElement(By.xpath("(//span[@title='Quote Number' and @class='slds-truncate']/following::span[@class='slds-assistive-text'])[1]")).getText();
		if(order.equalsIgnoreCase("Sorted Descending") == true )
		{

		}
		else
		{
			WebElement element = driver.findElement(By.xpath("//span[@title='Quote Number' and @class='slds-truncate']"));
			if(isClickable(element) == true)
			{				
			}
			else
			{
				findelement(element);
			}
		}
	}

	public void clickLinktab(String QuoteNumber,String linkName) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__universalcardpage?layout=ZPC%')]"));
		driver.switchTo().frame(iFrame);
		Thread.sleep(3000);
		WebElement element = driver.findElement(By.xpath("//span[contains(text(),'Quote Number') and contains(text(),' "+QuoteNumber+"')]/../../../following-sibling::div//div[contains(@class,'padded slds-size')][2]//a[contains(.,'"+linkName+"')]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		Thread.sleep(15000);
		new WebDriverWait(driver, 25).until(ExpectedConditions.elementToBeClickable(element));
		executor.executeScript("arguments[0].click();", element);
		/*if(isClickable(element) == true) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		}else {
//			JavascriptExecutor executor = (JavascriptExecutor)driver;
//			executor.executeScript("arguments[0].click();", element);
			findelementinframe(element,iFrame);
		}*/
		driver.switchTo().defaultContent();
	}

	public void selectContact(String fieldName,String value) throws Exception{
		WebElement element = driver.findElement(By.xpath("//div[@role='listitem']//span[text()='"+fieldName+"']/following::input"));
		if(isClickable(element) == true)
		{
			driver.findElement(By.xpath("//div[@role='listitem']//span[text()='"+fieldName+"']/following::input[1]")).sendKeys(value);
			driver.findElement(By.xpath("//span[contains(text(),'"+value+"') and contains(text(),'in Contacts')]")).click();
			driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS) ;
			driver.findElement(By.xpath("//table[contains(@class,'bordered') and not(contains(@class,'edit'))]//following::tbody/tr[1]/td[1]/a")).click();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		}
		else
		{
			findelement(element);
			driver.findElement(By.xpath("//div[@role='listitem']//span[text()='"+fieldName+"']/following::input[1]")).sendKeys(value);
			driver.findElement(By.xpath("//span[contains(text(),'"+value+"') and contains(text(),'in Contacts')]")).click();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;	
			driver.findElement(By.xpath("//table[contains(@class,'bordered') and not(contains(@class,'edit'))]//following::tbody/tr[1]/td[1]/a")).click();
		}
	}

	public void selectDropdownforGarageAndUse(String fieldName,String value,String pageName) throws Exception{
		iFrame = driver.findElement(By.xpath("//iframe[contains(@src,'/apex/vlocity_ins__OmniScriptUniversalPage?')]"));
		driver.switchTo().frame(iFrame);
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		WebElement element = driver.findElement(By.xpath("//select[@id='"+fieldName+"']/option[text()='"+value+"']"));
		if(isClickable(element) == true)
		{

		}else
		{
			findelementinframe(element,iFrame);
		}	
		driver.switchTo().defaultContent();
	}


	public void verifyRating() throws Exception
	{
		String parentWindow = driver.getWindowHandle();
		Set<String> Windowid = driver.getWindowHandles();
		Iterator<String> itr = Windowid.iterator();
		while(itr.hasNext())
		{
			driver.switchTo().window(itr.next());
			System.out.println("");
		}
		//WebDriverWait wait = new WebDriverWait(driver, 100);	   
		//wait.until(ExpectedConditions.textToBePresentInElement((WebElement) By.xpath("//div[contains(@id,'substatus') and contains(text(),'complete')]"),"complete"));			
		//WebDriverWait wait1 = new WebDriverWait(driver, 100);	   
		//wait1.until(ExpectedConditions.textToBePresentInElement((WebElement) By.xpath("//table[contains(@class,'slds-table slds-table_cell-buffer slds-table_bordered')]//tr[last()]//td[last()]"),"complete"));
		Thread.sleep(50000);
		driver.close();
		driver.switchTo().window(parentWindow);
	}
	
	public void selectFromDatePicker(String fieldName, String date,String pageName) throws InterruptedException
	{	
		WebElement element = driver.findElement(By.xpath("//span[text()='"+fieldName+"']/../following::a[@class='datePicker-openIcon display']/../input"));
		if(isClickable(element) == true)
		{  
			//for selecting current date
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@class='slds-day weekend todayDate selectedDate DESKTOP uiDayInMonthCell--default']")).click();
			//driver.findElement(By.xpath("//span[text()='"+fieldName+"']/../following::a[@class='datePicker-openIcon display']/../input")).click();
			driver.findElement(By.xpath("//span[text()='"+fieldName+"']/../following::a[@class='datePicker-openIcon display']/../input")).clear();
			driver.findElement(By.xpath("//span[text()='"+fieldName+"']/../following::a[@class='datePicker-openIcon display']/../input")).sendKeys(date);		
			driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
			driver.findElement(By.xpath("//span[text()='"+fieldName+"']/../following::a[@class='datePicker-openIcon display']/../input")).click();
			driver.findElement(By.xpath("//span[contains(@class,'selectedDate DESKTOP')]")).click();
		}
		else
		{
			findelement(element);
			driver.findElement(By.xpath("//span[text()='"+fieldName+"']/../following::a[@class='datePicker-openIcon display']/../input")).click();
			driver.findElement(By.xpath("//span[text()='"+fieldName+"']/../following::a[@class='datePicker-openIcon display']/../input")).clear();
			driver.findElement(By.xpath("//span[text()='"+fieldName+"']/../following::a[@class='datePicker-openIcon display']/../input")).sendKeys(date);
			driver.findElement(By.xpath("//span[text()='"+fieldName+"']/../following::a[@class='datePicker-openIcon display']/../input")).click();
			driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
			driver.findElement(By.xpath("//span[contains(@class,'selectedDate DESKTOP')]")).click();	
		}	
	}
	
	public void enterCompleteAddress(String postcode) {
		
		try {
			if(postcode.equals("NR3 2PQ")) {
				driver.findElement(By.xpath("//input[@name='postalCode']")).sendKeys("NR3 2PQ");
				driver.findElement(By.xpath("//input[@name='country']")).sendKeys("United Kingdom");
				driver.findElement(By.xpath("//input[@name='city']")).sendKeys("Norwich");
				driver.findElement(By.xpath("//div/textarea[@name='street']")).sendKeys("Drayton Road");
			}else if(postcode.equals("PO15 6SA")) {
				driver.findElement(By.xpath("//input[@name='postalCode']")).sendKeys("PO15 6SA");
				driver.findElement(By.xpath("//input[@name='country']")).sendKeys("United Kingdom");
				driver.findElement(By.xpath("//input[@name='city']")).sendKeys("Fareham");
				driver.findElement(By.xpath("//div/textarea[@name='street']")).sendKeys("Whiteley");
			}
			else if(postcode.equals("PO15 7JZ")) {
				driver.findElement(By.xpath("//input[@name='postalCode']")).sendKeys("PO15 7JZ");
				driver.findElement(By.xpath("//input[@name='country']")).sendKeys("United Kingdom");
				driver.findElement(By.xpath("//input[@name='city']")).sendKeys("Fareham");
				driver.findElement(By.xpath("//div/textarea[@name='street']")).sendKeys("Parkway Whiteley");
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}