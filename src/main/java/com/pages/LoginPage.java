package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

	private WebDriver driver;


	private By emailId = By.xpath("//input[@id='username']");
	private By password = By.xpath("//input[@id='password']");
	private By signInButton = By.xpath("//input[@id='Login']");
	private By ZPCText= By.xpath("//span[@title='ZPC']");

	//Constructor of the page class:
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	public void enterUserName(String username) {
		driver.findElement(emailId).sendKeys(username);
	}


	public void enterPassword(String pwd) {
		driver.findElement(password).sendKeys(pwd);
	}


	public void clickOnLogin() {
		driver.findElement(signInButton).click();
	}


	public String getLoginPageTitle() {
		return driver.getTitle();
	}
	
	public boolean verifyApplicationText(String Zpctext) {
		return driver.findElement(ZPCText).getText().equals(Zpctext);
	}

}
